-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 02, 2017 at 06:07 PM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `airfare`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `email`, `password`, `status`, `created_date`, `image`) VALUES
(1, 'admin', 'buildrepo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, '2017-07-19', '1500447374-3771708916_6f87c3e659.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` int(10) NOT NULL,
  `message` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `mobile`, `message`, `status`, `created_date`) VALUES
(7, 'Shashank Tripathi', 'buildrepo.shashank@gmail.com', 2147483647, 'Nice company', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` int(10) NOT NULL COMMENT '1 - aboutus, 2 - t&c, 3 - privacypolicy',
  `content` text NOT NULL,
  `updated date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `type`, `content`, `updated date`) VALUES
(1, 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lacinia augue erat, vel sodales quam iaculis eget. Vestibulum imperdiet lacus lectus, et dictum ex feugiat sed. Donec vel nisi non ante consequat viverra non sit amet nunc. Sed luctus aliquet ornare. Mauris dolor justo, lacinia non orci a, ultrices lacinia felis. Donec feugiat congue neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ultrices magna at odio tincidunt, mattis viverra enim ultricies. Phasellus interdum condimentum arcu ac pharetra. Curabitur id arcu at nibh ultricies accumsan sed sed nisi. Sed maximus egestas neque, non mollis libero auctor ut. Nam sed lorem odio. Aenean sed pulvinar augue. Mauris blandit lectus at urna suscipit, a accumsan nunc viverra.</p>\n<p>Duis ut arcu metus. Sed semper, nulla sit amet sodales cursus, nisl erat faucibus nunc, sed ultricies nunc lectus eu metus. Proin aliquam gravida tellus blandit vulputate. In posuere odio nec eros feugiat, a vehicula nisl finibus. Nulla augue massa, tincidunt vel quam sed, egestas scelerisque orci. Nunc non nulla eu lacus ultricies placerat. Nulla eget enim porta, pulvinar eros ac, tempor nisi. Maecenas pellentesque quam sit amet cursus posuere. Cras ligula eros, accumsan eu pulvinar sed, fermentum id sem. Cras nunc elit, luctus eget neque et, tempus gravida ante. Nunc tincidunt pulvinar turpis, et mattis odio fermentum eu.</p>\n<p>Suspendisse venenatis facilisis leo in luctus. Phasellus pulvinar facilisis efficitur. Fusce a tellus ultrices, accumsan nisi lobortis, placerat nibh. Etiam mattis dignissim tortor, id egestas tellus faucibus in. Nam non justo sit amet nibh maximus gravida. Duis accumsan nibh at ex iaculis, ac vulputate dolor lacinia. Suspendisse mi justo, pulvinar sit amet ornare et, sodales tempus turpis. Ut ut venenatis metus. Nunc purus augue, faucibus eget orci ac, fringilla ullamcorper eros. Proin bibendum convallis quam ac scelerisque. Ut sagittis, justo et viverra porta, mauris tellus tempus lectus, sed venenatis tellus velit eget risus.</p>', '2017-08-11 11:29:00'),
(2, 2, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lacinia augue erat, vel sodales quam iaculis eget. Vestibulum imperdiet lacus lectus, et dictum ex feugiat sed. Donec vel nisi non ante consequat viverra non sit amet nunc. Sed luctus aliquet ornare. Mauris dolor justo, lacinia non orci a, ultrices lacinia felis. Donec feugiat congue neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ultrices magna at odio tincidunt, mattis viverra enim ultricies. Phasellus interdum condimentum arcu ac pharetra. Curabitur id arcu at nibh ultricies accumsan sed sed nisi. Sed maximus egestas neque, non mollis libero auctor ut. Nam sed lorem odio. Aenean sed pulvinar augue. Mauris blandit lectus at urna suscipit, a accumsan nunc viverra.</p>\r\n<p>Duis ut arcu metus. Sed semper, nulla sit amet sodales cursus, nisl erat faucibus nunc, sed ultricies nunc lectus eu metus. Proin aliquam gravida tellus blandit vulputate. In posuere odio nec eros feugiat, a vehicula nisl finibus. Nulla augue massa, tincidunt vel quam sed, egestas scelerisque orci. Nunc non nulla eu lacus ultricies placerat. Nulla eget enim porta, pulvinar eros ac, tempor nisi. Maecenas pellentesque quam sit amet cursus posuere. Cras ligula eros, accumsan eu pulvinar sed, fermentum id sem. Cras nunc elit, luctus eget neque et, tempus gravida ante. Nunc tincidunt pulvinar turpis, et mattis odio fermentum eu.</p>\r\n<p>Suspendisse venenatis facilisis leo in luctus. Phasellus pulvinar facilisis efficitur. Fusce a tellus ultrices, accumsan nisi lobortis, placerat nibh. Etiam mattis dignissim tortor, id egestas tellus faucibus in. Nam non justo sit amet nibh maximus gravida. Duis accumsan nibh at ex iaculis, ac vulputate dolor lacinia. Suspendisse mi justo, pulvinar sit amet ornare et, sodales tempus turpis. Ut ut venenatis metus. Nunc purus augue, faucibus eget orci ac, fringilla ullamcorper eros. Proin bibendum convallis quam ac scelerisque. Ut sagittis, justo et viverra porta, mauris tellus tempus lectus, sed venenatis tellus velit eget risus.</p>', '2017-08-11 12:33:00'),
(3, 3, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lacinia augue erat, vel sodales quam iaculis eget. Vestibulum imperdiet lacus lectus, et dictum ex feugiat sed. Donec vel nisi non ante consequat viverra non sit amet nunc. Sed luctus aliquet ornare. Mauris dolor justo, lacinia non orci a, ultrices lacinia felis. Donec feugiat congue neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ultrices magna at odio tincidunt, mattis viverra enim ultricies. Phasellus interdum condimentum arcu ac pharetra. Curabitur id arcu at nibh ultricies accumsan sed sed nisi. Sed maximus egestas neque, non mollis libero auctor ut. Nam sed lorem odio. Aenean sed pulvinar augue. Mauris blandit lectus at urna suscipit, a accumsan nunc viverra.</p>\n<p>Duis ut arcu metus. Sed semper, nulla sit amet sodales cursus, nisl erat faucibus nunc, sed ultricies nunc lectus eu metus. Proin aliquam gravida tellus blandit vulputate. In posuere odio nec eros feugiat, a vehicula nisl finibus. Nulla augue massa, tincidunt vel quam sed, egestas scelerisque orci. Nunc non nulla eu lacus ultricies placerat. Nulla eget enim porta, pulvinar eros ac, tempor nisi. Maecenas pellentesque quam sit amet cursus posuere. Cras ligula eros, accumsan eu pulvinar sed, fermentum id sem. Cras nunc elit, luctus eget neque et, tempus gravida ante. Nunc tincidunt pulvinar turpis, et mattis odio fermentum eu.</p>\n<p>Suspendisse venenatis facilisis leo in luctus. Phasellus pulvinar facilisis efficitur. Fusce a tellus ultrices, accumsan nisi lobortis, placerat nibh. Etiam mattis dignissim tortor, id egestas tellus faucibus in. Nam non justo sit amet nibh maximus gravida. Duis accumsan nibh at ex iaculis, ac vulputate dolor lacinia. Suspendisse mi justo, pulvinar sit amet ornare et, sodales tempus turpis. Ut ut venenatis metus. Nunc purus augue, faucibus eget orci ac, fringilla ullamcorper eros. Proin bibendum convallis quam ac scelerisque. Ut sagittis, justo et viverra porta, mauris tellus tempus lectus, sed venenatis tellus velit eget risus.</p>', '2017-08-11 12:45:00');

-- --------------------------------------------------------

--
-- Table structure for table `flight_details`
--

CREATE TABLE IF NOT EXISTS `flight_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flight_no` varchar(500) DEFAULT NULL,
  `flight_name` varchar(500) DEFAULT NULL,
  `source` varchar(500) DEFAULT NULL,
  `destination` varchar(500) DEFAULT NULL,
  `datetime_arrival` datetime DEFAULT NULL,
  `datetime_departure` datetime DEFAULT NULL,
  `seat_availability` int(11) NOT NULL DEFAULT '0',
  `cost` float NOT NULL DEFAULT '0',
  `mail_id` varchar(500) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `flight_details`
--

INSERT INTO `flight_details` (`id`, `flight_no`, `flight_name`, `source`, `destination`, `datetime_arrival`, `datetime_departure`, `seat_availability`, `cost`, `mail_id`, `created_date`, `status`) VALUES
(2, 'test123', 'test', '1', '2', '2017-07-19 22:56:59', '2017-08-28 02:02:01', 144, 7876, 'test@test.test', '2017-07-20 09:17:47', 1),
(3, '98987', 'Ret456', '2', '1', '2017-09-01 12:00:00', '2017-08-29 08:00:00', 7, 4000, 'asd@tre.com', '2017-08-10 09:03:24', 1),
(4, '6E-373', 'INDIGO', '3', '4', '2017-08-30 18:25:00', '2017-08-30 17:30:00', 0, 2500, 'agtallinone@gmail.com', '2017-08-11 13:17:00', 1),
(5, '123', 'haha', '1', '2', '2017-08-30 00:00:00', '2017-08-28 00:00:00', 5, 1234, 'zzz@gmail.com', '2017-08-19 05:30:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `location` text NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location`, `created_date`) VALUES
(1, 'Lucknow', '2017-08-02 08:36:05'),
(2, 'Delhi', '2017-08-02 08:36:32'),
(3, 'AGARTALA', '2017-08-11 13:12:37'),
(4, 'KOLKATA', '2017-08-11 13:12:58'),
(5, 'Mumbai', '2017-08-25 09:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `order_number` varchar(10) NOT NULL,
  `journey_type` varchar(10) NOT NULL,
  `source` int(10) NOT NULL,
  `destination` int(10) NOT NULL,
  `passengers` int(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `return_date` varchar(20) DEFAULT NULL,
  `flight_id` int(10) NOT NULL,
  `Flight_id_return` int(10) DEFAULT '0',
  `amount` int(10) NOT NULL,
  `status` int(10) NOT NULL COMMENT '1 - Paid, 0 - Pending',
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_number`, `journey_type`, `source`, `destination`, `passengers`, `date`, `return_date`, `flight_id`, `Flight_id_return`, `amount`, `status`, `created_datetime`) VALUES
(94, 1, 'OR00', 'RoundTrip', 1, 2, 1, '28-08-2017', '29-08-2017', 2, 3, 11876, 1, '2017-08-10 10:42:53'),
(95, 1, 'OR00', 'OneWay', 2, 1, 3, '29-08-2017', '01-01-1970', 3, 0, 12000, 1, '2017-08-11 08:27:01'),
(96, 2, 'OR00', 'OneWay', 3, 4, 1, '30-08-2017', '01-01-1970', 4, 0, 2500, 1, '2017-08-11 13:19:54'),
(97, 2, 'OR00', 'OneWay', 3, 4, 9, '30-08-2017', '01-01-1970', 4, 0, 22500, 1, '2017-08-11 13:27:02'),
(98, 3, 'OR00', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-18 06:23:23'),
(99, 3, 'OR00', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-18 06:24:11'),
(100, 3, 'OR00', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-18 06:25:07'),
(101, 3, 'OR00', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 1, '2017-08-18 06:25:11'),
(102, 3, 'OR00', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-18 07:30:48'),
(103, 3, 'OR00', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-18 07:34:12'),
(104, 3, 'OR00', 'OneWay', 1, 2, 3, '28-08-2017', '01-01-1970', 2, 0, 23628, 0, '2017-08-18 07:34:28'),
(105, 3, 'OR00105', 'OneWay', 1, 2, 10, '28-08-2017', '01-01-1970', 2, 0, 78760, 0, '2017-08-18 08:16:59'),
(106, 3, 'OR00106', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 1, '2017-08-18 08:17:11'),
(107, 3, 'OR00107', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 1, '2017-08-18 12:15:40'),
(108, 3, 'OR00108', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 1, '2017-08-18 12:48:28'),
(109, 3, 'OR00109', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-18 17:50:10'),
(110, 3, 'OR00110', 'OneWay', 1, 2, 5, '28-08-2017', '01-01-1970', 2, 0, 39380, 0, '2017-08-18 17:50:48'),
(111, 3, 'OR00111', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 0, '2017-08-19 06:35:38'),
(112, 1, 'OR00112', 'OneWay', 1, 2, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-19 06:52:59'),
(113, 3, 'OR00113', 'OneWay', 1, 2, 2, '28-08-2017', '01-01-1970', 2, 0, 15752, 0, '2017-08-19 10:36:17'),
(114, 3, 'OR00114', 'OneWay', 1, 2, 2, '28-08-2017', '01-01-1970', 2, 0, 15752, 0, '2017-08-19 10:46:32'),
(115, 1, 'OR00115', 'OneWay', 0, 0, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-19 12:16:43'),
(116, 3, 'OR00116', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 12:18:08'),
(117, 3, 'OR00117', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 12:19:29'),
(118, 3, 'OR00118', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 12:24:52'),
(119, 3, 'OR00119', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 12:27:50'),
(120, 1, 'OR00120', 'RoundTrip', 0, 0, 1, '28/08/2017', '29-08-2017', 2, 3, 7876, 0, '2017-08-19 13:29:54'),
(121, 3, 'OR00121', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 14:06:45'),
(122, 3, 'OR00122', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 14:09:04'),
(123, 3, 'OR00123', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 14:12:44'),
(124, 3, 'OR00124', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 14:19:30'),
(125, 3, 'OR00125', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-19 14:34:04'),
(126, 3, 'OR00126', 'OneWay', 0, 0, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-20 14:44:24'),
(127, 3, 'OR00127', 'OneWay', 1, 2, 1, '28-08-2017', '01-01-1970', 2, 0, 7876, 1, '2017-08-20 17:28:56'),
(128, 1, 'OR00128', 'OneWay', 0, 0, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-20 17:33:29'),
(129, 3, 'OR00129', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-21 04:32:07'),
(130, 3, 'OR00130', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-21 10:13:24'),
(131, 3, 'OR00131', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-21 10:53:32'),
(132, 1, 'OR00132', 'OneWay', 1, 2, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-21 11:19:57'),
(133, 3, 'OR00133', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-22 07:06:52'),
(134, 3, 'OR00134', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-22 07:21:37'),
(135, 1, 'OR00135', 'RoundTrip', 1, 2, 1, '28/08/2017', '29-08-2017', 2, 3, 11876, 0, '2017-08-22 07:24:12'),
(136, 1, 'OR00136', 'OneWay', 1, 2, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-22 07:24:27'),
(137, 3, 'OR00137', 'RoundTrip', 1, 2, 1, '8/28/2017', '8/29/2017', 2, 3, 11876, 0, '2017-08-22 07:26:10'),
(138, 3, 'OR00138', 'RoundTrip', 1, 2, 1, '8/28/2017', '8/29/2017', 2, 3, 11876, 0, '2017-08-22 07:33:59'),
(139, 3, 'OR00139', 'RoundTrip', 1, 2, 1, '8/28/2017', '8/29/2017', 2, 3, 11876, 0, '2017-08-22 08:00:04'),
(140, 3, 'OR00140', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-22 08:31:30'),
(141, 1, 'OR00141', 'RoundTrip', 1, 2, 1, '28/08/2017', '29-08-2017', 2, 3, 11876, 0, '2017-08-22 09:36:51'),
(142, 3, 'OR00142', 'RoundTrip', 1, 2, 1, '8/28/2017', '8/29/2017', 5, 3, 5234, 0, '2017-08-22 09:41:28'),
(143, 3, 'OR00143', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-22 14:21:51'),
(144, 1, 'OR00144', 'RoundTrip', 1, 2, 1, '28/08/2017', '29-08-2017', 2, 3, 11876, 0, '2017-08-22 14:35:52'),
(145, 3, 'OR00145', 'RoundTrip', 1, 2, 1, '8/28/2017', '8/29/2017', 2, 3, 11876, 0, '2017-08-22 14:37:46'),
(146, 3, 'OR00146', 'RoundTrip', 1, 2, 1, '8/28/2017', '8/29/2017', 2, 3, 11876, 0, '2017-08-22 19:34:39'),
(147, 3, 'OR00147', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-22 20:17:48'),
(148, 1, 'OR00148', 'OneWay', 1, 2, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-23 11:23:35'),
(149, 3, 'OR00149', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-23 11:49:22'),
(150, 3, 'OR00150', 'OneWay', 1, 2, 2, '28-08-2017', '01-01-1970', 2, 0, 15752, 1, '2017-08-25 07:47:05'),
(151, 3, 'OR00151', 'OneWay', 1, 2, 2, '28-08-2017', '01-01-1970', 2, 0, 15752, 1, '2017-08-25 10:22:15'),
(152, 2, 'OR00152', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-28 03:08:34'),
(153, 2, 'OR00153', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-08-28 03:10:51'),
(154, 1, 'OR00154', 'OneWay', 1, 2, 1, '28/08/2017', NULL, 2, 0, 7876, 0, '2017-08-30 04:30:50'),
(155, 2, 'OR00155', 'OneWay', 1, 2, 1, '8/28/2017', NULL, 2, 0, 7876, 0, '2017-09-02 15:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

CREATE TABLE IF NOT EXISTS `passengers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `age` int(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `passengers`
--

INSERT INTO `passengers` (`id`, `name`, `age`, `gender`, `order_id`, `created_date`) VALUES
(3, 'Vipin', 25, 'Male', 71, '0000-00-00 00:00:00'),
(4, 'Shank', 23, 'Male', 71, '0000-00-00 00:00:00'),
(42, 'Shank', 22, 'Male', 72, '0000-00-00 00:00:00'),
(43, 'Atul', 17, 'Male', 72, '0000-00-00 00:00:00'),
(44, '1', 1, 'Female', 73, '0000-00-00 00:00:00'),
(45, '2', 3, 'Female', 73, '0000-00-00 00:00:00'),
(46, '123', 123, 'Male', 73, '0000-00-00 00:00:00'),
(47, '123', 123, 'Female', 73, '0000-00-00 00:00:00'),
(48, '123', 123, 'Male', 73, '0000-00-00 00:00:00'),
(49, '123', 123, 'Male', 73, '0000-00-00 00:00:00'),
(50, 'Shank', 22, 'Male', 74, '0000-00-00 00:00:00'),
(51, 'Gaurav', 24, 'Male', 74, '0000-00-00 00:00:00'),
(52, 'Atul', 18, 'Male', 74, '0000-00-00 00:00:00'),
(53, 'Nishant', 20, 'Male', 74, '0000-00-00 00:00:00'),
(54, 'Ayush', 20, 'Male', 74, '0000-00-00 00:00:00'),
(55, 'Shashank', 23, 'Male', 94, '0000-00-00 00:00:00'),
(56, 'Shashank', 23, 'Male', 95, '0000-00-00 00:00:00'),
(57, 'Atul', 18, 'Male', 95, '0000-00-00 00:00:00'),
(58, 'Gaurav', 32, 'Male', 95, '0000-00-00 00:00:00'),
(59, 'suresh agarwalla', 35, 'Male', 96, '0000-00-00 00:00:00'),
(60, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(61, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(62, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(63, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(64, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(65, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(66, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(67, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(68, 'g', 0, 'Male', 97, '0000-00-00 00:00:00'),
(69, 'Anant', 22, 'Male', 101, '0000-00-00 00:00:00'),
(70, 'Anant', 22, 'Male', 106, '0000-00-00 00:00:00'),
(71, 'anant ', 22, 'Male', 107, '0000-00-00 00:00:00'),
(72, 'anant', 0, 'Male', 108, '0000-00-00 00:00:00'),
(73, 'anant', 2, 'Male', 108, '0000-00-00 00:00:00'),
(74, 'Anant', 23, 'Male', 127, '0000-00-00 00:00:00'),
(75, 'anant', 22, 'Male', 150, '2017-08-25 07:47:22'),
(76, 'rishabh', 22, 'Male', 150, '2017-08-25 07:47:22'),
(77, 'anant', 22, 'Male', 151, '2017-08-25 10:22:35'),
(78, 'rishabh', 23, 'Male', 151, '2017-08-25 10:22:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `phone` bigint(20) NOT NULL DEFAULT '0',
  `gender` text NOT NULL,
  `image` varchar(50) NOT NULL DEFAULT 'default.png',
  `created_date` datetime DEFAULT NULL,
  `varify_status` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `gender`, `image`, `created_date`, `varify_status`, `status`) VALUES
(1, 'Roger', 'buildrepo.gaurav@gmail.com', 'c33367701511b4f6020ec61ded352059', 9999999999, 'Male', '62111.jpg', '2017-07-19 07:32:10', 1, 0),
(2, 'ajay', 'buildrepo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1234567890, 'Female', '11056.jpg', '2017-08-09 04:08:19', 1, 0),
(3, 'shashank', 'buildrepo.anant@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9818606542, 'Male', '2137.jpg', '2017-08-09 06:10:19', 1, 0),
(6, 'Shashank Tripathi', 'buildrepo.shashank@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9999999999, 'Male', 'default.png', '2017-08-09 09:08:28', 1, 0),
(7, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, '', 'default.png', NULL, 0, 0),
(8, 'ajay', 'buasd@hasd.com', '202cb962ac59075b964b07152d234b70', 123, 'Male', 'default.png', '2017-08-09 12:08:55', 0, 0),
(9, 'Suresh', 'Agtallinone@gmail.com', '43f637abd004bc0b86ea275b94484140', 8900404722, 'male', 'default.png', '2017-08-11 15:25:50', 0, 0),
(10, 'Suresh', 'agarwallaconsultancy@gmail.com', '43f637abd004bc0b86ea275b94484140', 8900404722, 'male', 'default.png', '2017-08-11 15:26:34', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
