-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 03, 2017 at 05:37 AM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jjdesign`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `category_id`, `name`, `status`) VALUES
(1, 1, 'WF-Attr-001', 1),
(2, 1, 'WF-Attr-002', 1),
(3, 2, 'MF-Attr-001', 1),
(4, 2, 'MF-Attr-002', 1),
(5, 3, 'OA-Attr-001', 1),
(6, 3, 'OA-Attr-002', 1);

-- --------------------------------------------------------

--
-- Table structure for table `billing_addresses`
--

CREATE TABLE IF NOT EXISTS `billing_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(500) DEFAULT NULL,
  `last_name` varchar(500) DEFAULT NULL,
  `street1` varchar(500) DEFAULT NULL,
  `street2` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE IF NOT EXISTS `buyers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(500) DEFAULT NULL,
  `last_name` varchar(500) DEFAULT NULL,
  `cid` varchar(500) DEFAULT NULL,
  `mobile` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `verify_status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_sets`
--

CREATE TABLE IF NOT EXISTS `candidate_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `set_name` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `candidate_sets`
--

INSERT INTO `candidate_sets` (`id`, `set_name`, `status`) VALUES
(1, 'Dining', 0),
(2, 'Deep Seating', 0),
(3, 'Chat', 0),
(4, 'Fire Pit', 0),
(5, 'Bar', 0),
(6, 'Lounge', 0),
(7, 'Stand Alone Item', 0);

-- --------------------------------------------------------

--
-- Table structure for table `card_details`
--

CREATE TABLE IF NOT EXISTS `card_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `card_type` varchar(500) DEFAULT NULL,
  `cc_number` varchar(500) DEFAULT NULL,
  `name_on_card` varchar(500) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `cvv` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `created_date` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(500) NOT NULL,
  `short_name` varchar(500) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `transport` float NOT NULL DEFAULT '0',
  `duty` float NOT NULL DEFAULT '0',
  `overhead` float NOT NULL DEFAULT '0',
  `profit` float NOT NULL DEFAULT '0',
  `shipping_increment` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `short_name`, `parent_id`, `transport`, `duty`, `overhead`, `profit`, `shipping_increment`) VALUES
(1, 'Wicker Furniture', 'WF', 0, 0.35, 0, 0.62, 0.4, 0.02),
(2, 'Metal Furniture', 'MF', 0, 0, 0, 0, 0, 0),
(3, 'Outdoor Accessories', 'OA', 0, 0, 0, 0, 0, 0),
(4, 'Fire Glass', 'FG', 0, 0, 0, 0, 0, 0),
(5, 'BBQ Accessories', 'BBQ', 0, 0, 0, 0, 0, 0),
(6, 'Umbrellas & Bases', 'UB', 0, 0, 0, 0, 0, 0),
(7, 'Motion Desks', 'MD', 0, 0, 0, 0, 0, 0),
(8, 'Vendor Line of Business', 'VEN', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE IF NOT EXISTS `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL,
  `content` text,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0=about, 1=term, 2=privacy',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `title`, `content`, `type`) VALUES
(1, 'About Us', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 0),
(2, 'Terms And Conditions', '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself,retretg</p>\r\n<p>&nbsp;</p>', 1),
(3, 'Privacy Policy', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 2);

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE IF NOT EXISTS `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_name` varchar(500) DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `col_id` varchar(500) DEFAULT NULL,
  `candidate_set_id` int(11) NOT NULL DEFAULT '0',
  `pid` varchar(500) DEFAULT NULL,
  `pname` varchar(500) DEFAULT NULL,
  `no_of_cartoon` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `collection_name`, `category_id`, `created_date`, `status`, `col_id`, `candidate_set_id`, `pid`, `pname`, `no_of_cartoon`) VALUES
(5, 'South Beach', 1, NULL, 0, 'SB', 0, 'Deep Seating SET', 'SB 5 Piece Deep Seating set', 2),
(6, 'South Beach', 1, NULL, 0, 'SB', 0, '1234', '7 pc deep seating set', 3),
(7, 'South Beach', 2, NULL, 0, 'SB', 0, 'Se34', 'SB 5 Piece Deep Seating set', 2);

-- --------------------------------------------------------

--
-- Table structure for table `collection_attributes`
--

CREATE TABLE IF NOT EXISTS `collection_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attr_name` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `collection_attributes`
--

INSERT INTO `collection_attributes` (`id`, `attr_name`, `status`) VALUES
(1, 'WF', 1),
(2, 'CC1', 1),
(3, 'CC2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `collection_attribute_groups`
--

CREATE TABLE IF NOT EXISTS `collection_attribute_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `col_group_id` int(11) NOT NULL DEFAULT '0',
  `col_attr_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `collection_attribute_groups`
--

INSERT INTO `collection_attribute_groups` (`id`, `col_group_id`, `col_attr_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `collection_attribute_metas`
--

CREATE TABLE IF NOT EXISTS `collection_attribute_metas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `col_attr_id` int(11) NOT NULL DEFAULT '0',
  `col_key` varchar(500) DEFAULT NULL,
  `col_value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `collection_attribute_metas`
--

INSERT INTO `collection_attribute_metas` (`id`, `col_attr_id`, `col_key`, `col_value`) VALUES
(1, 1, 'Wicker color', 'Red,Green,Blue'),
(2, 1, 'Wicker type', 'flat, Â½ rnd, rnd, thin rnd'),
(3, 1, 'Width', '55mm'),
(4, 1, 'Thickness', '44mm'),
(5, 1, 'Radius', '23mm'),
(6, 1, 'UV Rating in hours', '1000'),
(7, 2, 'Fabric manufacturer', 'Sunbrella, Olefin, PE'),
(8, 2, 'Fabric ID', 'NA'),
(9, 2, 'Color', 'NA'),
(10, 2, 'Seat Cushion thickness', '2â€, 3â€, 4â€, 5â€, 6â€'),
(11, 2, 'Back Cushion thickness', '2â€, 3â€, 4â€, 5â€, 6â€'),
(12, 2, 'Back Cushion type', 'blown, tailored rnd top, tailored sq top'),
(13, 2, '', ''),
(24, 3, 'Fabric manufacturer', 'Sunbrella, Olefin, PE'),
(25, 3, 'Fabric ID', 'NA'),
(26, 3, 'Color', 'NA'),
(27, 3, 'Seat Cushion thickness', '2â€, 3â€, 4â€'),
(28, 3, '', ''),
(29, 3, '', ''),
(30, 3, '', ''),
(31, 3, '', ''),
(32, 3, '', ''),
(33, 3, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `collection_groups`
--

CREATE TABLE IF NOT EXISTS `collection_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(500) DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `collection_groups`
--

INSERT INTO `collection_groups` (`id`, `group_name`, `category_id`, `status`) VALUES
(1, 'Group1', 0, 1),
(2, 'Group2', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `collection_group_sets`
--

CREATE TABLE IF NOT EXISTS `collection_group_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `set_name` varchar(500) DEFAULT NULL,
  `col_group_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `collection_group_sets`
--

INSERT INTO `collection_group_sets` (`id`, `set_name`, `col_group_id`, `category_id`, `status`) VALUES
(1, 'SET Dining', 1, 1, 1),
(2, 'SET Deep Seating', 2, 1, 1),
(3, 'South Beach', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `subject` varchar(500) DEFAULT NULL,
  `message` text,
  `reply` text,
  `created_date` date DEFAULT NULL,
  `phone` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dc_sellers`
--

CREATE TABLE IF NOT EXISTS `dc_sellers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '0',
  `dc_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `dc_sellers`
--

INSERT INTO `dc_sellers` (`id`, `seller_id`, `dc_id`) VALUES
(32, 1, 2),
(33, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `dc_vendors`
--

CREATE TABLE IF NOT EXISTS `dc_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `dc_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dc_vendors`
--

INSERT INTO `dc_vendors` (`id`, `vendor_id`, `dc_id`) VALUES
(1, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_addresses`
--

CREATE TABLE IF NOT EXISTS `delivery_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `street` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `distribution_centers`
--

CREATE TABLE IF NOT EXISTS `distribution_centers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dc_id` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(500) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `street` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `zipcode` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `street2` varchar(500) DEFAULT NULL,
  `fax` varchar(500) DEFAULT NULL,
  `contact_per_1` varchar(500) DEFAULT NULL,
  `contact_tele_1` varchar(500) DEFAULT NULL,
  `contact_email_1` varchar(500) DEFAULT NULL,
  `contact_per_2` varchar(500) DEFAULT NULL,
  `contact_tele_2` varchar(500) DEFAULT NULL,
  `contact_email_2` varchar(500) DEFAULT NULL,
  `storage_fee` varchar(500) DEFAULT NULL,
  `40lb` varchar(500) DEFAULT NULL,
  `80lb` varchar(500) DEFAULT NULL,
  `120lb` varchar(500) DEFAULT NULL,
  `per_crtn` varchar(500) DEFAULT NULL,
  `in_per_truck` varchar(500) DEFAULT NULL,
  `out_per_truck` varchar(500) DEFAULT NULL,
  `per_lable` varchar(500) DEFAULT NULL,
  `bolfree` varchar(500) DEFAULT NULL,
  `admin_fee` varchar(500) DEFAULT NULL,
  `pallet_fee` varchar(500) DEFAULT NULL,
  `stretch_fee` varchar(500) DEFAULT NULL,
  `other_fee` varchar(500) DEFAULT NULL,
  `storage_feeu` varchar(500) DEFAULT NULL,
  `40lbu` varchar(500) DEFAULT NULL,
  `80lbu` varchar(500) DEFAULT NULL,
  `120lbu` varchar(500) DEFAULT NULL,
  `per_crtnu` varchar(500) DEFAULT NULL,
  `in_per_trucku` varchar(500) DEFAULT NULL,
  `out_per_trucku` varchar(500) DEFAULT NULL,
  `per_lableu` varchar(500) DEFAULT NULL,
  `bolfreeu` varchar(500) DEFAULT NULL,
  `admin_feeu` varchar(500) DEFAULT NULL,
  `pallet_feeu` varchar(500) DEFAULT NULL,
  `stretch_feeu` varchar(500) DEFAULT NULL,
  `inquiry_note` varchar(500) DEFAULT NULL,
  `invoice` varchar(500) DEFAULT NULL,
  `invamt` varchar(500) DEFAULT NULL,
  `invdt` date DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `distribution_centers`
--

INSERT INTO `distribution_centers` (`id`, `dc_id`, `name`, `phone`, `email`, `type`, `street`, `city`, `state`, `zipcode`, `country`, `street2`, `fax`, `contact_per_1`, `contact_tele_1`, `contact_email_1`, `contact_per_2`, `contact_tele_2`, `contact_email_2`, `storage_fee`, `40lb`, `80lb`, `120lb`, `per_crtn`, `in_per_truck`, `out_per_truck`, `per_lable`, `bolfree`, `admin_fee`, `pallet_fee`, `stretch_fee`, `other_fee`, `storage_feeu`, `40lbu`, `80lbu`, `120lbu`, `per_crtnu`, `in_per_trucku`, `out_per_trucku`, `per_lableu`, `bolfreeu`, `admin_feeu`, `pallet_feeu`, `stretch_feeu`, `inquiry_note`, `invoice`, `invamt`, `invdt`, `created_by`, `user_type`) VALUES
(2, 'CA', 'USDC-CA', 9099238858, NULL, 1, '2041 South Lynx Place', 'Ontario', 'CA', '91761', 'United States', '', '', 'Ed Jmes', '9099238858', 'edjames@sbcglobal.net', 'Tong James', '9099238858', 'natalieZ168@Outlook.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '22', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, 0, 0),
(3, 'NY', 'USDC- NY', 2131234567, NULL, 2, '228 Park Avenue', 'NewYork', 'NY', '10003-1502', 'USA', '', '2137654321', 'Xavier Turdous', '2137777777', 'XTrudous@Yahoo.com', 'Sandy Trousdous', '2136666661', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, 0, 0),
(4, 'DC-VEN', 'USDC- VENDOR', 6061234567, NULL, 3, '123 Keller StreetNA', 'Memphis', 'TN', '30003-1502', 'USA', '', '6061234767', 'Allen McCall', '7171234567', 'AMcCall@Yahoo.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `answer` text,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE IF NOT EXISTS `favourites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `invoice` varchar(500) DEFAULT NULL,
  `grand_total` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `transaction_status` int(11) NOT NULL DEFAULT '0',
  `shipping_status` int(11) NOT NULL DEFAULT '0',
  `payment_mode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL,
  `description` text,
  `seller_id` int(11) NOT NULL DEFAULT '0',
  `dc_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `collection_id` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0' COMMENT '0=seller, 1=vendor',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `sale_price` float NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `base_price` float NOT NULL DEFAULT '0',
  `shipping_charge` float NOT NULL DEFAULT '0',
  `sku` varchar(500) DEFAULT NULL,
  `upc` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `seller_id`, `dc_id`, `category_id`, `collection_id`, `added_by`, `status`, `created_date`, `vendor_id`, `sale_price`, `quantity`, `base_price`, `shipping_charge`, `sku`, `upc`) VALUES
(2, 'Polywood Dining Tablefhgh', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, 2, 1, 0, 0, 1, '2017-09-15', 0, 57.84, 23, 23, 23, '234mdnf', 'gf34gb35'),
(3, 'drgb', 'qergvcdfg', 1, 2, 1, 0, 0, 1, '2017-09-15', 0, 4604.32, 2, 345, 34, '1234', '2dgv'),
(4, 'VD-test', 'test Product', 0, 4, 14, 0, 1, 1, '2017-09-21', 3, 101, 50, 56, 45, 'VD-pro-001', 'tst');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `attribute_id` int(11) NOT NULL DEFAULT '0',
  `value` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute_id`, `value`, `status`) VALUES
(20, 1, 1, 't1', 0),
(21, 1, 2, 't2', 0),
(32, 2, 1, 'testing', 0),
(33, 2, 2, 'testing', 0),
(34, 3, 1, 'etry', 0),
(35, 3, 2, '24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image_name` varchar(500) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image_name`, `type`, `status`) VALUES
(2, 1, '83910.png', 0, 0),
(4, 1, '12400.jpg', 1, 0),
(5, 1, '53593.jpg', 1, 0),
(6, 1, '21340.jpg', 1, 0),
(7, 1, '98502.jpg', 1, 0),
(8, 2, '41477.jpg', 0, 0),
(9, 2, '23056.jpg', 1, 0),
(10, 2, '15340.jpg', 1, 0),
(11, 2, '75298.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `product_type_attr_id` int(11) NOT NULL DEFAULT '0',
  `product_type_name` varchar(500) DEFAULT NULL,
  `product_type_short_name` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `category_id`, `product_type_attr_id`, `product_type_name`, `product_type_short_name`, `status`) VALUES
(2, 1, 2, 'Dining Table Rectangular', 'DTR', 1),
(3, 1, 2, 'Dining Table Square', 'DTS', 1),
(4, 1, 3, 'Dining Table Round', 'DTRD', 1),
(5, 1, 3, 'Dining Table Oval', 'DTO', 1),
(6, 1, 1, 'Dining Arm Chair', 'DAC', 1),
(7, 1, 2, 'Dining Armless Chair', 'DC', 1),
(8, 1, 2, 'Bench', 'B', 1),
(9, 1, 2, 'End table', 'ET', 1),
(10, 1, 2, 'Coffee table', 'CT', 1),
(11, 1, 1, 'Chaise Lounge', 'CL', 1),
(12, 1, 1, 'Club Chair', 'CC', 1),
(13, 1, 1, 'Swivel Club Rocker', 'SCR', 1),
(14, 1, 1, 'Club Spring Rocker', 'CSR', 1),
(15, 1, 1, 'Love Seat', 'LS', 0),
(16, 1, 1, 'Sofa', 'S', 1),
(17, 1, 1, 'Ottoman', 'O', 1),
(18, 1, 0, 'Sectional - Center Armless', 'CA', 1),
(19, 1, 0, 'Sectional - Corner', 'C', 1),
(20, 1, 1, 'Sectional - Right Arm', 'RA', 1),
(21, 1, 1, 'Sectional - Left Arm', 'LA', 1),
(22, 1, 1, 'Sectional - Right Arm Love Seat', 'RALS', 1),
(23, 1, 1, 'Sectional - Left Arm Love Seat', 'LALS', 1),
(24, 1, 2, 'Bar Table', 'BT', 1),
(25, 1, 2, 'Bar Table - Counter- High', 'BTC', 1),
(26, 1, 1, 'Bar Chair', 'BC', 1),
(27, 1, 1, 'Bar Chair- Counter - High', 'BCC', 1),
(28, 1, 1, 'Bar Stool', 'BS', 1),
(29, 1, 1, 'Day Bed', 'DB', 1),
(30, 1, 2, 'Swing', 'SW', 1),
(31, 1, 2, 'Hammock', 'H', 1),
(32, 1, 0, 'Fire Pit', 'FP', 1),
(33, 1, 0, 'Accessories', 'AC', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_type_attributes`
--

CREATE TABLE IF NOT EXISTS `product_type_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_type_attributes`
--

INSERT INTO `product_type_attributes` (`id`, `name`, `status`) VALUES
(1, 'ED1', 1),
(2, 'ED2', 1),
(3, 'ED3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_type_attribute_fields`
--

CREATE TABLE IF NOT EXISTS `product_type_attribute_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(500) NOT NULL,
  `product_type_attr_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `product_type_attribute_fields`
--

INSERT INTO `product_type_attribute_fields` (`id`, `field_name`, `product_type_attr_id`) VALUES
(1, 'Wt', 1),
(2, 'H', 1),
(3, 'W', 1),
(4, 'D', 1),
(5, 'H2Seat', 1),
(6, 'H2Arm', 1),
(7, 'H2Back', 1),
(8, 'Wt', 2),
(9, 'H', 2),
(10, 'W', 2),
(11, 'D', 2),
(12, 'Wt', 3),
(13, 'H', 3),
(14, 'W', 3),
(15, 'Dia', 3);

-- --------------------------------------------------------

--
-- Table structure for table `review_ratings`
--

CREATE TABLE IF NOT EXISTS `review_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `review` text,
  `rating` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE IF NOT EXISTS `sellers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` varchar(500) DEFAULT NULL,
  `username` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `phone` bigint(20) NOT NULL DEFAULT '0',
  `street1` varchar(500) DEFAULT NULL,
  `verify_status` int(11) NOT NULL DEFAULT '0',
  `street2` varchar(500) DEFAULT NULL,
  `telephone` varchar(500) DEFAULT NULL,
  `fax` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `province` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `export_city` varchar(500) DEFAULT NULL,
  `country_manufacture` varchar(500) DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `contact_person1` varchar(500) DEFAULT NULL,
  `contact_tele1` varchar(500) DEFAULT NULL,
  `contact_email1` varchar(500) DEFAULT NULL,
  `contact_person2` varchar(500) DEFAULT NULL,
  `contact_tele2` varchar(500) DEFAULT NULL,
  `contact_email2` varchar(500) DEFAULT NULL,
  `dc_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `seller_id`, `username`, `email`, `password`, `status`, `created_date`, `image`, `phone`, `street1`, `verify_status`, `street2`, `telephone`, `fax`, `city`, `province`, `country`, `zip`, `export_city`, `country_manufacture`, `category_id`, `contact_person1`, `contact_tele1`, `contact_email1`, `contact_person2`, `contact_tele2`, `contact_email2`, `dc_id`) VALUES
(1, 'LHX', 'Linhai Xinghe Arts & Crafts Co., Ltd.', 'seller@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, '2017-08-28', '1505403807-adminDefault.png', 186123456789016, 'test', 1, '', '18169999998888887777', '', 'Linhai', 'Juazhang', 'China', '110044', 'Ningbo', 'CN', 1, 'James Yu', '18165554443332227778', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

CREATE TABLE IF NOT EXISTS `shipping_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `street1` varchar(500) DEFAULT NULL,
  `street2` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `contact_phone` bigint(20) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_agents`
--

CREATE TABLE IF NOT EXISTS `shipping_agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sa_id` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `telephone` varchar(500) DEFAULT NULL,
  `fax` varchar(500) DEFAULT NULL,
  `street1` varchar(500) DEFAULT NULL,
  `street2` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `contact_person_1` varchar(500) DEFAULT NULL,
  `contact_tele_1` varchar(500) DEFAULT NULL,
  `contact_email_1` varchar(500) DEFAULT NULL,
  `contact_person_2` varchar(500) DEFAULT NULL,
  `contact_tele_2` varchar(500) DEFAULT NULL,
  `contact_email_2` varchar(500) DEFAULT NULL,
  `term_of_agreement` varchar(500) DEFAULT NULL,
  `rates` float NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `shipping_agents`
--

INSERT INTO `shipping_agents` (`id`, `sa_id`, `name`, `email`, `password`, `telephone`, `fax`, `street1`, `street2`, `city`, `state`, `country`, `zip`, `contact_person_1`, `contact_tele_1`, `contact_email_1`, `contact_person_2`, `contact_tele_2`, `contact_email_2`, `term_of_agreement`, `rates`, `status`, `created_date`) VALUES
(2, 'SA-007', 'Shipping Agent 007', 'shipping@gmail.com', '0e3117a1f7105e311edb71112bfa816e', '9099238858', '2133333333', '1421 Rosemead Blvd', '', 'Rosemead', NULL, 'United States', '90023', 'Lawrence', '2131234567', 'SA@Yahoo.com', '', '', '', '', 0, 1, '2017-09-30');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE IF NOT EXISTS `transaction_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `txn_id` varchar(500) DEFAULT NULL,
  `grand_total` double NOT NULL DEFAULT '0',
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `phone` bigint(20) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `response` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `role` int(11) NOT NULL DEFAULT '0',
  `image` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `role`, `image`) VALUES
(1, 'admin', 'admin@gmail.com', '123456', 1, 0, '1505403856-adminDefault.png'),
(2, 'globiz', 'yogesh.k@globiztechnology.com', '12345', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` varchar(500) DEFAULT NULL,
  `username` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `phone` bigint(20) NOT NULL DEFAULT '0',
  `street1` varchar(500) DEFAULT NULL,
  `verify_status` int(11) NOT NULL DEFAULT '0',
  `street2` varchar(500) DEFAULT NULL,
  `telephone` varchar(500) DEFAULT NULL,
  `fax` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `province` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `zip` varchar(500) DEFAULT NULL,
  `export_city` varchar(500) DEFAULT NULL,
  `country_manufacture` varchar(500) DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `contact_person1` varchar(500) DEFAULT NULL,
  `contact_tele1` varchar(500) DEFAULT NULL,
  `contact_email1` varchar(500) DEFAULT NULL,
  `contact_person2` varchar(500) DEFAULT NULL,
  `contact_tele2` varchar(500) DEFAULT NULL,
  `contact_email2` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `vendor_id`, `username`, `email`, `password`, `status`, `created_date`, `image`, `phone`, `street1`, `verify_status`, `street2`, `telephone`, `fax`, `city`, `province`, `country`, `zip`, `export_city`, `country_manufacture`, `category_id`, `contact_person1`, `contact_tele1`, `contact_email1`, `contact_person2`, `contact_tele2`, `contact_email2`) VALUES
(3, 'VEN1', 'Vendor', 'vendor@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, '2017-09-19', NULL, 5051234567, '123 Vendor Place', 1, '', '5057654321', '5055555555', 'Vendor City', '', 'USA', '40020-1234', 'Tulane', 'USA', 3, 'Sam Smith', '7071234567', 'SSVEN@Yahoo.com', 'MAry@Yahoo.com', '7077654321', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
