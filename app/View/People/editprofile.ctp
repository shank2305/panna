
<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
.pushes
{
  margin-bottom: 42px!important;
}
.small
{
  font-size: 133%;
}
#user_exp
{
  margin-bottom: 3%;
}
#user_cert
{
  margin-bottom: 3%;
}
#err
{
    margin-bottom: 3%;
    color: red;
    display: none;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="" enctype="multipart/form-data">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Profile</div>
      </div>
      <div class="pull-right"> <a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" id="myTabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a class="refresh" href="#personal-details">Personal Details</a> </li>
           <li class=""> <a class="refresh" href="#education">Education</a> </li>
           <li class=""> <a class="refresh" href="#experience">Experience</a> </li>
           <li class=""> <a class="refresh" href="#certifications">Certifications</a> </li>
        </ul>

        <div class="block-content tab-content">
          <!--Tab 1:- Personal Details-->
          <div class="tab-pane fade in active" id="personal-details">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-name" name="data[User][name]" placeholder="Name" value="<?php echo @$editUser['User']['name']?>" required>
                  <input type="hidden" name="data[User][id]" value="<?php echo @$editUser['User']['id']?>">
                </div>
                 <div class="form-group">
                  <label class="control-label" >Mobile<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-mobile" maxlength="10" minlength="10" name="data[User][mobile]" placeholder="Mobile" value="<?php echo @$editUser['User']['mobile']?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label" >Martial Status<span class="text-danger">*</span></label>
                  <select class="form-control required" id="val-marriage" name="data[User][martial_status]" required>
                    <option value="0" <?php if(@$editUser['User']['martial_status'] == 0) { ?> selected <?php } ?>>Unmarried</option>
                    <option value="1" <?php if(@$editUser['User']['martial_status'] == 1) { ?> selected <?php } ?>>Married</option>
                  </select>
                </div>
                
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Email<span class="text-danger">*</span></label>
                  <input class="form-control required email" type="text" id="val-email" name="data[User][email]" placeholder="Email" value="<?php echo @$editUser['User']['email']?>" readonly required>
                </div>
                <div class="form-group">
                  <label class="control-label" >Age<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-age" name="data[User][age]" placeholder="Age" value="<?php echo @$editUser['User']['age']?>" required>
                </div>
                <div class="form-group">
                  <a class="btn btn-primary pull-right" style="margin-top: 4%;" id="continue1" href="#">Continue</a>
                </div>
              </div>
            </div>
          </div>
          <!--Tab 2:- Education Details-->
          <div class="tab-pane fade" id="education">
            <div class="h2 page-heading push">Upload Marksheets</div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Class 10th School<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-tenschool" name="data[Education][tenschool]" placeholder="Enter class 10th School" value="<?php echo @$editUser['Education']['tenschool']?>" required>
                   <input type="hidden" name="data[Education][user_id]" value="<?php echo @$editUser['User']['id']?>">
                   <input type="hidden" name="data[Education][id]" value="<?php echo @$editUser['Education']['id']?>">
                </div>
                <div class="form-group">
                  <label class="control-label">Class 10th Passing Year<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-tenschoolyear" name="data[Education][tenschoolyear]" placeholder="Enter class 10th passing year" value="<?php echo @$editUser['Education']['tenschoolyear']?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Upload Marksheet<span class="text-danger"><?php if(empty(@$editUser['Education']['tenschoolmarksheet'])){ ?>*<?php } ?></span></label>
                  <input type="file" class="form-control required"  name="data[Education1][tenschoolmarksheet]" value="" <?php if(empty(@$editUser['Education']['tenschoolmarksheet'])){ ?> id="val-tenschoolmarks" required <?php } ?>>
                  <?php if(!empty(@$editUser['Education']['tenschoolmarksheet'])){ ?>
                  <input type="hidden" id="val-tenschoolmarks" value="1">
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                  <label class="control-label">Class 12th School<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-12school" name="data[Education][twelveschool]" placeholder="Enter class 12th School" value="<?php echo @$editUser['Education']['twelveschool']?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Class 12th Passing Year<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-12schoolyear" name="data[Education][twelveschoolyear]" placeholder="Enter class 12th passing year" value="<?php echo @$editUser['Education']['twelveschoolyear']?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Upload Marksheet<span class="text-danger"><?php if(empty(@$editUser['Education']['twelveschoolmarksheet'])){ ?>*<?php } ?></span></label>
                  <input type="file" class="form-control required"  name="data[Education1][twelveschoolmarksheet]" <?php if(empty(@$editUser['Education']['twelveschoolmarksheet'])){ ?> id="val-12schoolmarks" required <?php } ?>>
                  <?php if(!empty(@$editUser['Education']['twelveschoolmarksheet'])){ ?>
                  <input type="hidden" id="val-12schoolmarks" value="1">
                  <?php } ?>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">College of Graduation<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-grad" name="data[Education][gradcollege]" placeholder="Enter College" value="<?php echo @$editUser['Education']['gradcollege']?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Graduation Passing Year<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-gradyear" name="data[Education][gradyear]" placeholder="Enter passing year" value="<?php echo @$editUser['Education']['gradyear']?>" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Upload Marksheet<span class="text-danger"><?php if(empty(@$editUser['Education']['gradmarksheet'])){ ?>*<?php } ?></span></label>
                  <input type="file" class="form-control required"  name="data[Education1][gradmarksheet]" <?php if(empty(@$editUser['Education']['gradmarksheet'])){ ?> id="val-gradmarks" required <?php } ?>>
                  <?php if(!empty(@$editUser['Education']['gradmarksheet'])){ ?>
                  <input type="hidden" id="val-gradmarks" value="1">
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                  <label class="control-label">College of Post Graduation<span class="text-danger"></span></label>
                  <input class="form-control" type="text" id="val-postgrad" name="data[Education][postgradcollege]" placeholder="Enter College" value="<?php echo @$editUser['Education']['postgradcollege']?>">
                </div>
                <div class="form-group">
                  <label class="control-label">Post Graduation Passing Year<span class="text-danger"></span></label>
                  <input class="form-control" type="text" id="val-postgradyear" name="data[Education][postgradyear]" placeholder="Enter passing year" value="<?php echo @$editUser['Education']['postgradcollege']?>">
                </div>
                <div class="form-group">
                  <label class="control-label">Upload Marksheet<span class="text-danger"></span></label>
                  <input type="file" class="form-control required" id="postgradmarks" name="data[Education1][postgradmarksheet]">
                </div>
                <div class="form-group">
                  <a class="btn btn-primary pull-right" style="margin-top: 4%; margin-bottom: 4%;" id="continue2" href="#">Continue</a>
                </div>
              </div>
            </div>
          </div>
          <!--Tab 3:- Experience Details-->
          <div class="tab-pane fade" id="experience">
            <div class="pull-right"><a class="btn btn-primary pull-right" id="continue3" href="#">Continue</a><a id="add_butt" class="btn btn-success">+</a></div>
            <div class="h2 page-heading pushes">Enter Experience details</div>
            <?php if(!empty(@$editUser['Experience'])){ ?>
            <div class="page-heading small">Your Experiences</div>
              <table width="50%" id="user_exp">
                <?php $i=1; foreach($editUser['Experience'] as $exp){ ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $exp['company']; ?><td>
                  <td><?php echo $exp['job_desc']; ?><td>
                  <td><?php echo $exp['date_from'] ."-". $exp['date_to']; ?><td>
                  <td><a title="Delete Experience" href="<?php echo HTTP_ROOT.'People/deleteexp/'.base64_encode($exp['id'])?>"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i></a></td>
                </tr>
                <?php $i++; } ?>
              </table>
            <?php } ?>
              <div id="exp">
                <div class="row" id="add">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Company<span class="text-danger"></span></label>
                      <input class="form-control" type="text" name="data[Experience][0][company]" placeholder="Enter Company Name" value="">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Upload experience letter<span class="text-danger"></span></label>
                      <input type="file" name="data[Experience1][0][document]" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="control-label" >Date From<span class="text-danger"></span></label>
                      <div class='input-group date' id='datetimepickerx'>
                        <input class="form-control" type="text" id="val-from" name="data[Experience][0][date_from]" placeholder="YYYY-MM-DD" value="">
                        <span class="input-group-addon">
                          <span class="fa fa-calendar">
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Job Description<span class="text-danger"></span></label>
                      <input class="form-control" type="text" name="data[Experience][0][job_desc]" placeholder="Enter Job Description" value="">
                    </div>

                    <div class="form-group" style="padding-top: 78px;">
                      <label class="control-label" >Date Till<span class="text-danger"></span></label>
                      <div class='input-group date' id='datetimepickery'>
                        <input class="form-control" type="text" id="val-to" name="data[Experience][0][date_to]" placeholder="YYYY-MM-DD" value="">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar">
                            </span> 
                          </span>
                        </div>
                    </div>
                    <div class="form-group">
                      
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <!--Tab 4:- Cerifications Details-->
          <div class="tab-pane fade" id="certifications">
            <div class="pull-right"><button type="submit" id="sve" class="btn btn-primary pull-right">Save</button><a id="add_butt_again" class="btn btn-success">+</a></div>
            <div class="h2 page-heading push">Enter Certification details</div>
            <div id="err">
              You may have missed some required fields!
            </div>
            <?php if(!empty(@$editUser['Certification'])){ ?>
            <div class="page-heading small">Your Certifications</div>
              <table width="50%" id="user_cert">
                <?php $i=1; foreach($editUser['Certification'] as $cert){ ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $cert['Domain']['domain']; ?><td>
                  <td><?php echo "<b>From: </b>" .$cert['authority']; ?><td>
                  <td><a title="Delete Certification" href="<?php echo HTTP_ROOT.'People/deletecert/'.base64_encode($cert['id'])?>"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i></a></td>
                </tr>
                <?php $i++; } ?>
              </table>
            <?php } ?>
              <div id="exp2">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Certification Name<span class="text-danger"></span></label>
                      <select name="data[Certification][0][domain_id]" class="form-control">
                          <option>Select</option>
                        <?php foreach($domains as $domain){ ?>
                          <option value="<?php echo $domain['Domain']['id']; ?>"><?php echo $domain['Domain']['domain']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Certification Authority<span class="text-danger"></span></label>
                      <input class="form-control" type="text" name="data[Certification][0][authority]" placeholder="Enter Certification Name" value="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Upload Certificate<span class="text-danger"></span></label>
                      <input type="file" name="data[Certification1][0][document]" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        
        </div>
      </div>
    </div>
  </form>
  </main>
  <!-- END Main Container -->
    
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    $(document).ready(function(){
      $('#cform').removeAttr("novalidate");
    });
    </script>
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_wizard.js"></script>
    <script src="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript">
        $(function () {
            $('#datetimepickerx').datetimepicker({
              format:'YYYY-MM-DD'
            });
            $('#datetimepickery').datetimepicker({
              format:'YYYY-MM-DD'
            });
        });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $(".refresh").click(function () {
          $("#err").hide();
        });
      });
      $(document).ready(function(){
        $("#sve").click(function () {
          if($('#val-tenschool').val() == '' || $('#tenschoolyear').val() == '' || $('#val-12school').val() == '' || $('#val-12schoolyear').val() == '' || $('#val-grad').val() == '' || $('#val-gradyear').val() == '' || $('#val-name').val() == '' || $('#val-marriage').val() == '' || $('#val-mobile').val() == '' || $('#val-age').val() == '')
          {
            $("#err").show();
          }
        });
      });
      $(document).ready(function(){
        var i = 0;
        $("#add_butt").click(function () {
          //console.log('Yay! It works!');
          i++;
           $("#exp").append('<hr><div class="row" id="add"><div class="col-md-6"><div class="form-group"><label class="control-label">Company<span class="text-danger"></span></label><input class="form-control" type="text" id="" name="data[Experience]['+ i +'][company]" placeholder="Enter Company Name" value="<?php echo @$editUser['Experience']['0']['company']?>"></div><div class="form-group"><label class="control-label">Upload experience letter<span class="text-danger"></span></label><input type="file" name="data[Experience1]['+ i +'][document]" class="form-control"></div><div class="form-group"><label class="control-label" >Date From<span class="text-danger"></span></label><div class="input-group date" id="datetimepicker'+i+'"><input class="form-control" type="text" id="val-'+i+'" name="data[Experience]['+ i +'][date_from]" placeholder="YYYY-MM-DD" value="<?php echo @$editUser['Experience']['0']['date_from']?>"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Job Description<span class="text-danger"></span></label><input class="form-control" type="text" id="" name="data[Experience]['+ i +'][job_desc]" placeholder="Enter Job Description" value="<?php echo @$editUser['Experience']['0']['job_desc']?>"></div><div class="form-group" style="padding-top: 78px;"><label class="control-label" >Date Till<span class="text-danger"></span></label><div class="input-group date" id="datetimepicker'+i+i+'"><input class="form-control" type="text" id="val-'+i+'" name="data[Experience]['+ i +'][date_to]" placeholder="YYYY-MM-DD" value="<?php echo @@$editUser['Experience']['0']['date_to']?>"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div></div></div>');
        });
          $('#datetimepicker'+i).datetimepicker({
              format:'YYYY-MM-DD'
            });
            $('#datetimepicker2'+i+i).datetimepicker({
              format:'YYYY-MM-DD'
            });
      });
       $(document).ready(function(){
        var z = 0;
        $("#add_butt_again").click(function () {
          //console.log('Yay! It works!');
          z++;
           $("#exp2").append('<hr><div class="row"><div class="col-md-6"><div class="form-group"><label class="control-label">Certification Name<span class="text-danger"></span></label><select name="data[Certification]['+ z +'][domain_id]" class="form-control"><?php foreach($domains as $domain){ ?><option value="<?php echo $domain['Domain']['id']; ?>"><?php echo $domain['Domain']['domain']; ?></option><?php } ?></select></div><div class="form-group"><label class="control-label">Certification Authority<span class="text-danger"></span></label><input class="form-control" type="text" id="" name="data[Certification]['+ z +'][authority]" placeholder="Enter Certification Name" value="<?php echo @$editUser['Certification']['0']['authority']?>"></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Upload Certificate<span class="text-danger"></span></label><input type="file" name="data[Certification1]['+ z +'][document]" class="form-control"></div></div></div>');
        });
      });
       $(document).ready(function(){
        $('#continue1').click(function() {
          $('#myTabs a[href="#education"]').tab('show');
        });
        $('#continue2').click(function() {
          $('#myTabs a[href="#experience"]').tab('show');
        });
        $('#continue3').click(function() {
          $('#myTabs a[href="#certifications"]').tab('show');
        });
        });
    </script>