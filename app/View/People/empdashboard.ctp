 <main id="main-container"> 
    
    <div class="content">
    	<div class="row">
	    	<div class="col-md-2 options"><!-- <a href="<?php echo HTTP_ROOT?>People/editprofile/<?php echo base64_encode($this->Session->read('User.id')) ?>"><img class="ico" src="../img/avatar.png"></a><br>
	    		<span><b><a class="btn btn-primary" href="<?php echo HTTP_ROOT?>People/editprofile/<?php echo base64_encode($this->Session->read('User.id')) ?>">Add/Edit Profile</a></b></span> -->
	    	</div>
	    	<div class="col-md-8">
                <div class="table1">
    	    		<table width="100%">
                        <tbody>
                            <tr>
                                <td><b>Name:</b></td>
                                <td><?php echo $editUser['User']['name']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Email:</b></td>
                                <td><?php echo $editUser['User']['email']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Mobile:</b></td>
                                <td><?php echo $editUser['User']['mobile']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Age:</b></td>
                                <td><?php echo $editUser['User']['age']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table1" style="margin-top: 2%;">
                    <table width="100%;">
                        <tr>
                            <td colspan="3" style="padding-bottom: 2%;"><h4><i class="fa fa-university " aria-hidden="true"></i>   Education</h4></td>
                        </tr>
                        <tr>
                            <td><b>Class 10th School</b></td>
                            <td><?php echo $editUser['Education']['tenschool']; ?></td>
                            <td><b>Passing Year:</b></td>
                            <td><?php echo $editUser['Education']['tenschoolyear']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Class 12th School</b></td>
                            <td><?php echo $editUser['Education']['twelveschool']; ?></td>
                            <td><b>Passing Year:</b></td>
                            <td><?php echo $editUser['Education']['twelveschoolyear']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Graduation</b></td>
                            <td><?php echo $editUser['Education']['gradcollege']; ?></td>
                            <td><b>Passing Year:</b></td>
                            <td><?php echo $editUser['Education']['gradyear']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Post Graduation</b></td>
                            <td><?php echo $editUser['Education']['postgradcollege']; ?></td>
                            <td><b>Passing Year:</b></td>
                            <td><?php echo $editUser['Education']['postgradyear']; ?></td>
                        </tr>

                        <tr>
                            <td colspan="3" style="padding-bottom: 2%;padding-top: 2%;"><h4><i class="fa fa-industry" aria-hidden="true"></i>   Experience</h4></td>
                        </tr>
                        <?php foreach($editUser['Experience'] as $expr){ ?>
                        <tr>
                            <td><b>Company</b></td>
                            <td><?php echo $expr['company']; ?></td>
                            <td><?php echo $expr['job_desc']; ?></td>
                            <td><?php echo $expr['date_from']." - ".$expr['date_to']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="3" style="padding-bottom: 2%;padding-top: 2%;"><h4><i class="fa fa-graduation-cap" aria-hidden="true"></i>   Certifications</h4></td>
                        </tr>
                        <?php foreach($editUser['Certification'] as $cert){ ?>
                        <tr>
                            <td><b>Course</b></td>
                            <td><?php echo $cert['DomainName']['domain']; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
	    	</div>
    	</div>
    </div>
</main>
<style type="text/css">
	.ico
	{
		width:50%;
        margin-bottom: 4%;
	}
	.options
	{
		text-align: center;
	}
	.table1
	{
		background-color: white;
        padding: 2.5%;
	}
	table
	{
		margin: 2%;
	}
	.fa-times
	{
		color:red;
	}
	.fa-check
	{
		color: green;
	}
    #main-container
    {
        background-image: url("../img/492571003.jpg")!important;
    }
</style>
