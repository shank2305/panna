<style>.editing{margin-top: 5%;}
.bg-white{background: lightgrey!important;}
.block-header
{
    background: #2C343F;
    color: white!important;
}
</style>
  <!-- Main Container -->
  <main id="main-container">
    <div class="content push-15">
      <div class="h2 page-heading animated zoomIn">Dashboard</div>
    </div>
    
    <!-- Stats -->
    <div class="content bg-white border-b">
      <div class="row items-push text-uppercase">
        <div class="col-xs-6 col-sm-3">
          <div class="font-w700 text-gray-darker animated fadeIn">Pending Requests</div>
          <div class="text-muted animated fadeIn"><small><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Unevaluated</small></div>
          <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo HTTP_ROOT . 'People/pending'?>">Click</a> </div>
        <div class="col-xs-6 col-sm-3">
          <div class="font-w700 text-gray-darker animated fadeIn">Already Evaluated</div>
          <div class="text-muted animated fadeIn"><small><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Evaluated</small></div>
          <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo HTTP_ROOT . 'People/evaluated'?>">Click</a> </div>
        <!-- <div class="col-xs-6 col-sm-3">
          <div class="font-w700 text-gray-darker animated fadeIn">Total Earnings</div>
          <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> All Time</small></div>
          <a class="h2 font-w300 text-primary animated flipInX" href="base_comp_charts.html">$ 93,880</a> </div>-->
        <div class="col-xs-6 col-sm-3 pull-right editing">
          <a href="<?php echo HTTP_ROOT . 'People/editpro/' . base64_encode($this->Session->read('User.id'))?>" class="btn btn-primary">Edit Profile</a>
      </div>
    </div>
    <!-- END Stats --> 
    <hr>
    <!-- Page Content -->
    <div class="content">
      <div class="row">
        
        
      </div>
      <div class="row">
        <div class="col-lg-8"> 
          <!-- News -->
          <div class="block">
            <div class="block-header">
              <ul class="block-options">
                <li>
                  <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                </li>
              </ul>
              <h3 class="block-title">Words to live by!</h3>
            </div>
            <div class="block-content bg-gray-lighter"> 
              <!-- Slick slider (.js-slider class is initialized in App() -> uiHelperSlick()) --> 
              <!-- For more info and examples you can check out http://kenwheeler.github.io/slick/ -->
              <div class="js-slider remove-margin-b" data-slider-autoplay="true" data-slider-autoplay-speed="4000">
                <div>
                  <blockquotation>
                    <p>Be yourself; everyone else is already taken.</p>
                    <footer>Oscar Wilde</footer>
                  </blockquotation>
                </div>
                <div>
                  <blockquotation>
                    <p>Don't cry because it's over, smile because it happened.</p>
                    <footer>Dr. Seuss</footer>
                  </blockquotation>
                </div>
                <div>
                  <blockquotation>
                    <p>Strive not to be a success, but rather to be of value.</p>
                    <footer>Albert Einstein</footer>
                  </blockquotation>
                </div>
                <div>
                  <blockquotation>
                    <p>Every child is an artist. The problem is how to remain an artist once he grows up.</p>
                    <footer>Pablo Picasso</footer>
                  </blockquotation>
                </div>
                <div>
                  <blockquotation>
                    <p>There is only one way to avoid criticism: do nothing, say nothing, and be nothing.</p>
                    <footer>Aristotle</footer>
                  </blockquotation>
                </div>
              </div>
              <!-- END Slick slider --> 
            </div>
            
          </div>
          <!-- END News --> 
        </div>
        <div class="col-lg-4"> 
        </div>
      </div>
    </div>
    <!-- END Page Content --> 
  </main>
  <!-- END Main Container -->
  <script src="<?php echo HTTP_ROOT ?>js/pages/base_pages_dashboard.js"></script>