  <style>
  .pagination > li > span {
    float: left;
  }
  table, th, td
  {
    text-align: center;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Pending Evaluations</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Summary</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading"></div>
              </div>
                     
              <div class="clearfix"></div>
            </div>
            <table class="dataTable table-hover">
              <thead>
                      <tr role="row">
                          <th>S.No.<i class="sorting_icon"></i></th>
                          <th>Name<i class="sorting_icon"></i></th>
                          <th>Email<i class="sorting_icon"></i></th>
                          <th>Mobile<i class="sorting_icon"></i></th>
                          <th>Age<i class="sorting_icon"></i></th>
                          <th>Eval Status<i class="sorting_icon"></i></th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $i=1; foreach($finals as $user){?>
                        <tr class="odd">
                            <td><?php echo $i;?></td>
                            <td><?php echo $user['User']['name']?></td>

                            <td><?php echo $user['User']['email']?></td>

                            
                            <td><?php echo $user['User']['mobile']?></td>
                           
                            <td>
                             <?php echo $user['User']['age']; ?>
                            </td>
                            <td><?php if($user['User']['evaluation_status'] == 1) { ?>
                              <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>
                              <?php } else if($user['User']['evaluation_status'] == 2) { ?>
                              <i class="fa fa-circle" aria-hidden="true" style="color: orange;""></i>
                               <?php } else if($user['User']['evaluation_status'] == 3) { ?>
                              <i class="fa fa-circle" aria-hidden="true" style="color: green;""></i>
                               <?php } ?>
                            </td>
                            <td>
                              <a title="Evaluate" href="<?php echo HTTP_ROOT.'People/evaluate/'.base64_encode($user['User']['id'])?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php $i++; }?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
<style type="text/css">
  .fa-check
  {
    color: green;
  }
  .fa-times
  {
    color: red;
  }
</style>