  <!-- Main Container -->
  <style>
    .border-line{
          width: 8%;
    background: #025192;
    display: inline-block;
    margin-bottom: 20px;
    height: 3px;
    }
    .row{
      width: 100%;
      margin-left: 0px;
    }
    #main-container
    {
      background-image: url("../panna/img/back.jpg");
    }
  </style>
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="text-center">
            <div class="h2 page-heading" style="color:white;">Login</div>
            <span class="border-line"></span>
          </div>
          
          <div class="clearfix"></div>
     	</div>
     
        
        <div class="row">
         <div class="col-md-4 col-md-offset-4">
           <div class="block block-bordered">       
            <div class="form-horizontal block-content">
                
               
                      <form method="post" id="cform">
                      <div class="row">
                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Email</label>
                          </div>
                          <div class="col-md-12">
                              <input class="form-control required email" type="text" name="data[User][email]" value="" maxlength="255">
                          </div>
                          </div>
                      </div>
                      
                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Password</label>
                          </div>
                          <div class="col-md-12">
                              <input class="form-control required" type="password" name="data[User][password]" value="" maxlength="50">
                          </div>
                         </div> 
                      </div>
                      
                      
                      <div class="form-group">
                          <div class=" col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Submit</button>
                              <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                          </div>
                          <div class="sign" style="">New Employee?  <a href="<?php echo HTTP_ROOT.'People/signup'?>">SignUp</a></div>
                      </div>
                      </div>
                      </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>
  
  </main>
  <!-- END Main Container --> 
  <style type="text/css">
    .sign
    {
      text-align: center;
      margin-top: 12%; 
    }
  </style>