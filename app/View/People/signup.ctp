  <!-- Main Container -->
  <style>
    .border-line{
    width: 8%;
    background: #025192;
    display: inline-block;
    margin-bottom: 20px;
    height: 3px;
    }
    .row{
      width: 100%;
      margin-left: 0px;
    }
    #main-container
    {
      background-image: url("../img/back.jpg");
    }
    .otp
    {
      display: none;
    }
    .butt
    {
      display: none;
    }
  </style>
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="text-center">
            <div class="h2 page-heading" style="color: white;">SignUp</div>
            <span class="border-line"></span>
          </div>
          
          <div class="clearfix"></div>
     	</div>
     
        
        <div class="row">
         <div class="col-md-4 col-md-offset-4">
           <div class="block block-bordered">       
            <div class="form-horizontal block-content">
                
               
                      <form method="post" id="cform">
                      <div class="row">
                      
                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Email</label>
                          </div>
                          <div class="col-md-12">
                              <input class="form-control required email" type="text" name="data[User][email]" value="" maxlength="255">
                          </div>
                          </div>
                      </div>

                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Mobile</label>
                          </div>
                          <div class="col-md-10">
                              <input class="form-control number required" id="mobile" type="text" name="data[User][mobile]" value="" minlength="10" maxlength="10">
                              <input class="form-control" id="mobile_stat" type="hidden" name="data[User][mobile_status]" value="1" minlength="10" maxlength="10">
                          </div>
                          <div class="col-md-2 butt" style="padding-left: 0px;">
                            <a id="otp_button" class="btn btn-danger">OTP</a>
                          </div>
                          </div>
                      </div>

                      <div class="form-group otp">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">OTP</label>
                          </div>
                          <div class="col-md-12">
                              <input id="otp_val" class="form-control" type="text" name="" value="" maxlength="255">
                          </div>
                          </div>
                      </div>
                      
                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Password</label>
                          </div>
                          <div class="col-md-12">
                              <input class="form-control required" type="password" name="data[User][password]" value="" maxlength="50">
                          </div>
                         </div> 
                      </div>

                      <div class="form-group">
                      <div class="row">
                          <div class="col-md-12">
                              <label class="control-label">Preference</label>
                          </div>
                          <div class="col-md-12">
                              <label class="checkbox-inline"><input class="required " type="radio" name="data[User][user_type]" value="2">Consultant</label>
                              <label class="checkbox-inline"><input class="required" type="radio" name="data[User][user_type]" value="0">Evaluator</label>
                          </div>
                          </div>
                      </div>
                      
                      <div class="form-group">
                          <div class=" col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Submit</button>
                              <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                          </div>
                      </div>
                      </div>
                      </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    <script type="text/javascript">
    $(document).ready(function(){
      $("#mobile").keyup(function(){
        var len = $(this).val().length;
        if(len == 10)
        $(".butt").show();
      });
      $("#otp_button").click(function(){
        $(".otp").show();
      });
    });
  </script>
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>
  
  </main>
  <!-- END Main Container --> 
  <style type="text/css">
    .sign
    {
      text-align: center;
      margin-top: 12%; 
    }
  </style>
   
 <!--On successful OTP verification, set valur of mobile_stat as 1-->