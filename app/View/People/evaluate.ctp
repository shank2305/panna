
<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
.note
{
    margin-top: 3%;
    font-size: 75%;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="" enctype="multipart/form-data">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Profile</div>
      </div>
      <div class="pull-right"> <button type="submit" class="btn btn-primary">Save</button> <a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#personal-details">Personal Details</a> </li>
           <li class=""> <a href="#education">Education</a> </li>
           <li class=""> <a href="#experience">Experience</a> </li>
           <li class=""> <a href="#certifications">Certifications</a> </li>
           <li class=""> <a href="#remarks">Remarks</a> </li>
        </ul>
        <div class="block-content tab-content">
          <!--Tab 1:- Personal Details-->
          <div class="tab-pane fade in active" id="personal-details">
            <table class="dataTable table-hover">
              <tbody>
                <tr>
                  <td><b>Name:</b></td>
                  <td><?php echo $user['User']['name']; ?></td>
                </tr>
                <tr>
                  <td><b>Email:</b></td>
                  <td><?php echo $user['User']['email']; ?></td>
                </tr>
                <tr>
                  <td><b>Mobile:</b></td>
                  <td><?php echo $user['User']['mobile']; ?></td>
                </tr>
                <tr>
                  <td><b>Age:</b></td>
                  <td><?php echo $user['User']['age']; ?></td>
                </tr>
                <tr>
                  <td><b>Evaluation Status:</b></td>
                  <td>
                    <?php 
                      switch($user['User']['evaluation_status'])
                      {
                        case 1:
                        echo '<i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>';
                        break;
                        case 2:
                        echo '<i class="fa fa-circle" aria-hidden="true" style="color: orange;"></i>';
                        break;
                        case 3:
                        echo '<i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>';
                        break;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--Tab 2:- Education Details-->
          <div class="tab-pane fade" id="education">
            <div class="h2 page-heading push">Marksheets</div>
            <table class="dataTable table-hover">
              <tbody>
                <tr>
                  <td colspan="4"><b>Class X</b></td>
                </tr>
                <tr>
                  <td><?php echo $user['Education']['tenschool']; ?></td>
                  <td><?php echo $user['Education']['tenschoolyear']; ?></td>
                  <td><a target="_blank" href="<?php echo HTTP_ROOT . 'files/Marksheets/'. $user['Education']['tenschoolmarksheet']?>">Click to view</a></td>
                  <td>                 
                      <label class="checkbox-inline"><input type="checkbox" name="data[EducationEvaluation][ten_status]" value="1" <?php if(@$user['Education']['EducationEvaluation']['ten_status'] == 1){ ?> checked <?php } ?>><b>Verify</b></label>
                  </td>
                  <input type="hidden" name="data[EducationEvaluation][edu_id]" value="<?php echo $user['Education']['id'] ?>">
                </tr>
                <tr>
                  <td colspan="4"><b>Class XII</b></td>
                </tr>
                <tr>
                  <td><?php echo $user['Education']['twelveschool']; ?></td>
                  <td><?php echo $user['Education']['twelveschoolyear']; ?></td>
                  <td><a target="_blank" href="<?php echo HTTP_ROOT . 'files/Marksheets/'. $user['Education']['twelveschoolmarksheet']?>">Click to view</a></td>
                  <td>
                    <label class="checkbox-inline"><input type="checkbox" name="data[EducationEvaluation][twelve_status]" value="1" <?php if(@$user['Education']['EducationEvaluation']['twelve_status'] == 1){ ?> checked <?php } ?>><b>Verify</b></label>
                  </td>
                  
                </tr>
                <tr>
                  <td colspan="4"><b>Graduation</b></td>
                </tr>
                <tr>
                  <td><?php echo $user['Education']['gradcollege']; ?></td>
                  <td><?php echo $user['Education']['gradyear']; ?></td>
                  <td><a target="_blank" href="<?php echo HTTP_ROOT . 'files/Marksheets/'. $user['Education']['gradmarksheet']?>">Click to view</a></td>
                  <td>
                    <label class="checkbox-inline"><input type="checkbox" name="data[EducationEvaluation][grad_status]" value="1" <?php if(@$user['Education']['EducationEvaluation']['grad_status'] == 1){ ?> checked <?php } ?>><b>Verify</b></label>
                  </td>
            
                </tr>
                <?php if(!empty($user['Education']['postgradcollege'])){ ?>
                <tr>
                  <td colspan="4"><b>Graduation</b></td>
                </tr>
                <tr>
                  <td><?php echo $user['Education']['postgradcollege']; ?></td>
                  <td><?php echo $user['Education']['postgradyear']; ?></td>
                  <td><a target="_blank" href="<?php echo HTTP_ROOT . 'files/Marksheets/'. $user['Education']['postgradmarksheet']?>">Click to view</a></td>
                  <td>
                    <label class="checkbox-inline"><input type="checkbox" name="data[EducationEvaluation][postgrad_status]" value="1" <?php if(@$user['Education']['EducationEvaluation']['postgrad_status'] == 1){ ?> checked <?php } ?>><b>Verify</b></label>
                  </td>
                 
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="note">*Note: Click the red ticks to change the status</div>
          </div>
          <!--Tab 3:- Experience Details-->
          <div class="tab-pane fade" id="experience">    
            <div class="h2 page-heading push">Experience details</div>
            <table class="dataTable table-hover">
              <tbody>
                <tr><b>Companies</b></tr>
                <?php $i=0; foreach($user['Experience'] as $exp){ ?>
                <tr>
                  <td><?php echo $exp['company']; ?></td>
                  <td></td>
                  <td><a target="_blank" href="<?php echo HTTP_ROOT . 'files/Experiences/'. $exp['document']?>">Click to view</a></td>
                  <td>
                    <label class="checkbox-inline"><input type="checkbox" name="data[ExperienceEvaluation][<?php echo $i; ?>][status]" value="1" <?php if(@$exp['ExperienceEvaluation']['status'] == 1){ ?> checked <?php } ?>><b>Verify</b></label>
                  </td>
                  <input type="hidden" name="data[ExperienceEvaluation][<?php echo $i; ?>][exp_id]" value="<?php echo $exp['id']; ?>">
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
          <!--Tab 4:- Cerifications Details-->
          <div class="tab-pane fade" id="certifications">
            <div class="h2 page-heading push">Certification details</div>
             <table class="dataTable table-hover">
              <tbody>
                <tr><b>Courses</b></tr>
                <?php $i=0; foreach($user['Certification'] as $cert){ ?>
                <tr>
                  <td><?php echo $cert['Domain']['domain']; ?></td>
                  <td></td>
                  <td><a target="_blank" href="<?php echo HTTP_ROOT . 'files/Certificates/'. $cert['document']?>">Click to view</a></td>
                  <td>
                    <label class="checkbox-inline"><input type="checkbox" name="data[CertificationEvaluation][<?php echo $i; ?>][status]" value="1" <?php if(@$cert['CertificationEvaluation']['status'] == 1){ ?> checked <?php } ?>><b>Verify</b></label>
                  </td>
                  <input type="hidden" name="data[CertificationEvaluation][<?php echo $i; ?>][cert_id]" value="<?php echo $cert['id']; ?>">
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
          <!--Tab 5: Remarks-->
          <div class="tab-pane fade" id="remarks">
            <div class="h2 page-heading push">Remarks</div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Remark<span class="text-danger">*</span></label>
                  <textarea class="form-control" name="data[Evaluation][remark]" placeholder="Enter your Comments...." value=""><?php echo @$user['Evaluation']['remark']; ?></textarea>
                  <input type="hidden" name="data[Evaluation][user_id]" value="<?php echo $user['User']['id']; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Rating<span class="text-danger">*</span></label>
                  <select class="form-control" name="data[Evaluation][rating]">
                    <option value="5" <?php if($user['Evaluation']['rating'] == 5){ ?> selected <?php } ?>>5</option>
                    <option value="4" <?php if($user['Evaluation']['rating'] == 4){ ?> selected <?php } ?>>4</option>
                    <option value="3" <?php if($user['Evaluation']['rating'] == 3){ ?> selected <?php } ?>>3</option>
                    <option value="2" <?php if($user['Evaluation']['rating'] == 2){ ?> selected <?php } ?>>2</option>
                    <option value="1" <?php if($user['Evaluation']['rating'] == 1){ ?> selected <?php } ?>>1</option>
                  </select>
                  <input type="hidden" name="data[Evaluation][eval_id]" value="<?php echo $this->Session->read('User.id');?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </form>
  
  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>

