
<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
.pushes
{
  margin-bottom: 42px!important;
}
#user_skills
{
  margin-bottom: 3%;
}
.small
{
  font-size: 133%;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="" enctype="multipart/form-data">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Profile</div>
      </div>
      <div class="pull-right"> <button type="submit" class="btn btn-primary">Save</button> <a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#personal-details">Personal Details</a> </li>
           <li class=""> <a href="#skills">Skills</a> </li>
        </ul>
        <div class="block-content tab-content">
          <!--Tab 1:- Personal Details-->
          <div class="tab-pane fade in active" id="personal-details">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-no" name="data[User][name]" placeholder="Name" value="<?php echo @$editUser['User']['name']?>">
                  <input type="hidden" name="data[User][id]" value="<?php echo @$editUser['User']['id']?>">
                </div>
                 <div class="form-group">
                  <label class="control-label" >Mobile<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-name" maxlength="10" minlength="10" name="data[User][mobile]" placeholder="Mobile" value="<?php echo @$editUser['User']['mobile']?>">
                </div>
                <div class="form-group">
                  <label class="control-label" >Martial Status<span class="text-danger">*</span></label>
                  <select class="form-control" name="data[User][martial_status]">
                    <option value="0" <?php if(@$editUser['User']['martial_status'] == 0) { ?> selected <?php } ?>>Unmarried</option>
                    <option value="1" <?php if(@$editUser['User']['martial_status'] == 1) { ?> selected <?php } ?>>Married</option>
                  </select>
                </div>
                
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Email<span class="text-danger">*</span></label>
                  <input class="form-control required email" type="text" id="val-name" name="data[User][email]" placeholder="Email" value="<?php echo @$editUser['User']['email']?>" disable>
                </div>
                <div class="form-group">
                  <label class="control-label" >Age<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-name" name="data[User][age]" placeholder="Age" value="<?php echo @$editUser['User']['age']?>">
                </div>
              </div>
            </div>
          </div>
          <!--Tab 2:- Skill Details-->
          
          <div class="tab-pane fade" id="skills">
            <div class="pull-right"><a id="add_butt" class="btn btn-success">+</a></div>
            <div class="h2 page-heading pushes">Enter Domains</div>
            <?php if(!empty(@$editUser['EvaluatorDomain'])){ ?>
            <div class="page-heading small">Your Skills</div>
              <table width="50%" id="user_skills">
                <?php $i=1; foreach($editUser['EvaluatorDomain'] as $skills){ ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $skills['Domain']['domain']; ?><td>
                  <td><a title="Delete Skill" href="<?php echo HTTP_ROOT.'People/deleteskill/'.base64_encode($skills['id'])?>"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i></a></td>
                </tr>
                <?php $i++; } ?>
              </table>
            <?php } ?>
            <div class="row" id="exp">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Domain<span class="text-danger"></span></label>
                  <select name="data[EvaluatorDomain][0][domain_id]" class="form-control">
                    <?php foreach($domains as $domain){ ?>
                      <option value="<?php echo $domain['Domain']['id']; ?>"><?php echo $domain['Domain']['domain']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        </div>
      </div>
    </div>
  </form>
  
  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>
<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
              format:'YYYY-MM-DD'
            });
            $('#datetimepicker2').datetimepicker({
              format:'YYYY-MM-DD'
            });
        });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        var i = 0;
        $("#add_butt").click(function () {
          //console.log('Yay! It works!');
          i++;
           $("#exp").append('<div class="col-md-6"><div class="form-group"><label class="control-label">Domain<span class="text-danger">*</span></label><select name="data[EvaluatorDomain]['+ i +'][domain_id]" class="form-control"><?php foreach($domains as $domain){ ?><option value="<?php echo $domain['Domain']['id']; ?>"><?php echo $domain['Domain']['domain']; ?></option><?php } ?></select></div></div>');
        });
      });
    </script>