<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>Flight, Cheap air tickets, AirFare.com</title>
	<!-- Stylesheets -->
	<!-- Web fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
	<!-- Page JS Plugins CSS -->
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/slick/slick.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/slick/slick-theme.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="https://ns.yatracdn.com/common/images/fresco/favicon.ico">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/bootstrap.min.css">
	<link rel="stylesheet" id="css-main" href="<?php echo HTTP_ROOT ?>css/oneui.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/style.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/datatables/jquery.dataTables.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/select2/select2-bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/airfare.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/font-awesome.min.css">
	<script src="<?php echo HTTP_ROOT ?>js/core/jquery.min.js"></script>
	<script src="<?php echo HTTP_ROOT ?>js/core/airfare.js"></script>
	<script src="<?php echo HTTP_ROOT ?>js/core/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo HTTP_ROOT ?>js/core/bootstrap.min.js"></script>
	<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
	<script src="<?php echo HTTP_ROOT ?>js/validate.js"></script>

</head>
<body>
			
            <?php
                 echo $this->element('Frontend/header');
            ?>
            <section>
            <?php     echo $this->fetch('content'); ?>

            </section>
            <?php
                 echo $this->element('Frontend/footer');
            ?>
      
            <?php echo $this->element('Frontend/modal');?>

			<script type="text/javascript">
				<?php if(!empty($this->Session->read('verified'))){ ?>
				$('#msg').text('<?php echo $this->Session->read("verified"); ?>');
				$('#id03').modal({
    				backdrop: false,
					});
				$('#id03').modal('show');
				  
		  <?php unset($_SESSION['verified']); }?>

			</script>
                   
</body>
</html>