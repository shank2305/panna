  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="pull-left">
            <div class="h2 page-heading">Profile</div>
          </div>
          
          <div class="clearfix"></div>
     	</div>
        <div class="block block-bordered">
            <div class="form-horizontal block-content">
                <table class="table">
                    <!-- <tr>
                        <td>Profile</td>
                        <td>
                            <?php if(empty($profile['Admin']['image'])){?>
                            <img src="<?php echo HTTP_ROOT ?>img/avatars/avatar10.jpg" width="100px">
                            <?php } else{?>
                            <img src="<?php echo HTTP_ROOT.'img/profile/'.@$profile['Admin']['image']?>" width="100px">
                            <?php }?>
                        </td>
                    </tr> -->
                    <tr>
                        <td>UserName</td>
                        <td><?php echo $profile['Admin']['username'];?></td>
                    </tr>
                    
                    <tr>
                        <td>Email</td>
                        <td><?php echo $profile['Admin']['email'];?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <a href="<?php echo HTTP_ROOT?>Users/editProfile/<?php echo base64_encode($profile['Admin']['id']);?>" class="btn btn-primary">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 