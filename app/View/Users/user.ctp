  <style>
  .pagination > li > span {
    float: left;
  }
  table,th,td
  {
    text-align: center;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">System Users</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Users</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading">Users</div>
              </div>
                    <!-- <div class="pull-right">
                    <a href="<?php echo HTTP_ROOT?>Users/adduser" class="btn btn-default"><i class="fa fa-plus"></i> Add User</a>
                    </div> -->
              <div class="clearfix"></div>
            </div>
            <table class="dataTable table-hover">
              <thead>
                        <tr role="row">
                            <th>SNo<i class="sorting_icon"></i></th>
                            <th>Email<i class="sorting_icon"></i></th>
                            <th>Name<i class="sorting_icon"></i></th>
                            <th>Phone<i class="sorting_icon"></i></th>
                            <th>Status<i class="sorting_icon"></i></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $i=1; foreach($user_list as $user){?>
                        <tr class="odd">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $user['User']['email']?></td>

                            <td><?php echo $user['User']['name']?></td>

                            <td><?php echo $user['User']['phone']?></td>

                            <?php if($user['User']['varify_status'] == 0){?>
                            <td>Inactive</td>
                            <?php } else{?>
                            <td>Active</td>
                            <?php $i++; }?>
                            
                            <td>
                              <a title="Update Status" href="<?php echo HTTP_ROOT.'Users/update_user/'.$user['User']['id']?>"><i class="fa fa-flag" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php }?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
