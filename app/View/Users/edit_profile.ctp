
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="pull-left">
            <div class="h2 page-heading">Edit Profile</div>
          </div>
          
          <div class="clearfix"></div>
     	</div>
        <form method="post" enctype="multipart/form-data" >
        <div class="block block-bordered">
            <div class="form-horizontal block-content">
                
                

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Name</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="data[Admin][username]" id="name" value="<?php echo $profile['Admin']['username'];?>" maxlength="50" required>
                <input type="hidden" name="data[Admin][id]" id="id" value="<?php echo $profile['Admin']['id'];?>">
                </div></div>


                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Email</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="email" name="data[Admin][email]" id="name" value="<?php echo $profile['Admin']['email'];?>" maxlength="50" required>
                </div></div>

                
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 
  <script>

$(document).ready(function () {
    
    // $('.dataTables_filter').find('input').addClass('.form-control');

/*    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#role').on('change', function() {
        var role = $('#role').val();
        if(role == 1){
            $("#ware").css("display", "block");
        }
        else if(role == 0){
            $("#ware").css("display", "block");
        }
        else{
            $("#ware").css("display", "none");    
        }
        
    });*/
});
</script>