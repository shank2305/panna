  <style>
  .pagination > li > span {
    float: left;
  }
  table, th, td
  {
    text-align: center;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Consultants</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Summary</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading"></div>
              </div>
                    <div class="pull-right">
                    <a href="<?php echo HTTP_ROOT?>Users/adduser" class="btn btn-primary"><i class="fa fa-plus"></i> Add User</a>
                    </div> 
              <div class="clearfix"></div>
            </div>
            <table class="dataTable table-hover">
              <thead>
                      <tr role="row">
                          <th>S.No.<i class="sorting_icon"></i></th>
                          <th>Name<i class="sorting_icon"></i></th>
                          <th>Email<i class="sorting_icon"></i></th>
                          <th>Email Status<i class="sorting_icon"></i></th>
                          <th>Mobile<i class="sorting_icon"></i></th>
                          <th>Mobile Status<i class="sorting_icon"></i></th>
                          <th>Status<i class="sorting_icon"></i></th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $i=1; foreach($user_list as $user){?>
                        <tr class="odd">
                            <td><?php echo $i;?></td>
                            <td><?php echo $user['User']['name']?></td>

                            <td><?php echo $user['User']['email']?></td>

                            <td><?php if($user['User']['email_status'] == 1) { ?>
                              <i class="fa fa-check" aria-hidden="true"></i>
                              <?php } else { ?>
                               <i class="fa fa-times" aria-hidden="true"></i>
                               <?php } ?>
                            </td>
                            <td><?php echo $user['User']['mobile']?></td>
                           <td><?php if($user['User']['mobile_status'] == 1) { ?>
                              <i class="fa fa-check" aria-hidden="true"></i>
                              <?php } else { ?>
                               <i class="fa fa-times" aria-hidden="true"></i>
                               <?php } ?>
                            </td>
                            <td><?php if($user['User']['status'] == 1) { ?>
                              <i class="fa fa-check" aria-hidden="true"></i>
                              <?php } else { ?>
                               <i class="fa fa-times" aria-hidden="true"></i>
                               <?php } ?>
                            </td>
                            <td>
                              <a title="Evaluation Reports" href="<?php echo HTTP_ROOT.'Users/reports/'.base64_encode($user['User']['id'])?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a title="Update Status" href="<?php echo HTTP_ROOT.'Users/update_user/'.base64_encode($user['User']['id'])?>"><i class="fa fa-flag" aria-hidden="true"></i></a>
                              <a title="Edit" href="<?php echo HTTP_ROOT.'Users/adduser/'.base64_encode($user['User']['id']) . '/view'?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <a title="Delete" onClick="if(!confirm('Are you sure, you want to delete this Consultant?')){return false;}" href="<?php echo HTTP_ROOT.'users/deleteAll/User/'.base64_encode($user['User']['id'])?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php $i++; }?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
<style type="text/css">
  .fa-check
  {
    color: green;
  }
  .fa-times
  {
    color: red;
  }
</style>
<script>
  function deleteItem() {
    if (!confirm("Are you sure?")) {
        window.location.href = "<?php echo HTTP_ROOT.'users/deleteAll/User/'.base64_encode($user['User']['id'])?>";
        return false;
    }
    
}
</script>
