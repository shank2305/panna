  <style>
  .pagination > li > span {
    float: left;
  }
  table, th, td
  {
    text-align: center;
  }
  </style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content">
      <div class="h2 page-heading">Evaluations</div>
      <br>
      <div class="block block-bordered">
        <ul class="nav nav-tabs" data-toggle="tabs">
        	<li class="active"><a href="#user-access">Summary</a></li>
        </ul>
        
        <div class="block-content tab-content">
         
          <div class="tab-pane fade in active" id="user-access">
            <div class="push">
              <div class="pull-left">
                <div class="h2 page-heading"></div>
              </div>
                   
              <div class="clearfix"></div>
            </div>
            <table class="dataTable table-hover">
              <thead>
                      <tr role="row">
                          <th>S.No.<i class="sorting_icon"></i></th>
                          <th>Name<i class="sorting_icon"></i></th>
                          <th>Evaluator<i class="sorting_icon"></i></th>
                          <th>Remark<i class="sorting_icon"></i></th>
                          <th>Rating<i class="sorting_icon"></i></th>
                          <th>Date of Evaluation<i class="sorting_icon"></i></th>
                          <th>Final Admin Verification<i class="sorting_icon"></i></th>
                          <th>Actions<i class="sorting_icon"></i></th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $i=1; foreach($find as $user){?>
                        <tr class="odd">
                            <td><?php echo $i;?></td>
                            <td><?php echo $user['User']['name']?></td>
                            <td><?php echo $user['Eval']['name']?></td>
                            <td><?php echo $user['Evaluation']['remark']?></td>
                            <td><?php echo $user['Evaluation']['rating']?></td>
                            <td><?php echo $user['Evaluation']['created_date']?></td>
                            <td>
                              <?php 
                                switch ($user['User']['evaluation_status']) {
                                  case '1':
                                    echo 'Pending <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>';
                                    break;
                                  case '3':
                                    echo 'Approved <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>';
                                    break;
                                }
                              ?>
                            </td>
                            <td>
                              <a title="View Evaluation" href="<?php echo HTTP_ROOT.'Users/evaluationdetails/'.base64_encode($user['User']['id']) . '/' . base64_encode($user['Eval']['id'])?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a title="Finalize Evaluation" href="<?php echo HTTP_ROOT.'Users/updateevaluation/'.base64_encode($user['User']['id'])?>"><i class="fa fa-flag" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                      <?php $i++; }?>
                    </tbody>
				    </table>
            <div class="push">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <ul class="pagination pagination-sm inline">
                  <li class="page-item"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'page-link','style'=>'float:left;'));?></li>
                  <li class="page-item"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'page-link'));?></li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>    
              
          </div>  
        </div>
      </div>
    </div>
    <!-- END Stats --> 
    
    
  </main>
  <!-- END Main Container --> 
  <script>
      $(function () {
       
        $('#userTable1').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
        });
      });
    </script> 
<style type="text/css">
  .fa-check
  {
    color: green;
  }
  .fa-times
  {
    color: red;
  }
</style>