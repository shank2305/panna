
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <div class="content push-15">
   		<div title="heading-section" class="push">
          <div class="pull-left">
            <div class="h2 page-heading">Edit Profile</div>
          </div>
          
          <div class="clearfix"></div>
     	</div>
        <form method="post" enctype="multipart/form-data" >
        <div class="block block-bordered">
            <div class="form-horizontal block-content">
                
                

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Name</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="data[User][name]" id="name" value="<?php echo $profile['User']['name'];?>" maxlength="50" required>
                <input type="hidden" name="data[User][id]" id="id" value="<?php echo $profile['User']['id'];?>">
                </div></div>


                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Email</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="email" name="data[User][email]" id="name" value="<?php echo $profile['User']['email'];?>" maxlength="50" required readonly>
                </div></div>


                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Phone</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="text" name="data[User][phone]" id="name" value="<?php echo $profile['User']['phone'];?>" maxlength="50" required>
                </div></div>

                <!-- <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Date of Birth</label>
                </div>
                <div class="col-md-8">
                    <div class="input-group date">
                        <input class="form-control datepicker" type="text" id="val-date-available" name="data[User][dob]" value="<?php echo $profile['User']['dob'];?>" placeholder="Choose a date.." required>
                        <span class="input-group-addon"> </div>
                </div></div> -->

                <?php if($this->Session->read('Auth.User.role') == 0 && $this->Session->read('Auth.User.role') != null){?>
                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Role</label>
                </div>
                <div class="col-md-8">
                <select class="form-control" id="role" name="data[User][role_id]" placeholder="Choose role" required>
                    <option value="">Select</option>
                    <?php foreach($roles as $role){?>
                    <option value="<?php echo $role['Role']['id']?>" <?php echo @$profile['User']['role_id'] == $role['Role']['id'] ? 'selected' : '';?>><?php echo $role['Role']['role_name']?></option>
                    <?php }?>
                    <!-- <option value="0" <?php echo @$profile['User']['role'] == 0 ? 'selected' : '';?>>Admin</option>
                    <option value="1" <?php echo @$profile['User']['role'] == 1 ? 'selected' : '';?>>Warehouse</option> -->
                    <!-- <option value="2">Employee</option>
                    <option value="3">Customer</option> -->
                  </select>
                </div></div>


                <?php }?>

                <div class="form-group">
                <div class="col-md-4">
                <label class="control-label">Profile</label>
                </div>
                <div class="col-md-8">
                <input class="form-control" type="file" name="DishImage" id="">
                </div></div>

                <?php foreach ($profile['UserMeta'] as $image) { 
                            if($image['key_meta'] == 'image'){
                                $img = $image['value_meta'];
                            } 
                        }?>
                <div class="form-group">
                    <div class="col-md-4">
                    <label class="control-label"></label>
                    </div>
                    <div class="col-md-8">
                    <?php if(empty($img)){?>
                    <img src="<?php echo HTTP_ROOT ?>img/avatars/avatar10.jpg" width="100px">
                    <?php } else{?>
                    <img src="<?php echo HTTP_ROOT.'img/profile/'.@$img?>" width="100px">
                    <?php }?>
                </div></div>
                
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <!-- END Stats --> 
    
  
  </main>
  <!-- END Main Container --> 
  <script>

$(document).ready(function () {
    
    // $('.dataTables_filter').find('input').addClass('.form-control');

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#role').on('change', function() {
        var role = $('#role').val();
        if(role == 1){
            $("#ware").css("display", "block");
        }
        else if(role == 0){
            $("#ware").css("display", "block");
        }
        else{
            $("#ware").css("display", "none");    
        }
        
    });
});
</script>