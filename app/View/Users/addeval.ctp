<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Add Evaluator</div>
      </div>
      <div class="pull-right"> <button type="submit" class="btn btn-primary">Save</button> <a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#contact-details">Details</a> </li>

        </ul>
        <div class="block-content tab-content">
          <div class="tab-pane fade in active" id="contact-details">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Name<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="val-name" name="data[User][name]" placeholder="Name" value="<?php echo @$editUser['User']['name']?>">
                  <input type="hidden" name="data[User][id]" value="<?php echo @$editUser['User']['id']?>">
                </div>
                 <div class="form-group">
                  <label class="control-label" >Mobile<span class="text-danger">*</span></label>
                  <input class="form-control required number" type="text" id="val-mobile" maxlength="10" minlength="10" name="data[User][mobile]" placeholder="Mobile" value="<?php echo @$editUser['User']['mobile']?>">
                </div>
                
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Email<span class="text-danger">*</span></label>
                  <input class="form-control required email" type="text" id="val-email" name="data[User][email]" placeholder="Email" value="<?php echo @$editUser['User']['email']?>"<?php if(!empty(@$editUser['User']['email'])){?> disabled <?php } ?>>
                </div>
                <div class="form-group">
                 
                </div>
              </div>
            </div>
          </div>

            
          </div>
        </div>
      </div>
    </div>
  </form>
    <!-- END Stats --> 
    
    <!-- Page Content -->
    <div class="content"> </div>
    <!-- END Page Content -->

  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>
<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
              format:'YYYY-MM-DD HH:mm:ss'
            });
            $('#datetimepicker2').datetimepicker({
              format:'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>