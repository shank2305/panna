<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Add Domain</div>
      </div>
      <div class="pull-right"> <!-- <button type="submit" class="btn btn-primary">Save</button>  --><a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#Domain">Add Domain</a> </li>
           <li> <a href="#domains">Domains</a> </li>
        </ul>
        <div class="block-content tab-content">
          <div class="tab-pane fade in active" id="Domain">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Domain Name<span class="text-danger">*</span></label>
                  <input class="form-control required" type="text" id="dom-name" name="data[Domain][domain]" placeholder="Domain Name" value="">
                </div>
              </div>
              <div class="col-md-6">
                <br>
                <button class="btn btn-primary">Save</button>
              </div>
          </div>  
        </div>
        <div class="tab-pane fade in" id="domains">
          <div class="h2 page-heading pushes">Registered Domains</div>
          <table width="50%" style="margin: 2% 0% 1% 0%;">
            <tbody>
              <?php foreach($alldomains as $domains){ ?>
              <tr>
                <td><?php echo $domains['Domain']['domain']; ?></td>
                <td><a title="Delete" onClick="if(!confirm('Are you sure, you want to delete this Domain?')){return false;}" href="<?php echo HTTP_ROOT.'users/deleteAll/Domain/'.base64_encode($domains['Domain']['id'])?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</form></main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>
<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
              format:'YYYY-MM-DD HH:mm:ss'
            });
            $('#datetimepicker2').datetimepicker({
              format:'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>