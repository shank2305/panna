
<style>
.bootstrap-datetimepicker-widget{
z-index: 10000 !important;
}
.note
{
    margin-top: 3%;
    font-size: 75%;
}
.indications
{
    padding: 2%;
}
</style>
  <!-- Main Container -->
  <main id="main-container"> 
    
    <!-- Stats -->
    <form method="post" id="cform" action="" enctype="multipart/form-data">
    <div class="content push-15">
      <div class="pull-left">

        <div class="h2 page-heading">Evaluation of Consultant</div>
      </div>
      <div class="pull-right"> <button type="submit" class="btn btn-primary">Save</button> <a onclick="history.go(-1);" class="btn btn-danger">Back</a></div>
      <div class="clearfix"></div>
      <br>
      <div class="block block-bordered">
        <ul class=" bg-gray-lighter no-padding nav nav-tabs" data-toggle="tabs" style="z-index:1 !important;">
           <li class="active"> <a href="#education">Education</a> </li>
           <li class=""> <a href="#experience">Experience</a> </li>
           <li class=""> <a href="#certifications">Certifications</a> </li>
           <li class=""> <a href="#remarks">Remarks</a> </li>
        </ul>
        <div class="block-content tab-content">
          
          <!--Tab 2:- Education Details-->
          <div class="tab-pane fade in active" id="education">
            <div class="h2 page-heading push">Marksheets</div>
            <table class="dataTable table-hover">
              <tbody>
                <tr>
                  <td colspan="2"><b>Class X</b></td>
                  <td><?php echo $edueval['Education']['tenschool']; ?></td>
                  <td><?php echo $edueval['Education']['tenschoolyear']; ?></td>
                  <td>
                    <?php if($edueval['EducationEvaluation']['ten_status'] == 1){?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>
                    <?php } else if($edueval['EducationEvaluation']['ten_status'] == 0){ ?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><b>Class XII</b></td>
                  <td><?php echo $edueval['Education']['twelveschool']; ?></td>
                  <td><?php echo $edueval['Education']['twelveschoolyear']; ?></td>
                  <td>
                    <?php if($edueval['EducationEvaluation']['twelve_status'] == 1){?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>
                    <?php } else if($edueval['EducationEvaluation']['twelve_status'] == 0){ ?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><b>Graduation</b></td>
                  <td><?php echo $edueval['Education']['gradcollege']; ?></td>
                  <td><?php echo $edueval['Education']['gradyear']; ?></td>
                  <td>
                    <?php if($edueval['EducationEvaluation']['grad_status'] == 1){?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>
                    <?php } else if($edueval['EducationEvaluation']['grad_status'] == 0){ ?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>
                    <?php } ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--Tab 3:- Experience Details-->
          <div class="tab-pane fade" id="experience">    
            <div class="h2 page-heading push">Experience details</div>
            <table class="dataTable table-hover">
              <tbody>
                <tr><b>Companies</b></tr>
                <?php $i=0; foreach($expeval as $exp){ ?>
                <tr>
                  <td><?php echo $exp['Experience']['company']; ?></td>
                  <td><?php echo $exp['Experience']['job_desc']; ?></td>
                  <td><?php echo $exp['Experience']['date_from'] . "  -  " . $exp['Experience']['date_to']; ?></td>
                  <td>
                    <?php if($exp['ExperienceEvaluation']['status'] == 1){?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>
                    <?php } else if($exp['ExperienceEvaluation']['status'] == 0){ ?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>
                    <?php } ?>
                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
          <!--Tab 4:- Cerifications Details-->
          <div class="tab-pane fade" id="certifications">
            <div class="h2 page-heading push">Certification details</div>
             <table class="dataTable table-hover">
              <tbody>
                <tr><b>Courses</b></tr>
                <?php $i=0; foreach($certeval as $cert){ ?>
                <tr>
                  <td><?php echo $cert['Certification']['Domain']['domain']; ?></td>
                  <td><?php echo "<b>Authority: </b>" . $cert['Certification']['authority']; ?></td>
                  <td>
                    <?php if($cert['CertificationEvaluation']['status'] == 1){?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i>
                    <?php } else if($cert['CertificationEvaluation']['status'] == 0){ ?>
                    <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i>
                    <?php } ?>
                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
          <!--Tab 5: Remarks-->
          <div class="tab-pane fade" id="remarks">
            <div class="h2 page-heading push">Remarks</div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Remark<span class="text-danger">*</span></label>
                  <textarea class="form-control" name="data[Evaluation][remark]" placeholder="Enter your Comments...." value="" disabled><?php echo @$eval['Evaluation']['remark']; ?></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" >Rating<span class="text-danger">*</span></label>
                  <div class="form-group">
                  <?php echo $eval['Evaluation']['rating']; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="indications">
          <i class="fa fa-circle" aria-hidden="true" style="color: green;"></i> - Verified <i class="fa fa-circle" aria-hidden="true" style="color: red;"></i> - Unverified
        </div>
        </div>
      </div>
    </div>
  </form>
  
  </main>
  <!-- END Main Container --> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
    <script>
    $('#cform').validate();
    </script>

