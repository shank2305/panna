<section class="Offers">
<div><label id="heading_secton2">Airfare Specials</label></div>
	<div class="row">
		<div class="col-sm-3 spl">
			<div class="thumbnail">
			  <a href="#">
				<img src="<?php echo HTTP_ROOT ?>img/index/spl1.jpg" alt="Lights" style="width:100%">
				<div class="caption">
				  <p>Flat 50% Off on Homestays!!</p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 spl">
			<div class="thumbnail">
			  <a href="#">
				<img src="<?php echo HTTP_ROOT ?>img/index/spl2.jpg" alt="Lights" style="width:100%">
				<div class="caption">
				  <p>Flat Rs. 1000 off!</p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 spl">
			<div class="thumbnail">
			  <a href="#">
				<img src="<?php echo HTTP_ROOT ?>img/index/spl2.jpg" alt="Lights" style="width:100%">
				<div class="caption">
				  <p>Save upto Rs. 500!</p>
				</div>
			  </a>
			</div>
		</div>
	</div>
</section>

<section class="Holidays">
	<div><label id="heading_secton2">Perfect Holidays</label></div>
	<div class="row">
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero">Hill Stations</h2>
				   <p class="zero">Starting at Rs. 7,550</p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT ?>img/index/Holiday1.jpg" alt=""/></li>
			</ul>
		</div>
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero">Mega Sale</h2>
				   <p class="zero">Starting at Rs. 23,990</p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT ?>img/index/Holiday2.jpg" alt=""/></li>
			</ul>
		</div>
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero">Airfare Service Insurance</h2>
				   <p class="zero">Starting at Rs. 13,425</p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT ?>img/index/Holiday3.jpg" alt=""/></li>
			</ul>
		</div>
		<div class="col-md-2 spll">
			<ul class="demo-2 effect">
				<li>
				   <h2 class="zero">Honey Moon</h2>
				   <p class="zero">Starting at Rs. 5,400</p>
				</li>
				<li><img class="img-responsive top" src="<?php echo HTTP_ROOT ?>img/index/Holiday4.jpg" alt=""/></li>
			</ul>
		</div>
	</div>
	
</section>
<!--End Holiday Section-->
<section>
	<div class="parallax" style="background-image: url('<?php echo HTTP_ROOT ?>img/index/banner1.jpg');">
		<section class="advertise" style="margin-top:1%;padding-top: 4%;  margin-bottom:2%;">
			<div><image class="advertisement2" src="<?php echo HTTP_ROOT ?>img/index/ad10.jpg"></div>
		</section>
	</div>
</section>

<!--End of parallax-->

<section class="Travel">
	<div><label id="heading_secton2">Travel Talks</label></div>
	<p style="margin-left:3%;">Sharing travel experiences - one story at a time</p>
	<div class="row">
		<div class="col-sm-3 tra">
			<div class="thumbnail">
			  <a href="#">
				 <img src="<?php echo HTTP_ROOT ?>img/index/Organic_Meal.jpg" class="img-rounded" alt="Cinque Terre" width="304" height="236"> 
				 <div class="caption">
				  <p>#GastroTraveller Picks out best 6 Restaurants in India<br><br><b>Read More</b></p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 tra">
			<div class="thumbnail">
			  <a href="#">
				 <img src="<?php echo HTTP_ROOT ?>img/index/Village_and_fields.jpg" class="img-rounded" alt="Cinque Terre" width="304" height="236"> 
				<div class="caption">
				  <p>To Those Who Love The Mountains - Mystical Places to Go Off the Beaten Track In Uttrakhand<br><br><b>Read More</b></p>
				</div>
			  </a>
			</div>
		</div>
		<div class="col-sm-3 tra">
			<div class="thumbnail">
			  <a href="#">
				 <img src="<?php echo HTTP_ROOT ?>img/index/Degraves_Street1.jpg" class="img-rounded" alt="Cinque Terre" width="304" height="236"> 
				<div class="caption">
				  <p>A Foodie's Guide to the Best Restaurants in Melbourne<br><br><b>Read More</b></p>
				</div>
			 </a>
			</div>
		</div>
	
	</div>
</section>

<!--Advetise-->

<section class="advertise" style="margin-top:2%; margin-bottom:2%;">
	<div><image class="advertisement" src="<?php echo HTTP_ROOT ?>img/index/ad4.jpg"></div>
</section>
