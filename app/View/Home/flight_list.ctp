<style>
	body
	{
		overflow:hidden;
	}
	.table-hover tbody tr:hover td 
	{
	    background: #669999;
		color:white;
	}
	.dataTable .table-hover, th, td
	{
		text-align: center;
	}
	.sort
	{
		width:30%;
		height:5%; 
		margin-top:10; 
		margin-bottom:10; 
		text-align:center;
		float: right;
	}
</style>
<div class="container" style="">
		<h3>Search Results for:</h3><br>
		<table width="100%">
			<form action="" id="list-search-form" method="post">
			<tr>
				<td><b>One Way: </b></td>
				<td><b>Round Trip: </b></td>
				<td><b>D.O.J: </b></td>
				<td><b>Source:</b></td>
				<td><b>Destination:</b></td>
				<td><b>Adult (12+ Yrs):</b></td>
				<td><b>Child (2-11 Yrs):</b></td>
				<td><b>Infant (0-2 Yrs):</b></td>
				<td class="yet1" style="display:<?php echo @$search['type_journey'] == 'OneWay' ? "none" : "";?>"><b>D.O.R</b></td>
			</tr>
			<tr>
				<!-- <td></td> -->
				<td>
					<input type="radio" value="OneWay" id="OneWay1"  name="type_journey" <?php echo @$search['type_journey'] == 'OneWay' ? "checked" : "";?>>
				</td>
				<!-- <td></td> -->
				<td>
					<input type="radio" value="RoundTrip" id="RoundTrip1"  name="type_journey" <?php echo @$search['type_journey'] == 'RoundTrip' ? "checked" : "";?>>
				</td>
				<!-- <td></td> -->
				<td>
					<input name="depart_date" type="text" class="form-control required" value="<?php echo date("d-m-Y", strtotime(@$count['0']['FlightDetail']['datetime_departure'])); ?>" id="depart-date">
				</td>
				<!-- <td></td> -->
				<td>
					<select name="place_from" type="text" class="form-control required">
		                <option value=''>Select Source</option>
		                <?php foreach($locations as $location){?>
		                  <option value="<?php echo $location['Location']['id']?>" <?php echo @$search['place_from'] == $location['Location']['id'] ? "selected" : ""; ?>><?php echo $location['Location']['location']?></option>
		                <?php }?> 
            		</select>
				</td>
				<!-- <td></td> -->
				<td>
					<select name="place_to" type="text" class="form-control required">
		                <option value=''>Select Destination</option>
		                <?php foreach($locations as $location){?>
		                  <option value="<?php echo $location['Location']['id']?>" <?php echo @$search['place_to'] == $location['Location']['id'] ? "selected" : ""; ?>><?php echo $location['Location']['location']?></option>
		                <?php }?> 
            		</select>
				</td>
				<!-- <td></td> -->
				<td>
					<input value="<?php echo @$search['adult']?>" name="adult" type="text" class="form-control required number" >
				</td>
				<!-- <td></td> -->
				<td>
					<input value="<?php echo @$search['child']?>" name="child" type="text" class="form-control required number" >
				</td>
				<!-- <td></td> -->
				<td>
					<input value="<?php echo @$search['infant']?>" name="infant" type="text" class="form-control required number" >
				</td>
				
				<td class="yet1" style="display:<?php echo @$search['type_journey'] == 'OneWay' ? "none" : "";?>">
					<input name="return_date" type="text" class="form-control required " value="<?php echo date("d-m-Y", strtotime(@$countreturn['0']['FlightDetail']['datetime_departure'])); ?>" id="return-date" >
				</td>
				<td><b>Search Again:</b></td>
				<td>
					<button class="btn btn-primary" >Refine Search</button>
				</td>
			</tr>

			</form>
		</table>
		<br>
		<?php if($flag == 2) { ?>
			<style type="text/css">
				.table-responsive
				{
					display: none;
				}
			</style>
			<h1>No Results Found</h1>
		<?php }?>
		<div class="table-responsive">
			<form action="<?php echo HTTP_ROOT?>Home/bookFlight" method="post"> 
			<input type="hidden" value="<?php echo @$search['type_journey']?>"   name="type_journey" >
			
				
					
			
					<input name="depart_date" type="hidden" class="form-control" value="<?php echo date("d-m-Y", strtotime(@$count['0']['FlightDetail']['datetime_departure'])); ?>" >
					<input name="place_from" type="hidden" class="form-control" value="<?php echo @$search['place_from']?>" >
					
					<input name="place_to" type="hidden" class="form-control" value="<?php echo @$search['place_to']?>" >
					
				
					<input value="<?php echo @$search['adult']?>" name="adult" type="hidden" class="form-control number" >
				
					<input value="<?php echo @$search['child']?>" name="child" type="hidden" class="form-control number" >
				
					<input value="<?php echo @$search['infant']?>" name="infant" type="hidden" class="form-control number" >
				
				
					<input name="return_date" type="hidden" class="form-control " value="<?php echo date("d-m-Y", strtotime(@$countreturn['0']['FlightDetail']['datetime_departure'])); ?>" >
				<div class="">
				<div class="col-md-6 col-sm-9" style="margin-top:15; font-size:20px; font-weight:800">
					Journey
				</div>
				</div>
					<table class="dataTable table-hover" >
						<thead style="background-color:#a3c2c2; color:white; font-size:14px;">
							<tr>
								<th class="text-center">S.No.</th>
								<th class="text-center">Flight No.</th>
								<th class="text-center">Flight Name</th>
								<th class="text-center">Source</th>
								<th class="text-center">Dept. Date/Time</th>
								<th class="text-center">Destination</th>
								<th class="text-center">Arri. Date/Time</th>
								<th class="text-center">Seats available</th>
								<th class="text-center">Book Flight</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; foreach($count as $counting) { ?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $counting['FlightDetail']['flight_no'];?></td>
								<td><?php echo $counting['FlightDetail']['flight_name'];?></td>
								<td><?php echo $counting['From']['location'];?></td>
								<td><?php echo date("d-m-Y H:m:s", strtotime($counting['FlightDetail']['datetime_departure']));?></td>
								<td><?php echo $counting['To']['location'];?></td>
								<td><?php echo date("d-m-Y H:m:s", strtotime($counting['FlightDetail']['datetime_arrival']));?></td>
								<td><?php echo $counting['FlightDetail']['seat_availability'];?></td>
								<td><input type="radio" value="<?php echo $counting['FlightDetail']['id'];?>" name="onwardFlight" required></td>
							</tr>
							<?php $i++; } ?>
						</tbody>
					</table>
				<br>
				
				<br>
				<?php
				if($flag == 1) { ?>
				<div class="col-md-6 col-sm-9" style="margin-top:15; font-size:20px; font-weight:800">
				Return Journey
				</div>
					<table class="dataTable table-hover" >
						<thead style="background-color:#a3c2c2; color:white; font-size:14px;">
							<tr>
								<th class="text-center">S.No.</th>
								<th class="text-center">Flight No.</th>
								<th class="text-center">Flight Name</th>
								<th class="text-center">Source</th>
								<th class="text-center">Dept. Date/Time</th>
								<th class="text-center">Destination</th>
								<th class="text-center">Arri. Date/Time</th>
								<th class="text-center">Seats available</th>
								<th class="text-center">Book Flight</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; foreach(@$countreturn as $countingreturn) { ?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $countingreturn['FlightDetail']['flight_no'];?></td>
								<td><?php echo $countingreturn['FlightDetail']['flight_name'];?></td>
								<td><?php echo $countingreturn['From']['location'];?></td>
								<td><?php echo date("d-m-Y H:m:s", strtotime($countingreturn['FlightDetail']['datetime_departure']));?></td>
								<td><?php echo $countingreturn['To']['location'];?></td>
								<td><?php echo date("d-m-Y H:m:s", strtotime($countingreturn['FlightDetail']['datetime_arrival']));?></td>
								<td><?php echo $countingreturn['FlightDetail']['seat_availability'];?></td>
								<td><input type="radio" value="<?php echo $countingreturn['FlightDetail']['id'];?>" name="returnFlight" required></td>
							</tr>
							<?php $i++; } ?>
						</tbody>
					</table>
				<?php }?>
				<?php if($this->Session->check('Auth')) { ?>
				<button class="btn btn-success" type="submit">Checkout</button>
				<?php } else { ?>
				<a style="width: 100%;" class="btn btn-success" data-backdrop="false" data-toggle="modal" data-target="#id01">Checkout</a>
				<?php } ?>
				</form>	
		</div>
	</div>
	<script>
        $(document).ready(function(){
            $("#depart-date").datepicker({
            	format: 'dd-mm-yyyy',
				startDate: '-3d'
            });
            $("#return-date").datepicker({
            	format: 'dd-mm-yyyy',
				startDate: '-3d'
            });
             $("#OneWay1").click(function(){
                $(".yet1").hide();
            });
            $("#RoundTrip1").click(function(){
                $(".yet1").show();
            });
        });
        </script>
        <script type="text/javascript">
  $('#list-search-form').validate();
</script>