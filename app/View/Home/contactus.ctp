<style>
	body
	{
		background-color: #E6F2FF;
	}
	.container
	{
		background-color: white;
	}
	.head
	{
		margin-left: 3%;
		margin-top: 3%;
	}
	.address
	{
		margin-top: 6%;
	}
	.space
	{
		margin-top: 1%; 
	}
</style>
<div class="container">
	<div class="head"><h2>Contact Us</h2></div>
	<div class="content">
		<div class="row">
			<div class="col-md-7"><img style="margin-top: 15%;" class="img-responsive" src="<?php echo HTTP_ROOT ?>img/index/contactus.jpg"></div>
			<div class="col-md-4">
				<h4 style="color: #138DF2;">Address</h4>
				<div class="address">
				<p>08, Lorem Ipsum - 1, Somewhere,

				<br>Distt - Maecenas vitae

				<br>Curabitur vel congue 600000

				<br>+91 - 120 - 2000000 / 2111111

				<br><b>Fax:</b> +91 - 120-6767676

				<br><b>Email:</b> info@airfare.co.in

				<br><b>Enquiry:</b> info@airfare.co.in</p></div>

				<div class="contact">
					<form action="" method="post">
						<h4 style="color: #138DF2; margin-bottom: 6%;">Contact US</h4>
						<input type="text" class="form-control space" placeholder="Your Name" name="name">
						<input type="text" class="form-control space" placeholder="Your Email" name="email">
						<input type="text" class="form-control space" placeholder="Your Mobile Number" name="mobile">
						<textarea class="form-control space" placeholder="Message" name="message"></textarea>
						<button class="btn btn-primary" style="width: 50%;">Send</button>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>