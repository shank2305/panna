<style>
	body
	{
		background-color: #E6F2FF;
	}
	.container
	{
		background-color: white!important;
	}
	.flight_detail
	{
		margin-top: 5%;
	}
	.head2
	{
		margin-top: 3%;
	}
	.passenger_details
	{
		margin-top: 3%;
	}
	.status
	{
		text-align: right;
		margin-right: 7%;
		margin-top: 2%;
	}
</style>
<div class="container">
	<div class="head">
		<h3>Payment Successfull</h3>
	</div>	

	<div class="flight_detail">
		<table width="100%">
			<tbody>
				<tr>
					<td><b>Flight No.: </b></td>
					<td><?php echo $flight['FlightDetail']['flight_no']; ?></td>
					<td><b>Flight Name: </b></td>
					<td><?php echo $flight['FlightDetail']['flight_name']; ?></td>
				</tr>
				<tr>
					<td><b>From: </b></td>
					<td><?php echo $source['Location']['location']; ?></td>
					<td><b>To: </b></td>
					<td><?php echo $destination['Location']['location']; ?></td>
				</tr>
				<tr>
					<td><b>Departure: </b></td>
					<td><?php echo $flight['FlightDetail']['datetime_departure']; ?></td>
					<td><b>Arrival: </b></td>
					<td><?php echo $flight['FlightDetail']['datetime_arrival']; ?></td>
				</tr>
				<tr>
					<td><b>No of Passengers: </b></td>
					<td><?php echo $info['Order']['passengers']; ?></td>
					<td><b>Total amount: </b></td>
					<td><?php echo $info['Order']['amount']; ?></td>
				</tr>
				<tr>
					<td><b>Order No: </b></td>
					<td><?php echo $info['Order']['order_number']; ?></td>
					<td><b>Booking Date: </b></td>
					<td><?php echo $info['Order']['created_datetime']; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="head2">
		<h4>Passenger Details</h4>
	</div>
	<div class="passenger_details">
		<table width="100%">
			<thead>
				<tr>
					<th>SNo</th>
					<th>Name</th>
					<th>Age</th>
					<th>Gender</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; foreach($info['Passengers'] as $pass) { ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $pass['name']; ?></td>
					<td><?php echo $pass['age']; ?></td>
					<td><?php echo $pass['gender']; ?></td>
				</tr>
				<?php $i++; } ?>
			</tbody>
		</table>
	</div>
	<div class="status">
		<h3>Status: Booked</h3>
	</div>
</div>