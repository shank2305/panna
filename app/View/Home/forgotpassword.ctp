<style>
	body
	{
		background-color: #E6F2FF;
	}
	.content
	{
		background-color: white;
		padding-bottom: 2%;
	}
	.container 
	{
    	width: 850px!important;
	}
	.data
	{
		margin-top: 1%;
	}
</style>
<div class="container">
	<div class="content">
	<h3>Forgot Password</h3>
		<div class="row" style="margin-top: 5%;">
		<form action="" method="post" id="forgot_password">
			<div class="col-md-4 data">
				Enter Email id: 
			</div>
			<div class="col-md-7 data">
				<input type="text" class="form-control" name="email" id="email">
				<p class="message" style="color: red; font-size: 11px;"><?php echo @$message; ?></p>
			</div>
			<br>
			<div class="col-md-4 data">
				
			</div>
			<div class="col-md-7 data">
				<button type="submit" class="btn btn-primary" style="width: 35%;">Reset</button>
			</div>
		</form>
		<div class="col-md-4 data" style="margin-top: 4.5%;">
				<b style="float: right;">Note:</b>
			</div>
			<div class="col-md-7 data" style="margin-top: 4.5%;">
				Your password reset link will be sent to your mail id 
			</div>
		</div>
	</div>
</div>
