<style>
	body
	{
		background-color: #E6F2FF;
	}
	.sections
	{
		margin-left: 2%;
		background-color: white;
		padding-bottom: 2%;

	}
	.head
	{
		padding-top: 5%;
		padding-left: 1%;
	}
	.userdetails
	{
		margin-top: 5%;
		padding-left: 1%;
	}
	.box_detail
	{
		margin-top: 3%;
		border: 1px solid;
		padding: 1%;
	}
	.marg
	{
		margin-top: 1%;
	}
	/*table,th,td
	{
		text-align: center;
	}*/
</style>


<div class="row">
	<div class="col-md-8 sections">
		<div class="head"><h2>Invoice</h2></div>
		<div class="userdetails">
			<table width="70%">
				<tbody>
					<tr>
						<td><b>Journey:</b><td>
						<td><?php echo $find['Order']['journey_type']; ?></td>
						<td><b>Order Number:</b><td>
						<td><?php echo $find['Order']['order_number']; ?></td>
					</tr>
					<tr>
						<td><b>Source:</b><td>
						<td><?php echo $find['From']['location']; ?></td>
						<td><b>Destination:</b><td>
						<td><?php echo $find['To']['location']; ?></td>
					</tr>
					<tr>
						<td><b>Date:</b><td>
						<td><?php echo $find['Order']['date'] ?></td>
						<?php if($find['Order']['journey_type'] == 'RoundTrip') { ?>
						<td><b>Return Date:</b><td>
						<td><?php echo $find['Order']['return_date'] ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td><b>Flight Name:</b><td>
						<td><?php echo $find['Flight_onward']['flight_name']; ?></td>
						<?php if($find['Order']['journey_type'] == 'RoundTrip') { ?>
						<td><b>R Flight Name:</b><td>
						<td><?php echo $find['Flight_return']['flight_name']; ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td><b>Passengers:</b><td>
						<td><?php echo $find['Order']['passengers']; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box_detail">
			<div class="row">
				<div class="col-md-2 marg"><b>Flight No.</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_onward']['flight_no']; ?></div>
				<div class="col-md-2 marg"><b>Flight Name</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_onward']['flight_name']; ?></div>
				<br>
				<?php if($find['Order']['journey_type'] == 'RoundTrip') { ?>
				<div class="col-md-2 marg"><b>R. Flight No.</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_return']['flight_no']; ?></div>
				<div class="col-md-2 marg"><b>R. Flight Name</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_return']['flight_name']; ?></div>
				<br>
				<?php } ?>
				<div class="col-md-2 marg"><b>Source</b></div>
				<div class="col-md-4 marg"><?php echo $find['From']['location']; ?></div>
				<div class="col-md-2 marg"><b>Destination</b></div>
				<div class="col-md-4 marg"><?php echo $find['To']['location']; ?></div>
				<br>
				<div class="col-md-2 marg"><b>Departure</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_onward']['datetime_departure']; ?></div>
				<div class="col-md-2 marg"><b>Arrival</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_onward']['datetime_arrival']; ?></div>
				<br>
				<?php if($find['Order']['journey_type'] == 'RoundTrip') { ?>
				<div class="col-md-2 marg"><b>R Departure</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_return']['datetime_departure']; ?></div>
				<div class="col-md-2 marg"><b>R Arrival</b></div>
				<div class="col-md-4 marg"><?php echo $find['Flight_return']['datetime_arrival']; ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="box_detail" style="padding-bottom: 8%;">
			<h4 style="margin-bottom: 2%;">Passengers</h4>
			<table width="100%" style="margin-left: 7%; margin-top: 4%;">
				<thead>
					<tr>
						<th>SNo.</th>
						<th>Name</th>
						<th>Age</th>
						<th>Gender</th>
					</tr>
				</thead>
				<tbody>
				<?php $i=1; foreach($find['Passengers'] as $people) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $people['name']; ?></td>
						<td><?php echo $people['age']; ?></td>
						<td><?php echo $people['gender']; ?></td>
					</tr>
				<?php $i++; } ?>
				</tbody>
			</table>
			<h3 style="float:right; margin-top: 3%; margin-right: 4%;">Amount: <?php echo $find['Order']['amount']; ?></h3>
		</div>
		
	</div>
	
</div>