<style type="text/css">
      .navbar_main
      {
        margin-right: 0%!important;
      }
    </style>
<section class="form-section parallax">
<section>
 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header" style="margin-left: 4%;">
      <a class="navbar-brand " style="font-size: 30px;" href="<?php echo HTTP_ROOT?>"><img src="<?php echo HTTP_ROOT?>img/various/logo.png" style="width: 26%;"></a>
    </div>
    <ul class="nav navbar-nav navbar-right navbar_main">
    <?php if(!($this->Session->check('Auth'))) { ?>
    
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><button type="button" class="btn btn-warning Login-button" data-backdrop="false" data-toggle="modal" data-target="#id01">Login</button>
      <!-- <div id="signup">New Customer <a id="signup_link" data-backdrop="false" data-toggle="modal" data-target="#id02">SIGNUP</a> here<div> -->
      </li>
       <li class="divider"></li> 
          <li><button type="button" class="btn btn-warning Login-button" data-backdrop="false" data-toggle="modal" data-target="#id02">Register</button></li>
      <!-- <li class="divider"></li> 
          <li><a href="#">Agent Login</a></li>
      <li class="divider"></li> 
      <li><a href="#">My Bookings</a></li> -->
        </ul>
      </li>
      <?php } ?>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>">Home</a></li>
      <?php if(($this->Session->check('Auth'))) { ?>
        <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/dashboard">Dashboard</a></li>
      <?php } ?>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/aboutus">About Us</a></li>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/termsconditions">T&C</a></li>
      <li><a style="color: white;" href="<?php echo HTTP_ROOT?>Home/contactus">Contact Us</a></li>
      <!-- <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Support
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Contact Us</a></li>
      <li class="divider"></li> 
          <li><a href="#">Complete Booking</a></li>
      <li class="divider"></li> 
          <li><a href="#">Make a payment</a></li>
      <li class="divider"></li>
      <li><a href="#">Make a payment</a></li>
        </ul>
      </li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Special Deals
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Armed Forces Personnel</a></li>
      <li class="divider"></li>
          <li><a href="#">IDG Investee Companies</a></li>
      <li class="divider"></li>
          <li><a href="#">Corporates and SMEs</a></li>
      <li class="divider"></li>
      <li><a href="#">Corporate Card Holder</a></li>
      <li class="divider"></li>
      <li><a href="#">Offers</a></li>
        </ul>
      </li> -->
    <!-- <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Recent Search</a>
        <ul class="dropdown-menu">
          <li><a href="#">Things you view while searching are saved here</a></li>
        </ul>
      </li> -->
       <?php if(($this->Session->check('Auth'))) { ?>
      <li>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->Session->read('Auth.User.name'); ?>
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li class="dropdown-header">Profile</li>
            <li><a href="<?php echo HTTP_ROOT?>Home/editprofile/<?php echo base64_encode($this->Session->read('Auth.User.id'));?>">Edit Profile</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Home/changepassword/<?php echo base64_encode($this->Session->read('Auth.User.id'));?>">Change Password</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Home/bookinghistory/">Booking History</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo HTTP_ROOT?>Home/logout">Logout</a></li>
          </ul>
        </div>
      </li>
      <?php } ?>
    </ul>
  </div>
</nav> 
</section>

<?php 

$title=$this->params['action'];
if($title != 'index'){ ?>
<style type="text/css">
  .parallax
  {
    background-image: none;
    min-height: 0px; 
  }
  .navbar-inverse
{
  background-color: rgba(0, 0, 0, 1) !important;
  border: none;
  
}
</style>
<?php } ?>

<?php 
if($title == 'index'){ ?>

    <section class="form_main">
    <div class="row">
     <form id="search-form" action="<?php echo HTTP_ROOT?>Home/flight_list" method="post">
      <div class="col-sm-5 formforsearch">
        <div class="row">
          <div id="form_element" class="col-sm-12">
          <!-- <div class="btn-group" style="float:left;">
            <button type="button" class="btn btn-warning">Domestic</button>
            <button type="button" class="btn btn-warning">International</button>
          </div>  -->
          
          </div>
        </div>
        <br>
        <div class="row">
          
          <div class="col-sm-12 form_element">
          <label class="radio-inline"><input type="radio" value="OneWay" id="OneWay"  name="type_journey" checked>One Way</label>
          <label class="radio-inline"><input type="radio" value="RoundTrip" id="RoundTrip"  name="type_journey">Round Trip</label>
          </div>
          
        </div>
        <br>
        <div class="row">
          
          <div class="col-sm-4">
            <label>Leaving From</label>
          </div>
          <div class="col-sm-5">
            <label>Going to</label>
          </div>
        </div>
        <div class="row">
          
          <div class="col-sm-4">
            <select name="place_from" type="text" id="source" class="form-control required" required>
                <option value=''>Select Source</option>
                <?php foreach($locations as $location){?>
                  <option value="<?php echo $location['Location']['id']?>"><?php echo $location['Location']['location']?></option>
                <?php }?> 
            </select>
            </div>
          <div class="col-sm-4">
            <select name="place_to" type="text" id="destination" class="form-control required" required>
              <option value=''>Select Destination</option>
                <?php foreach($locations as $location){?>
                  <option value="<?php echo $location['Location']['id']?>"><?php echo $location['Location']['location']?></option>
                <?php }?>  
            </select>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <br>
        
        <div class="row">
          
          <div class="col-sm-4">
            <label>Departure Date</label>
          </div>
          <div class="col-sm-4 return_date">
            <label class="yet">Return Date</label>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row">
          
          <div class='col-sm-4'>
          <div class="input-group date" data-provide="datepicker">
            <input name="depart_date" id="godate" type="text" class="form-control required" required>
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
          </div>
          </div>
          <div class='col-sm-4 return_date yet'>
          <div class="input-group date"  data-provide="datepicker">
            <input name="return_date" id="comedate" type="text" class="form-control required">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
          </div>
          </div>
          <div class='col-sm-1'></div>
        </div>
        <br>
        <div class="row">
          
          <div class="col-sm-3">
            <label>Adult (12+ Yrs)</label>
          </div>
          <div class="col-sm-3">
            <label>Child (2-11 Yrs)</label>
          </div>
          <div class="col-sm-3">
            <label>Infant (0-2 Yrs)</label>
          </div>
        </div>
        <div class="row">
          
          <div class="col-sm-3"><input name="adult" type="text" class="form-control required number" required></div>
          <div class="col-sm-3"><input name="child" type="text" class="form-control number" ></div>
          <div class="col-sm-3"><input name="infant" type="text" class="form-control number" ></div>
        </div>
        <br>
        <div class="row">
          
          <!-- <div class="col-sm-3">
            <label>Pref Class</label>
          </div> -->
          <!-- <div class="col-sm-3">
            <label>Pref Airlines</label>
          </div> -->
          <div class="col-sm-3">
          </div>
        </div>
        <div class="row">
          
          <!-- <div class="col-sm-3 dropdowns">
             <select>
                <option value="" style="text-align:center;">All</option>
                <option value="Business">Business</option>
                <option value="Economy">Economy</option>
            </select> 
          </div> -->
          <!-- <div class="col-sm-5 dropdowns">
            <select name="airline">
                <option value="">---All---</option>
                <option value="BSP">BSP Carriers Only</option>
                <option value="Low Cost">Low Cost Only</option>
                <option value="Go Airways">Go Airways</option>
                <option value="Spice Jet">Spice Jet</option>
                <option value="Jet Airways">Jet Airways</option>
                <option value="Air India">Air India</option>
                <option value="Indigo">Indigo</option>
                <option value="Air Costa">Air Costa</option>
                <option value="Vistara">Vistara</option>
                <option value="True Jet">True Jet</option>
                <option value="Air Pegasus">Air Pegasus</option>
                <option value="Air Asia">Air Asia</option>
            </select> 
          </div> -->
          <div class="col-sm-3 search_button">
          <button type="submit" class="btn btn-warning">Search</button>
          </div>
          </form>
        </div>
        </div>
      
      <div class="col-sm-5 ads">
      <div id="myCarousel" class="carousel slide" data-ride="carousel"  >
        <!-- Indicators -->
          <!--<ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>-->
          <style type="text/css">
            .item
            {
              width: 94.5%;
            }
          </style>

          <!-- Wrapper for slides -->
            <div class="carousel-inner" style="height:426px;">
              <div class="item active" style="width:100%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad44.jpg" alt="Ad" width="91.5%">
              </div>

              <div class="item" style="width:100%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad55.jpg" alt="Ad" width="91.5%">
              </div>
            
              <div class="item" style="width:100%">
                <img src="<?php echo HTTP_ROOT ?>img/index/ad66.jpg" alt="Ad" width="91.5%">
              </div>
            </div>

          <!-- Left and right controls -->
            <!--<a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>-->
            </div>
      </div>
    </div>
    
    </section>

    <Section class="main_slider" style="background-color: white; opacity: 0.80;">
    <div>
      <div class="row" style="padding-top:2%; padding-bottom:2%; padding-left:2%">
        <div class="col-md-4">
          <div class="col-md-4">
            <img src="<?php echo HTTP_ROOT ?>img/index/bestprice.png" style="height:120px; width:120 px;"></div>
          <div class="col-md-8" style="padding-top:5%;">
                <b>Best Price Guranteed</b><br>
                Find out lowest price to
                destinations worldwide 
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-4">
            <img src="<?php echo HTTP_ROOT ?>img/index/easybook.png" style="height:120px; width:120 px;"></div>
          <div class="col-md-8" style="padding-top:5%; padding-left:11%;">
                <b>Easy Booking</b><br>
                Search, select and save the
                fastest way to book your trip
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-4">
            <img src="<?php echo HTTP_ROOT ?>img/index/customer.png" style="height:120px; width:120 px;"></div>
          <div class="col-md-8" style="padding-top:4%; padding-left:11%;">
                <b>24/7 Customer Care</b><br>
                Get booking assistance and special
                deals by calling +91-09999999999, <br>
                0361 -0000000 | 0000000  
          </div>
        </div>
      </div>
    </div>
</section>
<?php }?>

</section>
<script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/plugins/jquery-validation/jquery.validate.min.js"></script> 
    <script src="<?php echo HTTP_ROOT ?>js/pages/base_forms_validation.js"></script> 
  </script>
<script type="text/javascript">
  $('#search-form').validate();
</script>
<style type="text/css">
          .yet
          {
            display: none;
          }
        </style>
<script>

    $(document).ready(function(){
        $("#OneWay").click(function(){
            $(".yet").hide();
        });
        $("#RoundTrip").click(function(){
            $(".yet").show();
        });
    });

    //****************************************************
    //Validate the Places
    $(document).ready(function(){
      
      $( "#source" ).change(function() {
       var source = $( "#source" ).val();
       var destination = $( "#destination" ).val();
       if(source == destination){
        alert('Source and Destination cannot be same!');
        $('select').prop('selectedIndex', 0);
       }
       
      });
      $( "#destination" ).change(function() {
       var source = $( "#source" ).val();
       var destination = $( "#destination" ).val();
       if(source == destination){
        alert('Source and Destination cannot be same!');
        $('select').prop('selectedIndex', 0);
       }
       
      });
    });
  //********************************************************    
  //Validate the Dates
  $(document).ready(function(){
    $( "#comedate" ).change(function() {
       var Godate = $( "#godate" ).val();
       var Comedate = $( "#comedate" ).val();
       if(Godate > Comedate){
        alert('Return Date cannot be less than Departure Date!');
        document.getElementById("comedate").value = ""; 
       }
       
      });
    });
</script>