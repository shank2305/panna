<style>


/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 0% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
<div id="id01" class="modal">
  
  <form class="modal-content animate" style="width: 50%;" action="<?php echo HTTP_ROOT.'/Home/login'?>" method="post">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <h1 style="text-align: center;">Login</h1>
    <div class="container">
      <label><b>Email</b></label><br>
      <input style="width: 50%;" type="text" class="form-control" placeholder="Enter Email" name="email" required>
      <br>
      <label><b>Password</b></label><br>
      <input style="width: 50%;" type="password" class="form-control" placeholder="Enter Password" name="password" required>
      <br>
      <button style="width: 20%; margin-left: 18%;" type="submit">Login</button><br>
      <input type="checkbox" checked="checked"> Remember me
    </div>

    <div class="container" style="background-color:#f1f1f1; width: 100%;">
      <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
      <span class="psw">Forgot <a href="<?php echo HTTP_ROOT.'/Home/forgotpassword'?>">password?</a></span>
    </div>
  </form>
</div>

<div id="id02" class="modal">
  
  <form class="modal-content animate" style="width: 50%;" action="<?php echo HTTP_ROOT.'/Home/register'?>" method="post">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <h1 style="text-align: center;">Registration</h1>
    <div class="container">
      <label><b>Name</b></label><br>
      <input style="width: 50%;" type="text" class="form-control" placeholder="Enter Full Name" name="name" required>
      <br>
      <label><b>Email</b></label><br>
      <input style="width: 50%;" type="text" class="form-control" placeholder="Enter Email" name="email" required>
      <br>
      <label><b>Password</b></label><br>
      <input style="width: 50%;" type="password" class="form-control" placeholder="Enter Password" name="password" required>
      <br>
      <label><b>Mobile</b></label><br>
      <input style="width: 50%;" type="text" class="form-control" placeholder="Enter Mobile Number" name="phone" required>
      <br>
      <label><b>Gender</b></label><br>
      <label class="radio-inline"><input type="radio" value="Male" name="gender">Male</label>
      <label class="radio-inline"><input type="radio" value="Female" name="gender">Female</label>
      <br>
      <button style="width: 20%;margin-left: 30%;" type="submit">Register</button><br>
    </div>

    <div class="container" style="background-color:#f1f1f1; width: 100%;">
      <button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
      <span class="psw">Already Registered? <a onclick="document.getElementById('id02').style.display='none'" data-backdrop="false" data-toggle="modal" data-target="#id01">Login</a> Now</span>
    </div>
  </form>
</div>



<div id="id03" class="modal">
  
  <form class="modal-content animate" style="width: 50%; padding-bottom: 2%; margin-top: 12%; " action="<?php echo HTTP_ROOT.'/Home/login'?>" method="post">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <h3 id="msg" style="text-align: center;"></h3>
  </form>
</div>


<!-- <script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script> -->
<script type="text/javascript">

</script>