  <!-- Sidebar -->
  <nav id="sidebar"> 
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll"> 
      <!-- Sidebar Content --> 
      <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
      <div class="sidebar-content"> 
        <!-- Side Header -->
        <div class="side-header side-content bg-white-op"> 
          <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
          <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close"> <i class="fa fa-times"></i> </button>
          <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
         
          <a class="h4 text-white" href="javascript:void(0)"> <span class="font-w600">PNNA</span> <span class="sidebar-mini-hide"></span> </a> </div>
        <!-- END Side Header --> 
        
        <!-- Side Content -->
        <div class="side-content">
          <ul class="nav-main">
           
            
            <?php if($this->Session->check('User')) { 
                if($this->Session->read('User.user_type') == 1) {
              ?>
            <li> <a class="active" href="<?php echo HTTP_ROOT?>People/evaldashboard"><i class="si si-grid"></i><span class="sidebar-mini-hide">Dashboard</span></a> </li>
            <li> <a href="<?php echo HTTP_ROOT?>People/pending"><i class="fa fa-thumbs-o-down"></i><span class="sidebar-mini-hide">Pending Requests</span></a> </li>
            <li> <a href="<?php echo HTTP_ROOT?>People/evaluated"><i class="fa fa-thumbs-o-up"></i><span class="sidebar-mini-hide">Evaluated</span></a> </li>
            <?php }
                if($this->Session->read('User.user_type') == 2) {
                  ?>
            <li> <a class="active" href="<?php echo HTTP_ROOT?>People/empdashboard"><i class="si si-grid"></i><span class="sidebar-mini-hide">Dashboard</span></a> </li>
            <li> <a href="<?php echo HTTP_ROOT?>People/editprofile/<?php echo base64_encode($this->Session->read('User.id')) ?>"><i class="fa fa-pencil"></i><span class="sidebar-mini-hide">Add/Edit Profile</span></a> </li>
                  <?php
                }
            }
             ?>
            
            </ul>
        </div>
    
      </div>
  
    </div>
  </nav>
  <!-- Header -->
  <header id="header-navbar" class="content-mini content-mini-full"> 
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
      <li>
        <div class="btn-group">
          <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button" style="height:35px;"> 
            <!-- <?php if(empty($profile_image)){?>
            <img src="<?php echo HTTP_ROOT ?>img/avatar.png" alt="Avatar"> 
            <?php } else{?>
            <img src="<?php echo HTTP_ROOT.'img/profile/'.$profile_image['Admin']['image']?>" alt="Avatar"> 
            <?php }?> -->
            <?php if(!empty($this->Session->read('Admin.username'))){ 
                  echo $admin['Admin']['username'];
                   } 
                  else if(!empty($this->Session->read('User.name'))){ 
                  echo $this->Session->read('User.name');
                    } else echo "New User"; ?>
            <span class="caret"></span> </button>
          
          <ul class="dropdown-menu dropdown-menu-right">
            
            <?php if($this->Session->check('User')) { ?>
            <li> <a tabindex="-1" href="<?php echo HTTP_ROOT?>People/changepassword"> <i class="si si-settings pull-right"></i>Change Password</a> </li>
            <li class="divider"></li>
            <li> <a tabindex="-1" href="<?php echo HTTP_ROOT?>People/logout_user"> <i class="si si-logout pull-right"></i>Log out</a> </li>
            <?php } ?>
          </ul>
        </div>
      </li>
      
    </ul>
   
    <ul class="nav-header pull-left">
      <li class="hidden-md hidden-lg"> 
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button"> <i class="fa fa-navicon"></i> </button>
      </li>
      <li class="hidden-xs hidden-sm"> 
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button"> <i class="fa fa-ellipsis-v"></i> </button>
      </li>
     
    </ul>
    <!-- END Header Navigation Left --> 
  </header>
  <!-- END Header --> 
  <style>
    .btn.btn-image
    {
      padding-left: 8px!important;
    }
  </style>