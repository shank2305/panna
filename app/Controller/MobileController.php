<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');
ob_start();
class MobileController extends AppController
{
	public function decode_request()
    {
        return $this->request->input('json_decode',false);
    }

    function register()
    {
    	$this->loadModel('User');
    	$user = $this->decode_request();
    	if(!filter_var($user->user['0']->email, FILTER_VALIDATE_EMAIL)) {
            $data['status'] = false;
            $data['msg'] = 'Incorrect email id';
            echo json_encode($data);
            die;
        }
        if(empty($user->user['0']->name))
        {
            $data['status'] = false;
            $data['msg'] = 'Enter Name';
            echo json_encode($data);
            die;
        }
        if(empty($user->user['0']->email))
        {
            $data['status'] = false;
            $data['msg'] = 'EMail cannot be empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->user['0']->password))
        {
            $data['status'] = false;
            $data['msg'] = 'Password Field cannot be empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->user['0']->mobile_no))
        {
            $data['status'] = false;
            $data['msg'] = 'Need Mobile Number';
            echo json_encode($data);
            die;
        }
        if(empty($user->user['0']->gender))
        {
            $data['status'] = false;
            $data['msg'] = 'Specify Gender';
            echo json_encode($data);
            die;
        }
        $detail['User']['name'] = $user->user['0']->name;
        $detail['User']['email'] = $user->user['0']->email;
        $detail['User']['password'] = md5($user->user['0']->password);
        $detail['User']['phone'] = $user->user['0']->mobile_no;
        $detail['User']['gender'] = $user->user['0']->gender;
        $detail['User']['varify_status'] = 0;
        $detail['User']['status'] = 0;
        $detail['User']['created_date'] = date('Y-m-d H:i:s');
        $srch=$this->User->find('first', array('conditions'=>array('User.email'=>$detail['User']['email'])));
        if(!empty($srch)){
        	//pr($detail);die;
            $data['status'] = false;
            $data['msg'] = 'Email id already exist';
        }
        else
        {
	        if($this->User->save($detail))
	        {
	        	$id = $this->User->getLastInsertId();
	        	$count=$this->User->find('first', array('conditions'=>array('User.id'=>$id)));
                 $data['status'] = true;
                 $data['msg'] = 'successfull';
                 $data['User'] = $count['User'];
                    $lik = HTTP_ROOT.'Home/varify/'.base64_encode($id);
					$link = 'Greetings!!<br>Thank your for registering to Airfare<br>Please click the following link to verify your account<br><a href="'.$lik.'">Click here</a>';
					$Email = new CakeEmail();
                    $Email->emailFormat('html')
							->from(array('noreply@airfare.com' => 'Airfare'))
							->to($count['User']['email'])
							->subject('Account Verification')
							->send($link);

            }
            else
            {
                $data['status'] = false;
                $data['msg'] = 'Unsuccessfull';
            }
	    }
	    echo json_encode($data);
        die;
    }

    function login()
    {
    	$this->loadModel('User');
        $this->loadModel('Order');
        $this->loadModel('FlightDetail');
        $this->loadModel('Location');
        $user = $this->decode_request();
        //pr($user);die;
         $email = $user->user['0']->email;

         if(empty($email))
        {
            $data['status'] = false;
            $data['msg'] = 'Email can not be empty';
            echo json_encode($data);
            die;
        }
        $pass = $user->user['0']->password;
        $password = md5($user->user['0']->password); 
        if(empty($password))
        {
        	$data['status'] = false;
        	$data['msg'] = "Password field cannot be empty";
        	echo json_encode($data);
            die;
        }
        $count=$this->User->find('first', array('conditions'=>array('User.email'=>$email, 'User.password'=>$password))); 
      
        if(!empty($count))
        {
           if($count['User']['varify_status'] == 0)
           {
            $data['status'] = false;
            $data['msg'] = 'Please verify your account';
           }
           else
            {
               	$data['User'] = $count['User'];
                $data['Original_Password'] = $pass;
                $data['status'] = true;
                $data['msg'] = 'successfull';
            } 
                
        }
        else{
            $data['status'] = false;
            $data['msg'] = 'No record Found';
        }
        echo json_encode($data);
        die;
    }
    function orderhistory()
    {
        $this->loadModel('Order');
        $this->loadModel('FlightDetail');
        $this->loadModel('Location');
        $this->Order->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
        $this->Order->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'flight_id'
                    )
                )
            )
        );
        $data = $this->decode_request();
        $data1['User_id'] = $data->data['0']->user_id;
        $find = $this->Order->find('all', array('order' => array('Order.id DESC') ,'conditions' => array('Order.user_id' => $data1['User_id'], 'Order.status' => '1')));
        $i=0;
                        // $data1['Order']['0'] = [];
                        // $data1['Order']['0']['source_name'] = 'From';
                        // $data1['Order']['0']['date'] = 'Date';
                        // $data1['Order']['0']['amount'] = 'Price';
                        // $data1['Order']['0']['destination_name'] = 'To';
                        // $data1['Order']['0']['flight_name'] = 'Name';
                foreach($find as $order){
                    
                        $data1['Order'][$i] = $order['Order'];
                        $data1['Order'][$i]['source_name'] = $order['From']['location'];
                        $data1['Order'][$i]['destination_name'] = $order['To']['location'];
                        $data1['Order'][$i]['flight_no'] = $order['Flight']['flight_no'];
                        $data1['Order'][$i]['flight_name'] = $order['Flight']['flight_name'];
                    
                    $i++;
                }

        //pr($data1);die;

        $data1['status'] = true;
        $data1['msg'] = 'Orders found';
        echo json_encode($data1);
        die;
    }
    function location()
    {
        $this->loadModel('Location');
        $location = $this->Location->find('all');
        $j=0;
            foreach ($location as $locate) {
                $data['Location'][$j] = $locate['Location'];
                $j++;
            }
        $data['status'] = true;
        $data['msg'] = 'Location fetch Successfull';
        //$data['Locations'] = $location;
        //pr($data);
        echo json_encode($data);
        die;
    } 

    function search()
    {
        $this->loadModel('FlightDetail');
        $this->loadModel('Location');
        
        $data = $this->decode_request();
        //pr($data);die;
        if(empty($data->data['0']->journey_type))
        {
            $data1['status'] = false;
            $data1['msg'] = 'Mention Journey Type';
            echo json_encode($data1);
            die;
        }
        $info['Detail']['type'] = $data->data['0']->journey_type;
        if(empty($data->data['0']->date))
        {
            $data1['status'] = false;
            $data1['msg'] = 'Mention Departure Date';
            echo json_encode($data1);
            die;
        }
        $info['Detail']['DDate'] = $data->data['0']->date;
        //$info['Detail']['RDate'] = $data->data['0']->return_date;
        $depart = date("Y-m-d H:i:s", strtotime($info['Detail']['DDate']." 00:00:00"));
        $departEnd = date("Y-m-d H:i:s", strtotime($info['Detail']['DDate']." 23:59:59"));
        //$return = date("Y-m-d H:i:s", strtotime($info['Detail']['RDate']." 00:00:00"));
        //$returnEnd = date("Y-m-d H:i:s", strtotime($info['Detail']['RDate']." 23:59:59"));
        if(empty($data->data['0']->source))
        {
            $data1['status'] = false;
            $data1['msg'] = 'Mention Source Location';
            echo json_encode($data1);
            die;
        }
        $info['Detail']['Source'] = $data->data['0']->source;
        if(empty($data->data['0']->destination))
        {
            $data1['status'] = false;
            $data1['msg'] = 'Mention Destination';
            echo json_encode($data1);
            die;
        }
        if(empty($data->data['0']->adults))
        {
            $data1['status'] = false;
            $data1['msg'] = 'Atleast one adult Passenger needed';
            echo json_encode($data1);
            die;
        }

        $info['Detail']['Destination'] = $data->data['0']->destination;
        //pr($info);die;
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'From' => array(
                    'className' => 'Location',
                    'foreignKey' => 'source'
                    )
                )
            )
        );
        $this->FlightDetail->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        $count = $this->FlightDetail->find('all', array('conditions' => array('FlightDetail.source' => $info['Detail']['Source'], 'FlightDetail.destination' => $info['Detail']['Destination'], 'datetime_departure >=' => $depart,'datetime_departure <=' => $departEnd)));
        //pr($count);die;
        if($count)
        {
            $i=0;
            foreach($count as $counting1)
            {
                
                $data1['FlightDetail_onward'][$i] = $counting1['FlightDetail'];
                $data1['FlightDetail_onward'][$i]['source_name'] = $counting1['From']['location'];
                $data1['FlightDetail_onward'][$i]['destination_name'] = $counting1['To']['location'];
                $i++;
            }
            if(empty($data->data['0']->children) && empty($data->data['0']->infants))
            $data1['passengers'] = $data->data['0']->adults;
            else if(empty($data->data['0']->children) && !empty($data->data['0']->infants))
            $data1['passengers'] = $data->data['0']->adults + $data->data['0']->infants;
            else if(empty($data->data['0']->infants) && !empty($data->data['0']->children))
            $data1['passengers'] = $data->data['0']->adults + $data->data['0']->children;
            else
            $data1['passengers'] = $data->data['0']->adults + $data->data['0']->children + $data->data['0']->infants;
            $data1['Travel_Date'] = $data->data['0']->date;
            $data1['status_onward'] = true;
            $data1['msg_onward'] = "Flights Found";
            
        }
        else
        {
            $data1['status_onward'] = false;
            $data1['msg_onward'] = "No Flights found for entered search.";
        }
        if($info['Detail']['type'] == "RoundTrip")
        {
            $this->FlightDetail->bindModel(
            array('belongsTo' => array(
                    'From' => array(
                        'className' => 'Location',
                        'foreignKey' => 'source'
                        )
                    )
                )
            );
            $this->FlightDetail->bindModel(
            array('belongsTo' => array(
                    'To' => array(
                        'className' => 'Location',
                        'foreignKey' => 'destination'
                        )
                    )
                )
            );
            $info['Detail']['RDate'] = $data->data['0']->return_date;
            $return = date("Y-m-d H:i:s", strtotime($info['Detail']['RDate']." 00:00:00"));
            $returnEnd = date("Y-m-d H:i:s", strtotime($info['Detail']['RDate']." 23:59:59"));
            $countreturn = $this->FlightDetail->find('all', array('conditions' => array('FlightDetail.destination' => $info['Detail']['Source'], 'FlightDetail.source' => $info['Detail']['Destination'], 'datetime_departure >=' => $return,'datetime_departure <=' => $returnEnd)));
            if($countreturn)
            {
                $i=0;
                foreach($countreturn as $counting2)
                {
                    
                    $data1['FlightDetail_return'][$i] = $counting2['FlightDetail'];
                    $data1['FlightDetail_return'][$i]['source_name'] = $counting2['From']['location'];
                    $data1['FlightDetail_return'][$i]['destination_name'] = $counting2['To']['location'];
                    $i++;
                }
                $data1['Return_Date'] = $data->data['0']->return_date; 
                $data1['status_return'] = true;
                $data1['msg_return'] = "Return Flights Found";
            }
            else
            {
                $data1['status_return'] = false;
                $data1['msg_return'] = "No Return Flights Found";   
            }
        }
        else
        {
            $data1['status_return'] = false;
            $data1['msg_return'] = "No Need of Return Flights in OneWay Journey";
        }

        echo json_encode($data1);
        //pr($data);
        die;
    }

    function getuser()
    {
        $this->loadModel('User');
        $data = $this->decode_request();
        if(empty($data->data['0']->id))
        {
            $data['status'] = false;
            $data['msg'] = 'Enter User Id';
            echo json_encode($data);
            die;
        }
        $user['User']['id'] = $data->data['0']->id;
        //pr($user);die;
        $result = $this->User->findById($user['User']['id']);
        //pr($result);die;
        echo json_encode($result);
        die;
    }

    function updateuser()
    {
        $this->loadModel('User');
        $data = $this->decode_request();
        if(empty($data->data['0']->id))
        {
            $data1['status'] = false;
            $data1['msg'] = 'Enter User Id';
            echo json_encode($data1);
            die;
        }
        $user['User']['id'] = $data->data['0']->id;
        $user['User']['name'] = $data->data['0']->name;
        $user['User']['gender'] = $data->data['0']->gender;
        $user['User']['phone'] = $data->data['0']->mobile;
        $rndname = rand(10,100000);
        $imgstr = $data->data['0']->image;
        $img = base64_decode($imgstr);
        $file = realpath('../webroot/img/profile/').'/'.$rndname.'.jpg';
        $imgname = $rndname.'.jpg';
        if(file_put_contents($file, $img))
        {
           $user['User']['image'] = $imgname;
       }
        //pr($user); die;

        if($this->User->save($user)) 
        {
            $user['status'] = true;
            $user['msg'] = "Sucessfully updated details";
        }
        else
        {
            $user['status'] = false;
            $user['msg'] = "Something went wrong in update";   
        }
        echo json_encode($user);
        die;
    }
    function changepassword()
    {
        $this->loadModel('User');
        $data = $this->decode_request();
        if(empty($data->data['0']->id))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Enter User Id';
            echo json_encode($detail);
            die;
        }
        if(empty($data->data['0']->password))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Send new Password';
            echo json_encode($detail);
            die;
        }
        $detail['User']['id'] = $data->data['0']->id;
        $detail['User']['password'] = md5($data->data['0']->password);

        if($this->User->save($detail)) 
        {
            $detail['status'] = true;
            $detail['msg'] = "Sucessfully updated password";
        }
        else
        {
            $detail['status'] = false;
            $detail['msg'] = "Something went wrong in update";   
        }
        echo json_encode($detail);
        die;
    }     

    function forgotpassword() 
    {
        $this->loadModel('User');
        $data = $this->decode_request();
        if(empty($data->data['0']->email))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Enter User Mail Address';
            echo json_encode($detail);
            die;
        }
        $detail['email'] =  $data->data['0']->email;
        //pr($detail);die;
        $user = $this->User->findByEmail($detail['email']);
        //pr($user);die;
        if($user)
        {
            $link = HTTP_ROOT.'Home/resetpassword/'.base64_encode($user['User']['id']);
                    $mail = 'Greetings!<br>Please use the link to reset your password<br><a href="'.$link.'">Click Here</a>';
            $Email = new CakeEmail();
            $Email->emailFormat('html');
            $Email->from(array('airfare@gmail.com' => 'AirFare'));
            $Email->to($user['User']['email']);
            $Email->subject('Password Reset');
            $Email->send($mail);
            $detail['status'] = true;
            $detail['msg'] = 'Check Email for reset link';
        }
        else
        {
            $detail['status'] = false;
            $detail['msg'] = 'User not Registered';
        }
        echo json_encode($detail);
        die;
    } 

    function order()
    {
        $this->loadModel('User');
        $this->loadModel('FlightDetail');
        $this->loadModel('Location');
        $this->loadModel('Order');
        $this->loadModel('Passenger');
        $this->FlightDetail->bindModel(
            array('belongsTo' => array(
                    'From' => array(
                        'className' => 'Location',
                        'foreignKey' => 'source'
                        )
                    )
                )
            );
            $this->FlightDetail->bindModel(
            array('belongsTo' => array(
                    'To' => array(
                        'className' => 'Location',
                        'foreignKey' => 'destination'
                        )
                    )
                )
            );
        $data = $this->decode_request();
        
        if(empty($data->data['0']->onwardFlight))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Need Flight id of onward journey';
            echo json_encode($detail);
            die;
        }
        if(empty($data->data['0']->user_id))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Enter User Id';
            echo json_encode($detail);
            die;
        }
        if(empty($data->data['0']->journey_type))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Enter Journey Type';
            echo json_encode($detail);
            die;
        }
        if(empty($data->data['0']->passengers))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Enter number of Passengers';
            echo json_encode($detail);
            die;
        }
        if(empty($data->data['0']->date))
        {
            $detail['status'] = false;
            $detail['msg'] = 'Enter journey date';
            echo json_encode($detail);
            die;
        }
        $cost = $this->FlightDetail->findById($data->data['0']->onwardFlight);
        $order['Order']['flight_id'] = $data->data['0']->onwardFlight;
        $order['Order']['user_id'] = $data->data['0']->user_id;
        $order['Order']['journey_type'] = $data->data['0']->journey_type;
        $order['Order']['source_name'] = $cost['From']['location'];
        $order['Order']['destination_name'] = $cost['To']['location'];
        $order['Order']['passengers'] = $data->data['0']->passengers;
        $order['Order']['date'] = $data->data['0']->date;
        
        //pr($cost);die;
        $order['Order']['amount'] =  $order['Order']['passengers'] * $cost['FlightDetail']['cost'];
        $order['Order']['status'] = '0';
        $order['Order']['created_datetime'] = date('Y-m-d H:i:s');
        $order['Order']['flight_name'] = $cost['FlightDetail']['flight_name'];
        $order['Order']['flight_no'] = $cost['FlightDetail']['flight_no'];
        $order['Order']['cost_per_ticket'] = $cost['FlightDetail']['cost'];
        $order['Order']['source'] = $cost['FlightDetail']['source'];
        $order['Order']['destination'] = $cost['FlightDetail']['destination'];
        $order['Order']['datetime_arrival'] = $cost['FlightDetail']['datetime_arrival'];
        $order['Order']['datetime_departure'] = $cost['FlightDetail']['datetime_departure'];
        //pr($order);die;
        //**********************************************************************************************
        //Updating Seats
        $seats['id'] = $data->data['0']->onwardFlight;
        $seats['seat_availability'] =  $cost['FlightDetail']['seat_availability'] - $data->data['0']->passengers;
        $this->FlightDetail->save($seats);
        //**********************************************************************************************
        if($order['Order']['journey_type'] == 'RoundTrip')
        {
            if(empty($data->data['0']->return_date))
            {
                $detail['status'] = false;
                $detail['msg'] = 'Enter Return Date';
                echo json_encode($detail);
                die;
            }
            if(empty($data->data['0']->flight_id_return))
            {
                $detail['status'] = false;
                $detail['msg'] = 'Enter Return Flight Id';
                echo json_encode($detail);
                die;
            }
            $order['Order']['return_date'] = $data->data['0']->return_date;
            $order['Order']['Flight_id_return'] = $data->data['0']->flight_id_return;
            $Flight_return = $this->FlightDetail->findById($data->data['0']->flight_id_return);
            //pr($Flight_return);die;
            $order['Order']['amount'] = $order['Order']['amount'] + ($order['Order']['passengers'] * $Flight_return['FlightDetail']['cost']);
            $order['Order']['Flight_name_return'] = $Flight_return['FlightDetail']['flight_name'];
            $order['Order']['Flight_no_return'] = $Flight_return['FlightDetail']['flight_no'];
            //******************************************************************************************
            //Updating Return Flight Seats
            $seats_return['id'] = $data->data['0']->flight_id_return;
            $seats_return['seat_availability'] =  $Flight_return['FlightDetail']['seat_availability'] - $data->data['0']->passengers;
            $this->FlightDetail->save($seats_return);
            //******************************************************************************************
        }
        $this->Order->save($order['Order']);
        $order_id = $this->Order->getLastInsertID();
        $order1['id'] = $order_id;
        $order1['order_number'] = 'OR00'.$order_id;
        $this->Order->save($order1);
        $order['Order']['order_id'] = $order_id;
        $order['Order']['order_number'] = $order1['order_number'];
        $order['status'] = true;
        $order['msg'] = 'Sucessfull';
        echo json_encode($order);
        die;
    }
    function invoice()
    {
        $this->loadModel('User');
        $this->loadModel('FlightDetail');
        $this->loadModel('Location');
        $this->loadModel('Order');
        $this->loadModel('Passenger');
        $data = $this->decode_request(); 
        if(empty($data->data['0']->order_id))
        {
            $pass['status'] = false;
            $pass['msg'] = 'Send Order ID';
            echo json_encode($pass);
            die;
        } 
        $order_id = $data->data['0']->order_id;
        $this->Order->bindModel(
            array('belongsTo' => array(
                    'From' => array(
                        'className' => 'Location',
                        'foreignKey' => 'source'
                        )
                    )
                )
            );
        $this->Order->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'flight_id'
                    )
                )
            )
        );
        $this->Order->bindModel(
        array('hasMany' => array(
                'Passengers' => array(
                    'className' => 'Passenger',
                    'foreignKey' => 'order_id'
                    )
                )
            )
        );
         $details = $this->Order->findById($order_id);
        //pr($details);die;
        $new['Detail']['order_number'] = $details['Order']['order_number'];
        $new['Detail']['journey_type'] = $details['Order']['journey_type'];
        $new['Detail']['no_of_passengers'] = $details['Order']['passengers'];
        $new['Detail']['booking_date'] = $details['Order']['created_datetime'];
        $new['Detail']['journey_date'] = $details['Order']['date'];
        $new['Detail']['source'] = $details['From']['location'];
        $new['Detail']['destination'] = $details['To']['location'];
        $new['Detail']['arrival'] = $details['Flight']['datetime_arrival'];
        $new['Detail']['departure'] = $details['Flight']['datetime_departure'];
        $new['Detail']['onward_flight_name'] = $details['Flight']['flight_name'];
        $new['Detail']['onward_flight_no'] = $details['Flight']['flight_no'];
        if($new['Detail']['journey_type'] == 'RoundTrip')
        {
            $this->Order->bindModel(
            array('belongsTo' => array(
                    'Flight_return' => array(
                        'className' => 'FlightDetail',
                        'foreignKey' => 'Flight_id_return'
                        )
                    )
                )
            );
            $detailss = $this->Order->findById($order_id);
            //pr($detailss);die;
            $new['Detail']['return_flight_name'] = $detailss['Flight_return']['flight_name'];
            $new['Detail']['return_flight_no'] = $detailss['Flight_return']['flight_no'];
            $new['Detail']['return_date'] = $detailss['Order']['return_date'];
        }
        $z = 0;
        foreach($details['Passengers'] as $passenger)
        {
            $new['Passengers'][$z] = $passenger;
            $z++;
        }
        //*******************************************************************************************
        //Change Status of order on successful payment
        // $ord['id'] = $order_id;
        // $ord['status'] = '1';
        // $this->Order->save($ord);
        //*******************************************************************************************
        $new['status'] = true;
        $new['msg'] = "Invoice details found";
        echo json_encode($new);
        die;
    }

    function success()
    {
        $this->loadModel('User');
        $this->loadModel('FlightDetail');
        $this->loadModel('Location');
        $this->loadModel('Order');
        $this->loadModel('Passenger');
        $data = $this->decode_request();
        if(empty($data->order_id))
        {
            $pass['status'] = false;
            $pass['msg'] = 'Send Order ID';
            echo json_encode($pass);
            die;
        }
        $order_id = $data->order_id;
        //pr($data);die;
        $list = $data->data;
        //pr($list);die;
        //$pass['Passenger']['order_id'] = $data->order_id;
        $i = 0;
        foreach($list as $listing)
        {
            //pr($listing->name);die;
            $pass['Passenger']['name'] = $listing->name;
            $pass['Passenger']['age'] = $listing->age;
            $pass['Passenger']['gender'] = $listing->gender;
            $pass['Passenger']['order_id'] = $order_id;
            $pass['Passenger']['created_date'] = date("Y-m-d H:i:s");
            //pr($pass);die;
            $this->Passenger->create();
            $this->Passenger->save($pass);
            $i++;
        }
        $this->Order->bindModel(
            array('belongsTo' => array(
                    'From' => array(
                        'className' => 'Location',
                        'foreignKey' => 'source'
                        )
                    )
                )
            );
        $this->Order->bindModel(
        array('belongsTo' => array(
                'To' => array(
                    'className' => 'Location',
                    'foreignKey' => 'destination'
                    )
                )
            )
        );
        $this->Order->bindModel(
        array('belongsTo' => array(
                'Flight' => array(
                    'className' => 'FlightDetail',
                    'foreignKey' => 'flight_id'
                    )
                )
            )
        );
        $this->Order->bindModel(
        array('hasMany' => array(
                'Passengers' => array(
                    'className' => 'Passenger',
                    'foreignKey' => 'order_id'
                    )
                )
            )
        );
        $details = $this->Order->findById($order_id);
        //pr($details);die;
        $new['Detail']['order_number'] = $details['Order']['order_number'];
        $new['Detail']['journey_type'] = $details['Order']['journey_type'];
        $new['Detail']['no_of_passengers'] = $details['Order']['passengers'];
        $new['Detail']['booking_date'] = $details['Order']['created_datetime'];
        $new['Detail']['journey_date'] = $details['Order']['date'];
        $new['Detail']['source'] = $details['From']['location'];
        $new['Detail']['destination'] = $details['To']['location'];
        $new['Detail']['arrival'] = $details['Flight']['datetime_arrival'];
        $new['Detail']['departure'] = $details['Flight']['datetime_departure'];
        $new['Detail']['onward_flight_name'] = $details['Flight']['flight_name'];
        $new['Detail']['onward_flight_no'] = $details['Flight']['flight_no'];
        if($new['Detail']['journey_type'] == 'RoundTrip')
        {
            $this->Order->bindModel(
            array('belongsTo' => array(
                    'Flight_return' => array(
                        'className' => 'FlightDetail',
                        'foreignKey' => 'Flight_id_return'
                        )
                    )
                )
            );
            $detailss = $this->Order->findById($order_id);
            //pr($detailss);die;
            $new['Detail']['return_flight_name'] = $detailss['Flight_return']['flight_name'];
            $new['Detail']['return_flight_no'] = $detailss['Flight_return']['flight_no'];
            $new['Detail']['return_date'] = $detailss['Order']['return_date'];
        }
        $z = 0;
        foreach($details['Passengers'] as $passenger)
        {
            $new['Passengers'][$z] = $passenger;
            $z++;
        }
        //*******************************************************************************************
        //Change Status of order on successful payment
        $ord['id'] = $order_id;
        $ord['status'] = '1';
        $this->Order->save($ord);
        //*******************************************************************************************
        $new['status'] = true;
        $new['msg'] = "Successfull in saving Passenger list";
        echo json_encode($new);
        die;
    }


}
