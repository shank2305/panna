<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class UsersController extends AppController {
	public $uses = array();
  	public $components = array('Session', 'Email', 'Upload', 'RequestHandler', 'Cookie', 'Paginator');
  	public function isAuthorized($user)
    {
        return true;
    }
  	public function beforeFilter()
    {
        parent::beforeFilter();
        if($this->params['action'] != 'login'){
           if(!$this->Session->check('Admin') ){
           		if($this->params['action'] == 'forgot_password'){
           			$this->admin_forgot_password();
           		}else{
           			$this->redirect(array('action'=>'login'));
           		}	
           	}
       	}
        $this->loadModel('Admin');
        $id = $this->Session->read('Admin.id');
        $admin = $this->Admin->findById($id);
        //pr($admin);die;
        //$profile_image = $this->UserMeta->find('first', array('conditions' => array('UserMeta.user_id' => $id, 'UserMeta.key_meta' => 'image')));
        //pr($profile_image);die;
        $this->set(compact('admin')); 	

  	}

  	public function login(){
      $this->layout="admin";
      $this->loadModel("Admin");
      if($this->request->is('post')) 
      {
        $data = $this->data;
        $username = $data['Admin']['email'];
        $password = md5($data['Admin']['password']);
        $check_login = $this->Admin->find('first', array('conditions' => array('OR'=>array(array('Admin.username'=>$username),array('Admin.email'=>$username)),'Admin.password'=>$password)));
        if(!empty($check_login)){
          $this->Session->write('Admin',$check_login['Admin']);
          $this->redirect(array('action'=>'dashboard'));
        }
      }
  	}
    

  	public function logout(){
	    $this->Session->destroy();
	    $this->redirect(array('action'=>'login'));
	 }
  	public function dashboard(){
  		$this->layout="admin";
      $this->loadModel("User");
      $evals = $this->User->findAllByUserType(1);
      $emp = $this->User->findAllByUserType(2);
      $this->set(compact('evals','emp'));
      //echo "<pre>"; print_r($evals); print_r($emp); die;
  	}
   
    public function changepassword(){
      $this->layout="admin";
      $this->loadModel("Admin");
      if($this->request->is('post'))
      {
        $data1 = $this->request->data;
        $data['Admin'] = $data1['Admin'];
        $data['Admin']['password'] = md5($data['Admin']['password']);
        $this->Admin->id=$this->Session->read('Admin.id');
        $this->Admin->save($data);
        $this->Session->write('Admin.password', $data['Admin']['password']);
        $this->Session->write('success-msg','Your password has been changed.');
        $this->redirect(array('action'=>'dashboard'));
      }
    }
    public function check_password(){
      $this->loadModel('User');
      if($this->Session->read('Admin.password') == md5($_POST['oldpass'])){
        echo 'true';
      }
      else{
        echo 'false';
      }
      die;
    }
    public function profile(){
      $this->layout="admin";
      $this->loadModel('Admin');
      $id = $this->Session->read('Admin.id');
      $profile = $this->Admin->findById($id);
      //pr($profile);die;
      $this->set(compact('profile'));
    }
    public function editProfile($id=null){
      $this->layout="admin";
      $this->loadModel('Admin');
      $id = base64_decode($id);
      $profile = $this->Admin->findById($id);
      //pr($profile);die;
      $this->set(compact('profile'));
      if($this->request->is('post'))
      {
        $data = $this->request->data;
        if (!empty($_FILES)) {
                if (is_uploaded_file($_FILES['DishImage']['tmp_name'])) {
                  $image = $this->Components->load('resize');
                  //$image1 = $this->Components->load('resize');  
                  $destination1 = realpath('../webroot/img/profile') . '/';
                  $filename = time().'-'.$_FILES['DishImage']['name'];
                  $image->resize($_FILES['DishImage']['tmp_name'],$destination1.$filename,'aspect_fit',200,200,0,0,0,0);
                  $data['Admin']['image'] = $filename;
                }      
              }
        //echo "<pre>";print_r($data);die;
        $this->Admin->save($data);
        $this->redirect(array('action'=>'profile'));
      }
    }
 
    public function update_user($id=null){
        $this->loadModel('User');
        $id = base64_decode($id);
        $data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
        if($data['User']['status'] == '0'){
            $this->User->id = $id;
            $this->User->savefield('status',1); 
        }else{
            $this->User->id = $id;
            $this->User->savefield('status',0); 
        } 
        $this->redirect($this->referer());
    }
    public function addeval($id=null)
    {
      $this->layout="admin";
      $this->loadModel('User');
      $id = base64_decode($id);
        if($this->request->is('post'))
        {
          if(empty($id))
          {
              $data = $this->data;
              $data['User']['email_status'] = 1;
              $data['User']['mobile_status'] = 1;
              $data['User']['user_type'] = 1;
              $data['User']['status'] = 1;
              $data['User']['created_date'] = date("Y-m-d");
              $data['User']['evaluation_status'] = 3;
              $pass = RandomStringGenerator(8);
              $data['User']['password'] = md5($pass);
              //echo "<pre>"; print_r($data); die;
              try
              {
                if($this->User->save($data))
                { 
                    $mail = 'Greetings!<br>You have been Registered to PANNA Services Pvt. Ltd<br>Please use Your email to login using<br><b>Password</b>'. $pass;
                    $Email = new CakeEmail();
                    $Email->emailFormat('html');
                    $Email->from(array('noreply@panna.co.in' => 'PANNA'));
                    $Email->to($data['User']['email']);
                    $Email->subject('Account Registered');
                    $Email->send($mail);
                  
                }
              }
              catch(Exception $e){
                echo '</p>Employee code already exixst</p><a href="javascript:history.back()">back</a>';
                die;
             } 
          //echo "<pre>"; print_r($data); die;
          }
          else
          {
            $data = $this->data;
            $this->User->save($data);
            $this->redirect(array('action'=>'evaluators'));
          }
        }
        $editUser = $this->User->find("first", array('conditions' => array('User.id' => $id)));
        // $evals = $this->User->find("all", array('conditions' => array('User.user_type' => 1), 'fields' => array('name','id')));
        $this->set(compact('evals','editUser'));
    }
    public function adddomain($id=null)
    {
      $this->layout="admin";
      $this->loadModel('Domain');
      if($this->request->is('post'))
      {
        $data = $this->data;
        //pr($data);die;
        $this->Domain->save($data);
        $this->Session->write('success-msg','Domain Added.');
        $this->redirect($this->referer());
      }
      $alldomains = $this->Domain->find("all");
      $this->set(compact('alldomains'));
      //pr($alldomains);die;
    }
    public function adduser($id=null, $flag=null)
    {
        $this->layout="admin";
        $this->loadModel('User');
        $id = base64_decode($id);
        if($this->request->is('post'))
        {
          if(empty($id))
          {
              $data = $this->data;
              $data['User']['email_status'] = 1;
              $data['User']['mobile_status'] = 1;
              $data['User']['user_type'] = 2;
              $data['User']['status'] = 1;
              $data['User']['created_date'] = date("Y-m-d");
              $pass = RandomStringGenerator(8);
              $data['User']['password'] = md5($pass);
              //echo "<pre>"; print_r($data); die;
              try
              {
                if($this->User->save($data))
                { 
                    $mail = 'Greetings!<br>You have been Registered to PANNA Services Pvt. Ltd<br>Please use Your email to login using<br><b>Password: </b>'. $pass;
                    $Email = new CakeEmail();
                    $Email->emailFormat('html');
                    $Email->from(array('noreply@panna.co.in' => 'PANNA'));
                    $Email->to($data['User']['email']);
                    $Email->subject('Account Registered');
                    $Email->send($mail);
                  
                }
              }
              catch(Exception $e){
                echo '</p>Employee code already exixst</p><a href="javascript:history.back()">back</a>';
                die;
             } 
          //echo "<pre>"; print_r($data); die;
          }
          else
          {
            $data = $this->data;
            $this->User->save($data);
            $this->redirect(array('action'=>'employees'));
          }
        }
        $editUser = $this->User->find("first", array('conditions' => array('User.id' => $id)));
        $this->set(compact('editUser','flag'));
    }
    public function approvals()
    {
        $this->layout="admin";
        $this->loadModel('User');
        $user_list = $this->User->find("all", array('conditions' => array('User.user_type' => 0)));
        $this->set(compact('user_list'));
    }
    public function approve($id=null)
    {
      $this->loadModel('User');
      $id = base64_decode($id);
      $dat['User']['id'] = $id;
      $dat['User']['user_type'] = 1;
      $dat['User']['status'] = 1;
      $this->User->save($dat);
      $this->redirect($this->referer());
    }
    public function reports($id=null)
    {
      $this->layout = "admin";
      $id = base64_decode($id);
      $this->loadModel("User");
      $this->loadModel("Evaluation");
      $this->Evaluation->bindModel(
        array('belongsTo' => array(
                'User' => array(
                    'className' => 'User',
                    'foreignKey' => 'user_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->Evaluation->bindModel(
        array('belongsTo' => array(
                'Eval' => array(
                    'className' => 'User',
                    'foreignKey' => 'eval_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      //pr($this->User);die;
      $find = $this->Evaluation->find("all", array('recursive' => 2, 'conditions' => array('Evaluation.user_id' => $id)));
      $this->set(compact('find'));
      //pr($find);die;
    }
    public function evaluationdetails($cid=null, $eid=null)
    {
      $this->layout="admin"; 
      $cid = base64_decode($cid);
      $eid = base64_decode($eid);
      $this->loadModel("User");
      $this->loadModel("Education");
      $this->loadModel("Experience");
      $this->loadModel("Certification");
      $this->loadModel("Evaluation");
      $this->loadModel("Domain");
      $this->loadModel("ConsultantDomain");
      $this->loadModel("EvaluatorDomain");
      $this->loadModel("EducationEvaluation");
      $this->loadModel("ExperienceEvaluation");
      $this->loadModel("CertificationEvaluation");
      $this->EducationEvaluation->bindModel(
        array('belongsTo' => array(
                'Education' => array(
                    'className' => 'Education',
                    'foreignKey' => 'edu_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->ExperienceEvaluation->bindModel(
        array('belongsTo' => array(
                'Experience' => array(
                    'className' => 'Experience',
                    'foreignKey' => 'exp_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->CertificationEvaluation->bindModel(
        array('belongsTo' => array(
                'Certification' => array(
                    'className' => 'Certification',
                    'foreignKey' => 'cert_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
       $this->Certification->bindModel(
        array('belongsTo' => array(
                'Domain' => array(
                    'className' => 'Domain',
                    'foreignKey' => 'domain_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      //pr($eid); pr($cid);die;
      $edueval = $this->EducationEvaluation->find('first', array('conditions' => array('EducationEvaluation.eval_id' => $eid, 'EducationEvaluation.user_id' => $cid)));
      $expeval = $this->ExperienceEvaluation->find("all", array('conditions' => array('ExperienceEvaluation.eval_id' => $eid, 'ExperienceEvaluation.user_id' => $cid)));
      $certeval = $this->CertificationEvaluation->find("all", array('recursive' => 2, 'conditions' => array('CertificationEvaluation.eval_id' => $eid, 'CertificationEvaluation.user_id' => $cid)));
      $eval = $this->Evaluation->find('first', array('conditions' => array('Evaluation.eval_id' => $eid, 'Evaluation.user_id' => $cid)));
      //pr($expeval);die;
      //pr($edueval);pr($expeval);pr($certeval);die;
      $this->set(compact('edueval','expeval','certeval','eval'));
    }

    public function evaluators()
    {
        $this->layout="admin";
        $this->loadModel('User');
        $user_list = $this->User->find("all", array('conditions' => array('User.user_type' => 1)));
        //echo "<pre>"; print_r($user_list); die;
        $this->set(compact('user_list'));
    }
    public function employees()
    {
        $this->layout="admin";
        $this->loadModel('User');
        $user_list = $this->User->find("all", array('conditions' => array('User.user_type' => 2)));
        // $evals = $this->User->find("all", array('conditions' => array('User.user_type' => 1), 'fields' => array('name', 'id')));
        //echo "<pre>"; print_r($user_list); die;
        $this->set(compact('user_list','evals'));
    }
  
    public function deleteAll($modal=null,$id=null){
      $id = base64_decode($id);
      $this->loadModel($modal);
      $this->$modal->delete($id);
      $this->redirect($this->referer());
    }
    public function updateevaluation($id=null)
    {
      $id = base64_decode($id);
      $this->loadModel('User');
      $eval['User']['id'] = $id;
      $eval['User']['evaluation_status'] = 3;
      $this->User->save($eval);
      $this->redirect($this->referer());
    }
    public function verify($id=null)
    {
      $this->layout = null;
      $id = base64_decode($id);
      $this->loadModel('User');
      $data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
      if($data['User']['varify_status'] == '0')
      {
            $this->User->savefield('varify_status',1); 
        }
      else{
            $this->User->savefield('varify_status',0); 
        } 
    }
   
  }
