<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class PeopleController extends AppController {
	  public $uses = array();
  	public $components = array('Session', 'Email', 'Upload', 'RequestHandler', 'Cookie', 'Paginator');
  	public function isAuthorized($user)
    {
        return true;
    }
  	public function beforeFilter()
    {
        parent::beforeFilter();
        if($this->params['action'] != 'userlogin'){
           if(!$this->Session->check('User') ){
           		if($this->params['action'] == 'signup'){
           			//$this->user_forgot_password();
           		}
              else if($this->params['action'] == 'emailverification'){
                //$this->emailverification($id);
              }
              else{
           			$this->redirect(array('action'=>'userlogin'));
           		}	
           	}
       	}
        $this->loadModel('Admin');
        $id = $this->Session->read('Admin.id');
        $profile_image = $this->Admin->findById($id);
        $this->set(compact('profile_image')); 	

  	}
    public function userlogin(){
      $this->layout="user";
      $this->loadModel("User");
      if($this->request->is('post')) 
      {
        $data = $this->data;
        //echo "<pre>";print_r($data);die;
        $username = $data['User']['email'];
        $password = md5($data['User']['password']);
        $check_login = $this->User->find('first', array('conditions' => array('User.email'=>$username,'User.password'=>$password, 'User.mobile_status' => 1, 'User.status' => 1)));
        //echo "<pre>";print_r($check_login);die;

        if(!empty($check_login))
        {
          if($check_login['User']['email_status'] == 1)
          {
             $this->Session->write('User',$check_login['User']);
             if($this->Session->read('User.user_type') == 2)
             $this->redirect(array('action'=>'empdashboard'));
             if($this->Session->read('User.user_type') == 1)
             $this->redirect(array('action'=>'evaldashboard'));
          }
          else if($check_login['User']['email_status'] == 0)
          {
              $this->Session->write('success-msg','Please Verify your Email before login!');
              $this->redirect(array('action'=>'userlogin'));
          }
         }
        }
      }
    public function logout_user()
      {
        $this->Session->destroy();
        $this->redirect(array('action'=>'userlogin'));
      }
    public function changepassword()
    {
      $this->layout="user";
      $this->loadModel("User");
      if($this->request->is('post'))
      {
        $data1 = $this->request->data;
        
        $data['User'] = $data1['User'];
        $data['User']['password'] = md5($data['User']['password']);
        $data['User']['id'] = $this->Session->read('User.id');
        //pr($data);die;
        $this->User->save($data);
        $this->Session->write('User.password', $data['User']['password']);
        $this->Session->write('success-msg','Your password has been changed.');
        $this->redirect(array('action'=>'empdashboard'));
      }
    }
    public function check_password(){
      $this->loadModel('User');
      if($this->Session->read('User.password') == md5($_POST['oldpass'])){
        echo 'true';
      }
      else{
        echo 'false';
      }
      die;
    }
    public function empdashboard(){
      $this->layout="user";
      $id = $this->Session->read("User.id");
      $this->loadModel("User");
      $this->loadModel("Education");
      $this->loadModel("Experience");
      $this->loadModel("Domain");
      $this->loadModel("Certification");
      $this->User->bindModel(
      array('hasOne' => array(
              'Education' => array(
                  'className' => 'Education',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->Certification->bindModel(
      array('belongsTo' => array(
              'DomainName' => array(
                  'className' => 'Domain',
                  'foreignKey' => 'domain_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->User->bindModel(
      array('hasMany' => array(
              'Experience' => array(
                  'className' => 'Experience',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->User->bindModel(
      array('hasMany' => array(
              'Certification' => array(
                  'className' => 'Certification',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $editUser = $this->User->find("first", array('recursive' => 2,'conditions' => array('User.id' => $id)));
      $this->set(compact('editUser'));
      //pr($editUser);die;
      //echo "<pre>"; print_r($editUser); die;
    }
    public function evaluated()
    {
      $this->layout = "user";
      $this->loadModel("User");
      $this->loadModel("ConsultantDomain");
      $this->loadModel("EvaluatorDomain");
      $this->loadModel("Domain");
      $this->loadModel("Certification");
      $id = $this->Session->read("User.id");
      $evskills = $this->EvaluatorDomain->find('list', array('fields' => 'domain_id','conditions' => array('EvaluatorDomain.user_id' => $id)));
      
      $emps = $this->ConsultantDomain->find('list', array('fields' => 'user_id','conditions' => array('ConsultantDomain.domain_id' => $evskills)));
      
      $finals = $this->User->find("all", array('conditions' => array('User.id' => $emps, 'User.evaluation_status' => 3)));
      
      $this->set(compact('finals'));
    }
    public function deleteskill($id=null)
    {
      $this->layout = "user";
      $id = base64_decode($id);
      $this->loadModel('EvaluatorDomain');
      $this->EvaluatorDomain->delete($id);
      $this->redirect($this->referer());
      //pr($id);die; 
    }
    public function deleteexp($id=null)
    {
      $this->layout = "user";
      $id = base64_decode($id);
      $this->loadModel('Experience');
      $this->Experience->delete($id);
      $this->redirect($this->referer());
      //pr($id);die; 
    }
    public function deletecert($id=null)
    {
      $this->layout = "user";
      $id = base64_decode($id);
      $this->loadModel('Certification');
      $this->Certification->delete($id);
      $this->redirect($this->referer());
      //pr($id);die; 
    }
    public function editpro($id=null)
    {
      $this->layout = "user"; 
      $id = base64_decode($id);
      $this->loadModel("User");
      $this->loadModel("Domain");
      $this->loadModel("EvaluatorDomain");
      $this->User->bindModel(
      array('hasMany' => array(
              'EvaluatorDomain' => array(
                  'className' => 'EvaluatorDomain',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->EvaluatorDomain->bindModel(
      array('belongsTo' => array(
              'Domain' => array(
                  'className' => 'Domain',
                  'foreignKey' => 'domain_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      //pr($this->Skillset);die;
      $editUser = $this->User->find("first", array('recursive' => 2 ,'conditions' => array('User.id' => $id)));
      $domains = $this->Domain->find('all');
      //pr($editUser);die;
      $this->set(compact('editUser','domains')); 
      if($this->request->is('post'))
      {
        $data = $this->data;
        $user['User'] = $data['User'];
        $skill['EvaluatorDomain'] = $data['EvaluatorDomain']; 
        //pr($skill);pr($user);die;
        
        foreach($skill['EvaluatorDomain'] as $skills)
        {
            $new['EvaluatorDomain'] = $skills;
            $new['EvaluatorDomain']['user_id'] = $data['User']['id'];
            //pr($new);die;
            $this->EvaluatorDomain->create();
            $this->EvaluatorDomain->save($new);
            //
        }
        $this->User->save($user);
        $this->Session->write('success-msg','Your Profile has been updated!');
        $this->redirect(array('action'=>'evaldashboard'));

      }    
    }
    public function testing(){
      $this->layout = "user";
      $this->Session->write('success-msg','Your Profile has been updated!');
      $this->redirect(array('action'=>'evaldashboard'));
    }
    public function evaldashboard(){
      $this->layout = "user";
      $this->loadModel("User");
      $id = $this->Session->read("User.id");
    }
    public function signup(){
      $this->layout="user";
      $this->loadModel("User");
      if($this->request->is('post'))
      {
        $data = $this->data;
        //pr($data);die;
        $srch = $this->User->findByEmail($data['User']['email']);
        if($srch)
        {
          $this->Session->write('success-msg','Email Already Registered!!');
          $this->redirect(array('action'=>'signup'));
        }
        $data['User']['password'] = md5($data['User']['password']);
        $data['User']['parent_id'] = 0;
        $data['User']['email_status'] = 0;
        if($data['User']['user_type'] == 2)
        $data['User']['status'] = 1;
        else if($data['User']['user_type'] == 0)
        $data['User']['status'] = 0;
        $data['User']['created_date'] = date("Y-m-d");
        //pr($data);die;
        //echo "<pre>"; print_r($data);die;
        try
              {
                if($this->User->save($data))
                { 
                    $id = $this->User->getLastInsertId();
                    $link = HTTP_ROOT.'People/emailverification/'.base64_encode($id);
                    $mail = 'Greetings!<br>You have been Registered to PANNA Services Pvt. Ltd<br><a href="'.$link.'">Click Here</a>  to verify you account<br>Please use Your email and password to login';
                    $Email = new CakeEmail();
                    $Email->emailFormat('html');
                    $Email->from(array('noreply@panna.co.in' => 'PANNA'));
                    $Email->to($data['User']['email']);
                    $Email->subject('Email Verification');
                    $Email->send($mail);
                  
                }
              }
              catch(Exception $e){
                echo '</p>Oops!! Some Error Occured!!</p><a href="javascript:history.back()">back</a>';
                die;
             } 
        $this->Session->write('signup', "Please Verify Your Email. You can login after the admin verifies you!!!");
        $this->redirect(array('action'=>'userlogin'));
      }
    }
    public function emailverification($id=null){
      $id = base64_decode($id);
      $this->loadModel("User");
      //pr($id);die;
      $user = $this->User->findById($id);
      $check['User']['id'] = $id;
      $check['User']['email_status'] = 1;
      $this->User->save($check);
      $this->redirect(array('action'=>'userlogin'));
    }
    public function editprofile($id=null)
    {
        $this->layout="user";
        $id = base64_decode($id);
        $this->loadModel("User");
        $this->loadModel("Education");
        $this->loadModel("Experience");
        $this->loadModel("Certification");
        $this->loadModel("Domain");
        $this->loadModel("ConsultantDomain");
        $this->User->bindModel(
        array('hasOne' => array(
                'Education' => array(
                    'className' => 'Education',
                    'foreignKey' => 'user_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
        $this->Certification->bindModel(
        array('belongsTo' => array(
                'Domain' => array(
                    'className' => 'Domain',
                    'foreignKey' => 'domain_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
        $this->User->bindModel(
        array('hasMany' => array(
                'Experience' => array(
                    'className' => 'Experience',
                    'foreignKey' => 'user_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
        $this->User->bindModel(
        array('hasMany' => array(
                'Certification' => array(
                    'className' => 'Certification',
                    'foreignKey' => 'user_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
        $editUser = $this->User->find("first", array('recursive' => 2,'conditions' => array('User.id' => $id)));
        $domains = $this->Domain->find('all');
        //pr($editUser);die;
        //echo "<pre>"; print_r($editUser); print_r($output); die;
        if($this->request->is('post'))
        {
          $data = $this->data;
          //pr($data);die;
          $user['User'] = $data['User'];
          $this->User->save($user);
          $edu['Education'] = $data['Education'];
          $edu['Education']['created_date'] = date("Y-m-d");
          $exp['Experience'] = $data['Experience'];
          $certi['Certification'] = $data['Certification'];
          //echo "<pre>"; print_r($certi); print_r($_FILES); die;

          /////////////////////////////////////Education Details upload///////////////////////////////////////
          if($this->Education->save($edu))
          {
            $Idedu = $this->Education->getLastInsertId();
            if(!empty($_FILES['data']['name']['Education1']))
                {
                    $file = realpath('../webroot/files/Marksheets/').'/';
                    if(move_uploaded_file($_FILES['data']['tmp_name']['Education1']['tenschoolmarksheet'], $file.$_FILES['data']['name']['Education1']['tenschoolmarksheet']))
                    {
                      $file1['Education']['id'] = $Idedu;
                      $file1['Education']['tenschoolmarksheet'] = $_FILES['data']['name']['Education1']['tenschoolmarksheet'];
                      $this->Education->create();
                      $this->Education->save($file1);
                      //echo "<pre>"; print_r($edu); print_r("Saved"); die;
                    }
                    if(move_uploaded_file($_FILES['data']['tmp_name']['Education1']['twelveschoolmarksheet'], $file.$_FILES['data']['name']['Education1']['twelveschoolmarksheet']))
                    {
                      $file2['Education']['id'] = $Idedu;
                      $file2['Education']['twelveschoolmarksheet'] = $_FILES['data']['name']['Education1']['twelveschoolmarksheet'];
                      $this->Education->create();
                      $this->Education->save($file2);
                      //echo "<pre>"; print_r($edu); print_r("Saved"); die;
                    }
                    if(move_uploaded_file($_FILES['data']['tmp_name']['Education1']['gradmarksheet'], $file.$_FILES['data']['name']['Education1']['gradmarksheet']))
                    {
                      $file3['Education']['id'] = $Idedu;
                      $file3['Education']['gradmarksheet'] = $_FILES['data']['name']['Education1']['gradmarksheet'];
                      $this->Education->create();
                      $this->Education->save($file3);
                      //echo "<pre>"; print_r($edu); print_r("Saved"); die;
                    }
                    if(move_uploaded_file($_FILES['data']['tmp_name']['Education1']['postgradmarksheet'], $file.$_FILES['data']['name']['Education1']['postgradmarksheet']))
                    {
                      $file4['Education']['id'] = $Idedu;
                      $file4['Education']['postgradmarksheet'] = $_FILES['data']['name']['Education1']['postgradmarksheet'];
                      $this->Education->create();
                      $this->Education->save($file4);
                      //echo "<pre>"; print_r($edu); print_r("Saved"); die;
                    }
                }

          }
          /////////////////////////Experience Details Upload////////////////////////////////////////////////
          $i=0;
          foreach($exp['Experience'] as $exprt)
          {
            if(!empty($exprt['company']) && !empty($exprt['date_from']) && !empty($exprt['job_desc']) && !empty($exprt['date_to']))
            {
              $expr['Experience'] = $exprt;
              $expr['Experience']['user_id'] = $user['User']['id'];
              $expr['Experience']['status'] = 0;
              $expr['Experience']['created_date'] = date("Y-m-d");
              $this->Experience->create();
              if($this->Experience->save($expr))
              {
                $Idexp = $this->Experience->getLastInsertId();
                if(!empty($_FILES['data']['name']['Experience1']))
                {
                  $file = realpath('../webroot/files/Experiences/').'/';
                  if(move_uploaded_file($_FILES['data']['tmp_name']['Experience1'][$i]['document'], $file.$_FILES['data']['name']['Experience1'][$i]['document']))
                  {
                    $file1['Experience']['id'] = $Idexp;
                    $file1['Experience']['document'] = $_FILES['data']['name']['Experience1'][$i]['document'];
                    $this->Experience->create();
                    $this->Experience->save($file1);
                    //echo "<pre>"; print_r($edu); print_r("Saved"); die;
                  }
                }
              }
              $i++;
            }
          }
          
          ////////////////////////////////Certification Document Upload//////////////////////////////////////////
          $i=0;
          foreach($certi['Certification'] as $cert)
          {
            if(!empty($cert['domain_id']) && !empty($cert['authority']))
            {
              $cer['Certification'] = $cert;
              $cer['Certification']['document'] = "";
              $cer['Certification']['user_id'] = $user['User']['id'];
              $cer['Certification']['status'] = 0;
              $cer['Certification']['created_date'] = date("Y-m-d");
              $sk['ConsultantDomain']['domain_id'] = $cert['domain_id'];
              $sk['ConsultantDomain']['user_id'] = $user['User']['id'];
              //pr($cer);die;
              $this->ConsultantDomain->create();
              $this->ConsultantDomain->save($sk);
              $this->Certification->create();
              if($this->Certification->save($cer))
              {
                $Idcert = $this->Certification->getLastInsertId();
                if(!empty($_FILES['data']['name']['Certification1']))
                {
                  $file = realpath('../webroot/files/Certificates/').'/';
                  if(move_uploaded_file($_FILES['data']['tmp_name']['Certification1'][$i]['document'], $file.$_FILES['data']['name']['Certification1'][$i]['document']))
                  {
                    $file1['Certification']['id'] = $Idcert;
                    $file1['Certification']['document'] = $_FILES['data']['name']['Certification1'][$i]['document'];
                    $this->Certification->create();
                    $this->Certification->save($file1);
                    //echo "<pre>"; print_r($edu); print_r("Saved"); die;
                  }
                }
              }
              $i++;
            }
          }
          ////////////////////////////////////////////////////////////////////////////////////////////////////////
          $this->redirect(array('action'=>'empdashboard'));
        }
        $this->set(compact('editUser','domains','id'));
    }

    public function pending()
    {
      $this->layout = "user";
      $this->loadModel("User");
      $this->loadModel("ConsultantDomain");
      $this->loadModel("EvaluatorDomain");
      $this->loadModel("Domain");
      $this->loadModel("Certification");
      $id = $this->Session->read("User.id");
      //pr($id);die;
      $evskills = $this->EvaluatorDomain->find('list', array('fields' => 'domain_id','conditions' => array('EvaluatorDomain.user_id' => $id)));
      //pr($evskills);die;
      $emps = $this->ConsultantDomain->find('list', array('fields' => 'user_id','conditions' => array('ConsultantDomain.domain_id' => $evskills)));
      //pr($emps);die;
      $finals = $this->User->find("all", array('order' => 'id DESC','conditions' => array('User.id' => $emps, 'User.evaluation_status !=' => 3)));
      //pr($finals);die;
      $this->set(compact('finals'));
    }
    public function evaluate($id=null)
    {
      $this->layout = "user";
      $id = base64_decode($id);
      $this->loadModel("User");
      $this->loadModel("Education");
      $this->loadModel("Experience");
      $this->loadModel("Certification");
      $this->loadModel("Evaluation");
      $this->loadModel("Domain");
      $this->loadModel("ConsultantDomain");
      $this->loadModel("EvaluatorDomain");
      $this->loadModel("EducationEvaluation");
      $this->loadModel("ExperienceEvaluation");
      $this->loadModel("CertificationEvaluation");
      $this->User->bindModel(
      array('hasOne' => array(
              'Education' => array(
                  'className' => 'Education',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->Certification->bindModel(
        array('belongsTo' => array(
                'Domain' => array(
                    'className' => 'Domain',
                    'foreignKey' => 'domain_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->Certification->bindModel(
        array('hasOne' => array(
                'CertificationEvaluation' => array(
                    'className' => 'CertificationEvaluation',
                    'foreignKey' => 'cert_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->Experience->bindModel(
        array('hasOne' => array(
                'ExperienceEvaluation' => array(
                    'className' => 'ExperienceEvaluation',
                    'foreignKey' => 'exp_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->Education->bindModel(
        array('hasOne' => array(
                'EducationEvaluation' => array(
                    'className' => 'EducationEvaluation',
                    'foreignKey' => 'edu_id',
                    // 'fields' => array('id','name')
                    )
                )
            )
        );
      $this->User->bindModel(
      array('hasOne' => array(
              'Evaluation' => array(
                  'className' => 'Evaluation',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->User->bindModel(
      array('hasMany' => array(
              'Experience' => array(
                  'className' => 'Experience',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $this->User->bindModel(
      array('hasMany' => array(
              'Certification' => array(
                  'className' => 'Certification',
                  'foreignKey' => 'user_id',
                  // 'fields' => array('id','name')
                  )
              )
          )
      );
      $user = $this->User->find("first", array('recursive' => 2, 'conditions' => array('User.id' => $id)));
      $this->set(compact('user'));
      //pr($user);die;
      //echo "<pre>"; print_r($user); die;
      if($this->request->is('post'))
      {
        $data = $this->data;
        //pr($data);die;
        $data['EducationEvaluation']['eval_id'] = $this->Session->read("User.id");
        $data['EducationEvaluation']['edu_id'] = $user['Education']['id'];
        $data['EducationEvaluation']['user_id'] = $user['User']['id'];
        $data['EducationEvaluation']['created_date'] = date("Y-m-d");
        $this->EducationEvaluation->save($data);
        foreach($data['ExperienceEvaluation'] as $ex)
        {
          $exp['ExperienceEvaluation'] = $ex;
          $exp['ExperienceEvaluation']['eval_id'] = $this->Session->read("User.id");
          $exp['ExperienceEvaluation']['user_id'] = $user['User']['id'];
          $exp['ExperienceEvaluation']['created_date'] = date("Y-m-d");
          $this->ExperienceEvaluation->create();
          $this->ExperienceEvaluation->save($exp);
        }
        foreach ($data['CertificationEvaluation'] as $cert) 
        {
          $cer['CertificationEvaluation'] = $cert;
          $cer['CertificationEvaluation']['eval_id'] = $this->Session->read("User.id");
          $cer['CertificationEvaluation']['user_id'] = $user['User']['id'];
          $cer['CertificationEvaluation']['created_date'] = date("Y-m-d");
          $this->CertificationEvaluation->create();
          $this->CertificationEvaluation->save($cer);
        }
        $data['Evaluation']['created_date'] = date("Y-m-d");
        $this->Evaluation->save($data);
        $this->Session->write('success-msg','Thank You for Verification!');
        $this->redirect(array('action'=>'pending'));
      }
    }
    public function updateall($id, $model, $field)
    {
      $id = base64_decode($id);
      $model = base64_decode($model);
      $field = base64_decode($field);
      //echo "<pre>"; print_r($id);print_r($model);print_r($field); die;
      $this->loadModel($model);
      $data[$model]['id'] = $id;
      $data[$model][$field] = 1;
      $this->$model->save($data);
      $this->redirect($this->referer());
    }
}