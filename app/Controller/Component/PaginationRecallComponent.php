<?php
App::uses('Component', 'Controller');

/**
 * Pagination Recall CakePHP Component
 * CakePHP 2.x version Copyright (c) 2014 Thanassis Bakalidis abakalidis.blogspot.com=
 *
 * @author  thanassis 
 * @version  2.0
 * @license  MIT
 */
class PaginationRecallComponent extends Component {
    const PREV_DATA_KEY = 'Pafinaion-PrevData';

    public $components = array('Session');
    private $_controller = NULL;
    private $_previousUrl;

    public function startup(Controller $controller)
    {
        $this->_controller = $controller;        
        $sessionKey = "Pagination.{$this->_controller->modelClass}.options";

        // extract paging data from the request parameters
        $pagingParams = $this->_extractPagingParams();

        // if paging data exist write them in the session
        if (!empty($pagingParams)) {
            $this->Session->write( $sessionKey, $pagingParams);
            return;
        }

        
        // no paging data.
        // construct the previous URL
        $this->_previousUrl = $this->Session->check(self::PREV_DATA_KEY)
            ? $this->Session->read(self::PREV_DATA_KEY)
            : array(
                'controller' => '',
                'action' => ''
            );
        
        // and check if the current page is the same as the previous
        if ($this->_previousUrl['controller'] === $this->_controller->name &&
            $this->_previousUrl['action'] === $this->_controller->params['action']) {
            // in this case we have a link from our own paging::numbers() function
            // to move to page 1 of the current page
            return;
        }

        // we are comming from a different page so if we have any session data
        if ($this->Session->check($sessionKey))
            // then restore and use them
            $this->_controller->request->params['named'] = array_merge(
                $this->_controller->request->params['named'],
                $this->Session->read($sessionKey)
            );
    }
    
    public function shutdown(\Controller $controller)
    {
        // save the current controller and action for the next time
        $this->Session->write(
            self::PREV_DATA_KEY,
            array(
                'controller' => $this->_controller->name,
                'action' => $this->_controller->request->params['action']
            )
        );
    }

    private function _extractPagingParams()
    {
        $pagingParams = $this->_controller->request->params['named'];
        $vars = array('page', 'sort', 'direction');
        $keys = array_keys($pagingParams);
        $count = count($keys);

        for ($i = 0; $i < $count; $i++)
            if (!in_array($keys[$i], $vars))
                unset($pagingParams[$keys[$i]]);

        return $pagingParams;
    }
}
